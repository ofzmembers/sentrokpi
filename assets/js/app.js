


function doconfirm(){
    job = confirm("Are you sure to delete permanently?");
    if(job!=true)
    {
        return false;
    }
}

function getKpiList(employee_id,org_uid) {
    loadPreloader();
    var emp_id  = employee_id.value;  
   
    $.ajax({
        url: BASE_URL+'ajax/getKpiList',
        type: 'POST',
        data: {
            employee_id:emp_id,
            organization_uid:org_uid
        },
        success: function(data){
            var obj  = jQuery.parseJSON(data);
            $('#kpi_drop_down').empty();

            if(obj.length){
               $('#kpi_drop_down').append("<option value=''> -- Please Select -- </option>");
                for(var i = 0 ; i < obj.length ; i++){
                    $('#kpi_drop_down').append("<option value='"+obj[i].kpi_id+"'>"+obj[i].kpi_name+"</option>");
                }  
            }else{
                $('#kpi_drop_down').append("<option value=''> -- No new KPIs for assign to this employee -- </option>");
            }
            hidePreloader();
        },
        error: function(e) {
            //console.log(e);
        }
    });
}


function getKpiListForDivisions(division_id,org_uid) {

    var div_id  = division_id.value;  
   
    $.ajax({
        url: BASE_URL+'ajax/getKpiListForDivisions',
        type: 'POST',
        data: {
            division_id:div_id,
            organization_uid:org_uid
        },
        success: function(data){
            var obj  = jQuery.parseJSON(data);
            $('#kpi_drop_down').empty();

            if(obj.length){
               $('#kpi_drop_down').append("<option value=''> -- Please Select -- </option>");
                for(var i = 0 ; i < obj.length ; i++){
                    $('#kpi_drop_down').append("<option value='"+obj[i].kpi_id+"'>"+obj[i].kpi_name+"</option>");
                }  
            }else{
                $('#kpi_drop_down').append("<option value=''> -- No new KPIs for assign to this division -- </option>");
            }
        },
        error: function(e) {
            //console.log(e);
        }

    });


 }

  function getEmployeeListForDivision(division_id,org_uid){
    var div_id  = division_id.value; 
    loadPreloader();
   
    $.ajax({
        url: BASE_URL+'ajax/getEmployeeListForDivision',
        type: 'POST',
        data: {
            division_id:div_id,
            organization_uid:org_uid
        },
        success: function(data){
            var obj  = jQuery.parseJSON(data);

            $('#employee_id').empty();

            if(obj.length){
               $('#employee_id').append("<option value='all'> -- Please Select -- </option>");
                for(var i = 0 ; i < obj.length ; i++){
                    $('#employee_id').append("<option value='"+obj[i].employee_id+"'>"+obj[i].employee_name+"</option>");
                }  
            }else{
                $('#employee_id').append("<option value='all'> -- No Employees -- </option>");
            }
            hidePreloader();
           
        },
        error: function(e) {
            //console.log(e);
        }

    });

 }






/**
 * [update_division_actual_row_value in asscociation table
 * @param  {[type]} id      [description]
 * @param  {[type]} org_uid [description]
 * @return {[type]}         [description]
 */
function update_division_actual_row_value(id,org_uid){
    loadPreloader();
    $.ajax({
            url: BASE_URL+'ajax/update_division_actual_row_value',
            type: 'POST',
            data: {
                actual_value:$("#act_val"+id).val(), 
                association_id:id,
                organization_uid:org_uid
            },
        success: function(data){
            $("#btn"+id).empty();
            $("#btn"+id).append(' <i class="fa fa-check"></i> ');
            hidePreloader();
        },
        error: function(e) {
           // console.log(e);
            hidePreloader();
        }

    });
    return false;
 }

 function update_employee_actual_row_value(id,org_uid){
    $.ajax({
            url: BASE_URL+'ajax/update_division_actual_row_value',
            type: 'POST',
            data: {
                actual_value:$("#act_val"+id).val(), 
                association_id:id,
                organization_uid:org_uid
            },
        success: function(data){
            $("#btn"+id).empty();
            $("#btn"+id).append(' <i class="fa fa-check"></i> ');
        },
        error: function(e) {
            //console.log(e);
        }
    });
    return false;
 }


/*
	update previous division KPI data to records table
 */
 function update_division_kpi_actual_row_value(record_id,association_id,org_uid){
    loadPreloader();
    $.ajax({
            url: BASE_URL+'ajax/update_division_kpi_actual_row_value',
            type: 'POST',
            data: {
                actual_value:$("#act_val"+record_id+association_id).val(), 
                record_id:record_id,
                association_id:association_id,
                organization_uid:org_uid
            },
        success: function(data){
            $("#btn"+record_id).empty();
            $("#btn"+record_id).append(' <i class="fa fa-check"></i> ');
            hidePreloader();
        },
        error: function(e) {
            hidePreloader();
            console.log(e);
        }

    });
    return false;
 }

 /**
  * 
  */
 function update_employee_kpi_actual_row_value(record_id,association_id,org_uid){
    loadPreloader();
    $.ajax({
            url: BASE_URL+'ajax/update_employee_kpi_actual_row_value',
            type: 'POST',
            data: {
                actual_value:$("#act_val"+record_id+association_id).val(), 
                record_id:record_id,
                association_id:association_id,
                organization_uid:org_uid
            },
        success: function(data){
            $("#btn"+record_id).empty();
            $("#btn"+record_id).append(' <i class="fa fa-check"></i> ');
            hidePreloader();
        },
        error: function(e) {
            hidePreloader();
            //console.log(e);
        }

    });
    return false;
 }



/**
 * [getIndicators 
 * draw indicators
 * @return {[type]} [description]
 */
function getIndicators(){

    loadPreloader();

     $.ajax({
            url: BASE_URL+'ajax/getValuesForIndicators',
            type: 'POST',
            data: {
                organization_uid:$("#organization_uid").val(), 
                division_id:$("#division_id").val(), 
                employee_id:$("#employee_id").val()
            },
        success: function(data){
            $('#guage-area').empty();
            var obj  = jQuery.parseJSON(data);
            console.log(obj);
            if(obj.length){
                for(var i = 0 ; i < obj.length ; i++){
                    var divId = i;
                    var divIdNext = i+'A' ;
                    var kpi_name =  obj[i].kpi_name;
                    var targetVal =  obj[i].target;
                    var actualVal =  obj[i].actual;
                    var accumulated_target = obj[i].accumulated_target;
                    var accumulated_actual = obj[i].accumulated_actual;
                    var value1 =  obj[i].value1;
                    var value2 =  obj[i].value2;
                    var statusDetermination =  obj[i].status_determination;
                    var statusDererminationNum;
                    if(statusDetermination == 'by_value'){
                        statusDererminationNum = 1;
                    }else{
                        statusDererminationNum = 2;
                    }
                    var operationStatus =  obj[i].operation_status;
                    //load gauge meters
                    drawGuageGrid(divId,divIdNext,kpi_name,statusDererminationNum,operationStatus,targetVal,actualVal,accumulated_target,accumulated_actual,value1,value2);
                }  
            }
            hidePreloader();
        },
        error: function(e) {
            //console.log(e);
        }

    });
    
  
   return false;
}


/**
 * [getEmployeeListForDivisionReport description]
 * @param  {[type]} division_id [description]
 * @param  {[type]} org_uid     [description]
 * @return {[type]}             [description]
 */
 function getEmployeeListForDivisionReport(division_id,org_uid,nextUrl){
    var div_id  = division_id.value; 
    loadPreloader();
   
    $.ajax({
        url: BASE_URL+'ajax/getEmployeeListForDivision',
        type: 'POST',
        data: {
            division_id:div_id,
            organization_uid:org_uid
        },
        success: function(data){
            var obj  = jQuery.parseJSON(data);

            $('#employee_id').empty();

            if(obj.length){
               $('#employee_id').append("<option value='all'> -- All Employees -- </option>");
                for(var i = 0 ; i < obj.length ; i++){
                    $('#employee_id').append("<option value='"+obj[i].employee_id+"'>"+obj[i].employee_name+"</option>");
                }  
            }else{
                $('#employee_id').append("<option value='all'> -- No Employees -- </option>");
            }
            getReportsSummary(nextUrl);
           // hidePreloader();
           
        },
        error: function(e) {
            //console.log(e);
        }

    });

 }

/**
 * [getReportsSummary description]
 * @param  {[type]} nextUrl [description]
 * @return {[type]}         [description]
 */
function getReportsSummary(nextUrl){
    var nextURL = nextUrl;
    loadPreloader();

     $.ajax({
            url: BASE_URL+'ajax/getReportSummary',
            type: 'POST',
            data: {
                organization_uid:$("#organization_uid").val(), 
                division_id:$("#division_id").val(), 
                employee_id:$("#employee_id").val(),
                from_date:$("#datepicker1").val(),
                to_date:$("#datepicker2").val(),
                nextURL:nextUrl
            },
        success: function(data){
            console.log(data);
            $('.backend-data-table').empty();
            $('#print-preview-btn-js').show();
            
            $('#report-no-result-found').hide();
            $('#report-summary-table-area').show();
            $('#report-summar-tbody').empty();
           
            var obj  = jQuery.parseJSON(data);
           
            if(obj.length){
                for(var i = 0 ; i < obj.length ; i++){
                    var kpi_id =  obj[i].kpi_id;
                    var kpi_name =  obj[i].kpi_name;
                    var repeat_dur =  obj[i].repeat_duration;
                    if(repeat_dur == 1){
                         repeat_duration = 'Daily';
                    }
                    else if(repeat_dur == 2){
                        repeat_duration = 'Weekly';
                    }
                    else if(repeat_dur == 3){
                         repeat_duration = 'Monthly';
                    }
                    else if(repeat_dur == 4){
                         repeat_duration = 'Quaterly';
                    }
                    else if(repeat_dur == 5){
                        repeat_duration = 'Annually';
                    }
                    var status_determination =  obj[i].status_determination;
                    var operation_status =  obj[i].operation_status;
                    var accumulated_actual =  obj[i].accumulated_actual;
                    var accumulated_target =  obj[i].accumulated_target;
                    var actual =  obj[i].actual;
                    var target =  obj[i].target;
                    var YTD_target =  obj[i].YTD_target;
                    var YTD_actual =  obj[i].YTD_actual;
                    var YTD_variation =  obj[i].YTD_variation;
                    var YTD_variation_presage =  obj[i].YTD_variation_presage;
                    var value1 =  obj[i].value1;
                    var value2 =  obj[i].value2;
                    var variation =  obj[i].variation;
                    var variation_presage =  obj[i].variation_presage;
                    var $cssClass = '';
                    if(variation_presage){
                            $t = target;
                            $a = actual; 
                            

                            if(status_determination == 'by_value'){
                                // $v = $kpi->value1;
                                $v = value1;

                                // Operation Status A/T > V  {= 1}
                                if(operation_status == 1){
                                    // T*V > A ----------- red
                                    if($t * $v > $a){
                                        //print 'red'." ".'T*V > A';
                                        $cssClass = 'red-txt-op';
                                    }
                                    // T*V < A ----------- green
                                    else{
                                        //print 'green'." ".'T*V < A';
                                        $cssClass = 'green-txt-op';
                                    }

                                }
                                //Operation Status A/T < V  {= 2}
                                else{
                                    // T*V < A ----------- red
                                    if($t * $v < $a){
                                        //print 'red'." ".'T*V < A';
                                        $cssClass = 'red-txt-op';
                                    }
                                    // T*V > A ----------- green
                                    else{
                                        //print 'green'." ".'T*V > A';
                                        $cssClass = 'green-txt-op';
                                    }
                                    
                                }
                                
                            }
                            else{
                                // $v1 = $kpi->value1;
                                $v1 = value1;
                                $v2 = value2;

                                //Operation Status A/T > V1 > A/T > V2 > A/T   {= 1}

                                if(operation_status == 1){
                                    // T*V1 < A < T*V2 
                                    // T * V1  < A && A < T * V2 --------yellow
                                    if(($t * $v1 < $a) && ($t * $v2 > $a)){
                                        //print 'red'." ".'T*V1 > A';
                                        $cssClass = 'yellow-txt-op';
                                    }
                                    // T*V1 < A > T*V2 
                                    // T * V1  < A && A > T * V2 ---- green
                                    else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                        //$cssClass = 'yellow-txt-op';
                                        $cssClass = 'green-txt-op';
                                    }
                                    // T*V1 >A < T*V2 
                                    // T * V1  > A && A < T * V2 ----------- red 
                                    else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                        $cssClass = 'red-txt-op';
                                    }
                                }
                                //Operation Status A/T < V1 < A/T < V2 < A/T
                                else{
                                    // T * V1  < A && A < T * V2
                                    // T * V1  < A  < T * V2
                                    if(($t * $v1 < $a) && ($t * $v2 > $a)){
                        
                                       $cssClass = 'yellow-txt-op';
                                    }
                                    // T * V1  < A > T * V2
                                   // T * V1  < A && A > T * V2 ---- red
                                    else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                        //print 'yellow'." ".'T*V1 > A > T*V2';
                                       $cssClass = 'red-txt-op';
                                    }
                                    // T*V1 >A < T*V2----------- green
                                    // T * V1  > A && A < T * V2
                                     else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                        //print 'green'." ".'T*V2 > A';
                                       $cssClass = 'green-txt-op';
                                    }
                                }
                            }
                    }
                    //YTD
                    if(YTD_variation_presage){
                            $t = YTD_target;
                            $a = YTD_actual; 
                            

                            if(status_determination == 'by_value'){
                                // $v = $kpi->value1;
                                $v = value1;

                                // Operation Status A/T > V  {= 1}
                                if(operation_status == 1){
                                    // T*V > A ----------- red
                                    if($t * $v > $a){
                                        //print 'red'." ".'T*V > A';
                                        $cssClass = 'red-txt-op';
                                    }
                                    // T*V < A ----------- green
                                    else{
                                        //print 'green'." ".'T*V < A';
                                        $cssClass = 'green-txt-op';
                                    }

                                }
                                //Operation Status A/T < V  {= 2}
                                else{
                                    // T*V < A ----------- red
                                    if($t * $v < $a){
                                        //print 'red'." ".'T*V < A';
                                        $cssClass = 'red-txt-op';
                                    }
                                    // T*V > A ----------- green
                                    else{
                                        //print 'green'." ".'T*V > A';
                                        $cssClass = 'green-txt-op';
                                    }
                                    
                                }
                                
                            }
                            else{
                                // $v1 = $kpi->value1;
                                $v1 = value1;
                                $v2 = value2;

                                //Operation Status A/T > V1 > A/T > V2 > A/T   {= 1}

                                if(operation_status == 1){
                                    // T*V1 < A < T*V2 
                                    // T * V1  < A && A < T * V2 --------yellow
                                    if(($t * $v1 < $a) && ($t * $v2 > $a)){
                                        //print 'red'." ".'T*V1 > A';
                                        $cssClass = 'yellow-txt-op';
                                    }
                                    // T*V1 < A > T*V2 
                                    // T * V1  < A && A > T * V2 ---- green
                                    else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                        //$cssClass = 'yellow-txt-op';
                                        $cssClass = 'green-txt-op';
                                    }
                                    // T*V1 >A < T*V2 
                                    // T * V1  > A && A < T * V2 ----------- red 
                                    else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                        $cssClass = 'red-txt-op';
                                    }
                                }
                                //Operation Status A/T < V1 < A/T < V2 < A/T
                                else{
                                    // T * V1  < A && A < T * V2
                                    // T * V1  < A  < T * V2
                                    if(($t * $v1 < $a) && ($t * $v2 > $a)){
                        
                                       $cssClass = 'yellow-txt-op';
                                    }
                                    // T * V1  < A > T * V2
                                   // T * V1  < A && A > T * V2 ---- red
                                    else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                        //print 'yellow'." ".'T*V1 > A > T*V2';
                                       $cssClass = 'red-txt-op';
                                    }
                                    // T*V1 >A < T*V2----------- green
                                    // T * V1  > A && A < T * V2
                                     else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                        //print 'green'." ".'T*V2 > A';
                                       $cssClass = 'green-txt-op';
                                    }
                                }
                            }
                    }

                     $('#report-summar-tbody').append('<tr><td class="text-td">'+kpi_id+'</td><td class="text-td">'+kpi_name+'</td><td class="text-td">'+repeat_duration+'</td><td class="text-td">'+target+'</td><td class="text-td">'+actual+'</td><td class="text-td">'+variation+'</td><td class="text-td txt-white '+$cssClass+'">'+variation_presage+'</td><td class="text-td">'+YTD_target+'</td><td class="text-td">'+YTD_actual+'</td> <td class="text-td">'+YTD_variation+'</td> <td class="text-td txt-white '+$cssClass+'">'+YTD_variation_presage+'</td> <td> <div class="btn-group cust-group" role="group"> <a class="btn btn-warning" href="'+nextURL+'/'+kpi_id+'"> View Detail Report</a> </div></td></tr>');
                }
                 
            }else{
                $('#report-summary-table-area').hide();
                $('#report-no-result-found').show();
            }
            hidePreloader();
        },
        error: function(e) {
            //console.log(e);
        }

    });
    
  
   return false;
}


/**
 * [printDiv 
 * print reports
 * @param  {[type]} divName [description]
 * @return {[type]}         [description]
 */
function printDiv(divName) {

   
        // var printContents = document.getElementById(divName).innerHTML;
        // var originalContents = document.body.innerHTML;
        // document.body.innerHTML = printContents;
        // window.print();
        // document.body.innerHTML = originalContents;
        // 
      var orgUID = $("#organization_uid").val();
      var kpiName = $("#kpi_name").val();
      var nowTime = moment().format('MMMM Do YYYY, h:mm:ss a');
      var headerText =" <center><h2> KPI Detail Report</h2></center><center><h4>Organization Unique ID :- "+ orgUID+ "</h4><h4>KPI Name :- "+kpiName+"</h4><h5 id ='dateArea'>"+nowTime+"</h5></center><br><br>";

 
     $("#"+divName).print({
                    //Use Global styles
                    globalStyles : true,
                    //Add link with attrbute media=print
                    mediaPrint : true,
                    //Custom stylesheet
                    stylesheet : "",
                    //Print in a hidden iframe
                    iframe : false,
                    //Don't print this
                    noPrintSelector : ".avoid-this",
                    //Add this at top
                    prepend : headerText,
                    //Add this on bottom
                    append : ""
                });
   
  
}



/**
 * [printDiv  with organization name, division,employee
 * print reports
 * @param  {[type]} divName [description]
 * @return {[type]}         [description]
 */
function printDiv2(divName) {

     var orgUID = $("#organization_uid").val();
     var divisionName = $("#division_id option:selected").text();
     var employeeName = $("#employee_id option:selected").text();

     console.log(orgUID+" "+divisionName+" "+employeeName);
     var nowTime = moment().format('MMMM Do YYYY, h:mm:ss a');

     var headerText =" <center><h2>KPI</h2></center><center><h4>Organization Unique ID :- "+ orgUID+ "</h4><h4>Division Name :- "+divisionName+"</h4><h4>Employee Name :- "+employeeName+"</h4><h5 id ='dateArea'>"+nowTime+"</h5></center><br><br>";

   
        // var printContents = document.getElementById(divName).innerHTML;
        // var originalContents = document.body.innerHTML;
        // document.body.innerHTML = printContents;
        // window.print();
        // document.body.innerHTML = originalContents;
 
     $("#"+divName).print({
                    //Use Global styles
                    globalStyles : true,
                    //Add link with attrbute media=print
                    mediaPrint : true,
                    //Custom stylesheet
                    stylesheet : "",
                    //Print in a hidden iframe
                    iframe : false,
                    //Don't print this
                    noPrintSelector : ".avoid-this",
                    //Add this at top
                    prepend : headerText,
                    //Add this on bottom
                    append : ""
                });
   
  
}

/**
 * [getAvailableEmployeeTargetForKPI description]
 * @return {[type]} [description]
 */
function getAvailableEmployeeTargetForKPI(kpi_id_obj,org_uid){
    loadPreloader();
    $("#balanceTarget").html('0');
    var kpi_id  = kpi_id_obj.value; 
    $.ajax({
            url: BASE_URL+'ajax/getAvailableEmployeeTargetForKPI',
            type: 'POST',
            data: {
                kpi_id:kpi_id,
                organization_uid:org_uid,
                employee_id:$("#employee_id").val()
            },
        success: function(data){
            console.log(data);
            var obj  = jQuery.parseJSON(data);
            $("#balanceTarget").html(obj[0].final);
            hidePreloader();
        },
        error: function(e) {
            hidePreloader();
            //console.log(e);
        }

    });
    return false;
}

/**
 * create assign kpi to employee
 */

function checkTargetMatch(){
    var target_value = $("#target_value").val();
    var balanceTarget = $("#balanceTarget").text();


    target_value = Number(target_value)
    balanceTarget = Number(balanceTarget)
 

    if(balanceTarget > target_value || balanceTarget == target_value){
        return true;
    }else{
        alert("Please check the target value");
        return false;
    }
    
}


/**
 * [divisionSelectDataSubmit description]
 * @return {[type]} [description]
 */
function divisionSelectDataSubmit(){
    document.getElementById("employee_id").value = "all";
    document.getElementById("employee-kpi-list").submit();
}




     
    
