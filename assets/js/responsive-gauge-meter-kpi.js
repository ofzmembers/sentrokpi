/**
 * example ajax request
 * @return {[type]} [description]
 */
function getIndicatorsJson(){

   var BASE_URL = "http://localhost/";
    $.ajax({
        url: BASE_URL+'ajax/getValuesForIndicators',
        type: 'POST',
        data: {
            organization_uid:$("#organization_uid").val(),
            division_id:$("#division_id").val(),
            employee_id:$("#employee_id").val()
        },
        success: function(data){
            $('#guage-area').empty();
            var obj  = jQuery.parseJSON(data);
            console.log(data);

            if(obj.length){
                for(var i = 0 ; i < obj.length ; i++){
                    var divId = i;
                    var kpi_name =  obj[i].kpi_name;
                    var targetVal =  obj[i].target;
                    var actualVal =  obj[i].actual;
                    var value1 =  obj[i].value1;
                    var value2 =  obj[i].value2;
                    var statusDetermination =  obj[i].status_determination;
                    var statusDererminationNum;
                    if(statusDetermination == 'by_value'){
                        statusDererminationNum = 1;
                    }else{
                        statusDererminationNum = 2;
                    }
                    var operationStatus =  obj[i].operation_status;
                    drawGuageGrid(divId,kpi_name,statusDererminationNum,operationStatus,targetVal,actualVal,value1,value2)

                }
            }

        },
        error: function(e) {
            //console.log(e);
        }

    });


    return false;
}

function drawGuageGrid(divId,divIdNext,kpi_name,statusDetermination,operationStatus,targetVal,actualVal,accumulated_target,accumulated_actual,value1,value2){

    //$('.gauge-area-row').append('<div class="col-sm-5 col-sm-offset-1 gauge" id="guage'+divId+'"> <h4 class="text-center">'+kpi_name+'</h4><div class="by-value"> <h5>Actual <div class="small-square actual"></div><span class="actualValue">0</span></h5> <h5>Target <div class="small-square target"></div><span class="targetValue">0</span></h5> <h5>&nbsp;<div class="small-square"></div><span class="targetValueIntoV2"></span></h5> </div><div class="by-reference"> <h5>Actual <div class="small-square actual"></div><span class="actualValue">0</span></h5> <h5>Target * V1 <div class="small-square tv1"></div><span class="targetValueIntoV1">0</span></h5> <h5>Target * V2 <div class="small-square tv2"></div><span class="targetValueIntoV2">0</span></h5> </div><div class="guage-outer"> <ul class="meter"> <li class="red-1"></li><li class="red-2"></li><li class="yellow-1"></li><li class="yellow-2"></li><li class="green-1"></li><li class="green-2"></li></ul> <div class="dial"> <div class="inner"> <div class="arrow"></div></div></div><div class="info-meter1 actual"> <div class="inner1"> <div class="arrow1"> <div class="actualVal">A=<span class="actualValue">0</span></div></div></div></div><div class="info-meter2 target"> <div class="inner2"> <div class="arrow2"> <div class="targetVal">T=<span class="targetValue">0</span></div></div></div></div><div class="info-meter3 targetV1"> <div class="inner3"> <div class="arrow3"> <div class="targetValIntoV1">T*V1=<span class="targetValueIntoV1">0</span></div></div></div></div><div class="info-meter4 targetV2"> <div class="inner4"> <div class="arrow4"> <div class="targetValIntoV2">T*V2=<span class="targetValueIntoV2">0</span></div></div></div></div></div></div>');
    $('.gauge-area-row').append('<div class="col-sm-5 col-sm-offset-1 gauge" id="guage'+divId+'"> <h4 class="text-center">'+kpi_name+'</h4><div class="by-value"> <h5>Actual <div class="small-square actual"></div><span class="actualValue">0</span></h5> <h5>Target <div class="small-square target"></div><span class="targetValue">0</span></h5> <h5>&nbsp;<div class="small-square"></div><span class="targetValueIntoV2"></span></h5> </div><div class="by-reference"> <h5>Actual <div class="small-square actual"></div><span class="actualValue">0</span></h5> <h5>Target * V1 <div class="small-square tv1"></div><span class="targetValueIntoV1">0</span></h5> <h5> Target * V2 <div class="small-square tv2"></div><span class="targetValueIntoV2">0</span></h5> </div><div class="guage-outer"> <ul class="meter"> <li class="red-1"></li><li class="red-2"></li><li class="yellow-1"></li><li class="yellow-2"></li><li class="green-1"></li><li class="green-2"></li></ul> <div class="dial"> <div class="inner"> <div class="arrow"></div></div></div><div class="info-meter1 actual"> <div class="inner1"> <div class="arrow1"> <div class="actualVal">A=<span class="actualValue">0</span></div></div></div></div><div class="info-meter2 target"> <div class="inner2"> <div class="arrow2"> <div class="targetVal">T=<span class="targetValue">0</span></div></div></div></div><div class="info-meter3 targetV1"> <div class="inner3"> <div class="arrow3"> <div class="targetValIntoV1">T*V1=<span class="targetValueIntoV1">0</span></div></div></div></div><div class="info-meter4 targetV2"> <div class="inner4"> <div class="arrow4"> <div class="targetValIntoV2">T*V2=<span class="targetValueIntoV2">0</span></div></div></div></div></div></div><!-- /end --><div class="col-sm-5 col-sm-offset-1 gauge" id="guage'+divIdNext+'"> <h4 class="text-center">'+kpi_name+'</h4><div class="by-value"> <h5>Accumulated Actual <div class="small-square actual"></div><span class="actualValue">0</span></h5> <h5>Accumulated Target <div class="small-square target"></div><span class="targetValue">0</span></h5> <h5>&nbsp;<div class="small-square"></div><span class="targetValueIntoV2"></span></h5> </div><div class="by-reference"> <h5>Accumulated Actual <div class="small-square actual"></div><span class="actualValue">0</span></h5> <h5>AccumulatedTarget * V1 <div class="small-square tv1"></div><span class="targetValueIntoV1">0</span></h5> <h5>Accumulated Target * V2 <div class="small-square tv2"></div><span class="targetValueIntoV2">0</span></h5> </div><div class="guage-outer"> <ul class="meter"> <li class="red-1"></li><li class="red-2"></li><li class="yellow-1"></li><li class="yellow-2"></li><li class="green-1"></li><li class="green-2"></li></ul> <div class="dial"> <div class="inner"> <div class="arrow"></div></div></div><div class="info-meter1 actual"> <div class="inner1"> <div class="arrow1"> <div class="actualVal">A=<span class="actualValue">0</span></div></div></div></div><div class="info-meter2 target"> <div class="inner2"> <div class="arrow2"> <div class="targetVal">T=<span class="targetValue">0</span></div></div></div></div><div class="info-meter3 targetV1"> <div class="inner3"> <div class="arrow3"> <div class="targetValIntoV1">T*V1=<span class="targetValueIntoV1">0</span></div></div></div></div><div class="info-meter4 targetV2"> <div class="inner4"> <div class="arrow4"> <div class="targetValIntoV2">T*V2=<span class="targetValueIntoV2">0</span></div></div></div></div></div></div>');

    setTimeout(function() {
        gaugeDraw(divId,kpi_name,statusDetermination,operationStatus,targetVal,actualVal,value1,value2);
        gaugeDraw(divIdNext,kpi_name,statusDetermination,operationStatus,accumulated_target,accumulated_actual,value1,value2);
    },10);

}




function gaugeDraw(divId,kpi_name,statusDetermination,operationStatus,targetVal,actualVal,value1,value2){
    //get container width
    var width = $('.gauge-area').width();
    var guage_outer_width = $('.guage-outer').outerWidth();
   // console.log(guage_outer_width);

    //set gauge outer height
    $('.guage-outer').css('height',guage_outer_width / 2);
    //
    $('.meter li').css('width',guage_outer_width/2);
    //
    $('.meter li').css('height',guage_outer_width/2);


    //dial area
    $('.dial').css('width',guage_outer_width-100);
    $('.dial').css('height',guage_outer_width-100);
    $('.dial').css('margin-top',50);
    $('.dial').css('margin-left',50);

    //arrow
    $('.arrow').css('border-bottom-width',(guage_outer_width-100)/2);
    $('.arrow').css('left',(guage_outer_width-100)/2);

    //arrow2
    $('.arrow1,.arrow2,.arrow3,.arrow4').css('border-bottom-width',(guage_outer_width-100)/9);
    $('.arrow1,.arrow2,.arrow3,.arrow4').css('left',guage_outer_width/2);

    //infor meter
    $('.info-meter1,.info-meter2,.info-meter3,.info-meter4').css('width',guage_outer_width);
    $('.info-meter1,.info-meter2,.info-meter3,.info-meter4').css('height',guage_outer_width);

    drawDaugeMeter(divId,kpi_name,statusDetermination,operationStatus,targetVal,actualVal,value1,value2);

}

function drawDaugeMeter(divId,kpi_name,statusDetermination,operationStatus,targetVal,actualVal,value1,value2) {




    var actualValue = Number(actualVal);
    var targetValue = Number(targetVal);
    var divId = divId;
    var value1 = Number(value1);
    var value2 = Number(value2);
    var statusDetermination = Number(statusDetermination);
    var operationStatus = Number(operationStatus);

    /**
     * arcs
     */
    var red1Deg = 0;
    var red2Deg = 0;
    var yello1Deg = 0;
    var yello2Deg = 0;
    var green1Deg = 0;
    var greenCir2Deg = 0;

    var redCir1 = $('#guage' + divId + ' .red-1');
    var redCir2 = $('#guage' + divId + ' .red-2');
    var yelloCir1 = $('#guage' + divId + ' .yellow-1');
    var yelloCir2 = $('#guage' + divId + ' .yellow-2');
    var greenCir1 = $('#guage' + divId + ' .green-1');
    var greenCir2 = $('#guage' + divId + ' .green-2');

    var dial = $('#guage' + divId + ' .dial .inner'); // arrow
    var actualArrow = $('#guage' + divId + ' .inner1'); // arrow actual value
    var targetArrow = $('#guage' + divId + ' .inner2'); // arrow actual value
    var targetV1Arrow = $('#guage' + divId + ' .inner3'); // arrow target value1
    var targetV2Arrow = $('#guage' + divId + ' .inner4'); // arrow target value2

    var actualValueTxt = $('#guage' + divId + ' .actualValue'); // actual value text
    var targetValueTxt = $('#guage' + divId + ' .targetValue'); // target value text
    var targetValueIntoV1Txt = $('#guage' + divId + ' .targetValueIntoV1'); // target value * v1 text
    var targetValueIntoV2Txt = $('#guage' + divId + ' .targetValueIntoV2'); // target value * v2 text

    

    /**
     *  if statusDetermination
     *      1 = by value
     *      2 = by range
     */

    /**
     * by value
     * ============
     * if operation status
     *  1 = A/T > V
     *  2 = A/T < V
     */

    // by value
    if(statusDetermination == 1){
        $('#guage' + divId + ' .by-value').show();
        $('#guage' + divId + ' .by-reference').hide();
        targetV1Arrow.hide();
        targetV2Arrow.hide();
        actualArrow.show();
        targetArrow.show();


        //1 = A/T > V
        if(operationStatus == 1){
            //Actual < Target
            if(actualValue < targetValue){
                var percentageForValue = getPercentage(actualValue, targetValue);
                var targetDegrees = getDegrees(100);// 100% value
                var actualDegrees = getDegrees(percentageForValue);

                drawArcType1(statusDetermination,operationStatus,actualDegrees,targetDegrees,actualValue,targetValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetArrow, targetDegrees-90); // rotate arrow to target value
            }

            //Actual > Target
            if(actualValue > targetValue){

                var percentageForValue = getPercentage(actualValue, targetValue);
                var targetDegrees = getDegrees(percentageForValue);
                var actualDegrees = getDegrees(100);// 100% value

                drawArcType1(statusDetermination,operationStatus,actualDegrees,targetDegrees,actualValue,targetValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetArrow, targetDegrees-90); // rotate arrow to target value
            }

            //Actual = Target
            if(actualValue == targetValue){

                var percentageForValue = getPercentage(actualValue, targetValue);
                var targetDegrees = getDegrees(percentageForValue);
                var actualDegrees = getDegrees(100);// 100% value

                drawArcType1(statusDetermination,operationStatus,actualDegrees,targetDegrees,actualValue,targetValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetArrow, targetDegrees-90); // rotate arrow to target value
            }
        }
        //2 = A/T < V
        if(operationStatus == 2){
            //Actual < Target
            if(actualValue < targetValue){
               
                var percentageForValue = getPercentage(actualValue, targetValue);
                var targetDegrees = getDegrees(100);// 100% value
                var actualDegrees = getDegrees(percentageForValue);

                drawArcType2(statusDetermination,operationStatus,actualDegrees,targetDegrees,actualValue,targetValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetArrow, targetDegrees-90); // rotate arrow to target value
            }

            //Actual > Target
            if(actualValue > targetValue){

                var percentageForValue = getPercentage(actualValue, targetValue);
                var targetDegrees = getDegrees(percentageForValue);
                var actualDegrees = getDegrees(100);// 100% value

                drawArcType2(statusDetermination,operationStatus,actualDegrees,targetDegrees,actualValue,targetValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetArrow, targetDegrees-90); // rotate arrow to target value
            }

             //Actual = Target
            if(actualValue == targetValue){

                var percentageForValue = getPercentage(actualValue, targetValue);
                var targetDegrees = getDegrees(percentageForValue);
                var actualDegrees = getDegrees(100);// 100% value

                drawArcType2(statusDetermination,operationStatus,actualDegrees,targetDegrees,actualValue,targetValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetArrow, targetDegrees-90); // rotate arrow to target value
            }


        }
    }


    /**
     * by range
     * ============
     * if operation status
     *  1 =  A / T > V1 > A / T > V2 > A / T
     *  2 =  A / T < V1 < A / T < V2 < A / T
     */
    // by range
    if(statusDetermination == 2){
        //1 =  A / T > V1 > A / T > V2 > A / T
        $('#guage' + divId + ' .by-value').hide();
        $('#guage' + divId + ' .by-reference').show();
        actualArrow.show();
        targetV1Arrow.show();
        targetV2Arrow.show();

        //targetValue*value1
        var TV1 = targetValue*value1;
        //targetValue*value2
        var TV2 = targetValue*value2;

        if(operationStatus == 1){
            // Target * V1  < Actual && Actual < Target * V2
            if((TV1 < actualValue) && (TV2 > actualValue)){
                var percentageForTV1 = getPercentage(TV1,TV2);
                var TV2Degrees = getDegrees(100);// 100% value
                var TV1Degrees = getDegrees(percentageForTV1);
                var percentageForActualValue = getPercentage(actualValue,TV2);
                var actualDegrees = getDegrees(percentageForActualValue);
               
                drawArcType3(statusDetermination,operationStatus,actualDegrees,TV1Degrees,TV2Degrees,TV1,TV2,actualValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetV1Arrow, TV1Degrees-90); // rotate arrow to target*value1
                transform(targetV2Arrow, TV2Degrees-90); // rotate arrow to target*value2
            }

            // Target * V1  < Actual && Actual > Target * V2
            if((targetValue*value1 < actualValue) && (targetValue*value2 < actualValue)){
               

                var percentageForTV1 = getPercentage(TV1,actualValue);
                var percentageForTV2 = getPercentage(TV2,actualValue);               
                var TV2Degrees = getDegrees(percentageForTV2);               
                var TV1Degrees = getDegrees(percentageForTV1);
                var percentageForActualValue = getPercentage(actualValue,TV2);
                var actualDegrees = getDegrees(100); // 100%
                
                drawArcType3(statusDetermination,operationStatus,actualDegrees,TV1Degrees,TV2Degrees,TV1,TV2,actualValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetV1Arrow, TV1Degrees-90); // rotate arrow to target*value1
                transform(targetV2Arrow, TV2Degrees-90); // rotate arrow to target*value2
            }

              // Target * V1  > Actual && Actual < Target * V2
            if((TV1 > actualValue) && (TV2 > actualValue)){
                
                var percentageForTV1 = getPercentage(TV1,TV2);
                var TV2Degrees = getDegrees(100);// 100% value
                var TV1Degrees = getDegrees(percentageForTV1);
                var percentageForActualValue = getPercentage(actualValue,TV2);
                var actualDegrees = getDegrees(percentageForActualValue);
               
                drawArcType3(statusDetermination,operationStatus,actualDegrees,TV1Degrees,TV2Degrees,TV1,TV2,actualValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetV1Arrow, TV1Degrees-90); // rotate arrow to target*value1
                transform(targetV2Arrow, TV2Degrees-90); // rotate arrow to target*value2
            }

        }
        //2 =  A / T < V1 < A / T < V2 < A / T
        if(operationStatus == 2){            

            // Target * V1  < Actual && Actual < Target * V2
            if((TV1 < actualValue) && (TV2 > actualValue)){
                
                var percentageForTV1 = getPercentage(TV1,TV2);
                var TV2Degrees = getDegrees(100);// 100% value
                var TV1Degrees = getDegrees(percentageForTV1);
                var percentageForActualValue = getPercentage(actualValue,TV2);
                var actualDegrees = getDegrees(percentageForActualValue);
                
                drawArcType4(statusDetermination,operationStatus,actualDegrees,TV1Degrees,TV2Degrees,TV1,TV2,actualValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetV1Arrow, TV1Degrees-90); // rotate arrow to target*value1
                transform(targetV2Arrow, TV2Degrees-90); // rotate arrow to target*value2
            }

            // Target * V1  < Actual && Actual > Target * V2
            if((TV1 < actualValue) && (TV2 < actualValue)){
                
                var percentageForTV1 = getPercentage(TV1,actualValue);
                var percentageForTV2 = getPercentage(TV2,actualValue);
                var TV2Degrees = getDegrees(percentageForTV2);
                var TV1Degrees = getDegrees(percentageForTV1);
                var percentageForActualValue = getPercentage(actualValue,TV2);
                var actualDegrees = getDegrees(100); // 100%

                drawArcType4(statusDetermination,operationStatus,actualDegrees,TV1Degrees,TV2Degrees,TV1,TV2,actualValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetV1Arrow, TV1Degrees-90); // rotate arrow to target*value1
                transform(targetV2Arrow, TV2Degrees-90); // rotate arrow to target*value2
            }


             // Target * V1  > Actual && Actual < Target * V2
            if((TV1 > actualValue) && (TV2 > actualValue)){
                
                var percentageForTV1 = getPercentage(TV1,TV2);
                var TV2Degrees = getDegrees(100);// 100% value
                var TV1Degrees = getDegrees(percentageForTV1);
                var percentageForActualValue = getPercentage(actualValue,TV2);
                var actualDegrees = getDegrees(percentageForActualValue);
                
                drawArcType4(statusDetermination,operationStatus,actualDegrees,TV1Degrees,TV2Degrees,TV1,TV2,actualValue);//statusDetermination,actualDegrees,targetDegrees
                transform(dial, actualDegrees-90); // rotate arrow to actual value
                transform(actualArrow, actualDegrees-90); // rotate arrow to actual value
                transform(targetV1Arrow, TV1Degrees-90); // rotate arrow to target*value1
                transform(targetV2Arrow, TV2Degrees-90); // rotate arrow to target*value2
            }


        }
    }


    /**
     * getPercentage
     * @param actualValue
     * @param targetValue
     * @returns {number}
     */
    function getPercentage(actualValue, targetValue) {

        if(actualValue <= targetValue){
            var percentage = ( 100 * actualValue ) / targetValue;
            percentage = Math.round(percentage);
            return percentage;
        }
        else{
            var percentage = ( 100 * targetValue ) / actualValue;
            percentage = Math.round(percentage);
            return percentage;
        }

    }

    /**
     * getDegrees
     * @param percentage
     * @returns {number}
     */
    function getDegrees(percentage) {
        var degrees = ( 180 * percentage ) / 125;
        degrees = Math.round(degrees);
        return degrees;
    }

    /**
     * transform
     * @param element
     * @param deg
     */
    function transform(element, deg) {
        element.css({'transform': 'rotate(' + deg + 'deg)'});
    }

    /**
     * drawArcType1 for BY VAL
     * @param statusDetermination
     * @param operationStatus
     * @param actualDegrees
     * @param targetDegrees
     * @param actualValue
     * @param targetValue
     */
    function drawArcType1(statusDetermination,operationStatus,actualDegrees,targetDegrees,actualValue,targetValue){
        yelloCir1.css('z-index', 0);
        yelloCir2.css('z-index', 0);
        greenCir1.css('z-index', 1);
        greenCir2.css('z-index', 1);
        redCir1.css('z-index', 2);
        redCir2.css('z-index', 2);
        drawRedArc(targetDegrees);
        drawFullGreenArc();
        txtActualValue(actualValue);
        txtTargetValue(targetValue);
    }

    /**
     * drawArcType2 for BY VAL
     * @param statusDetermination
     * @param operationStatus
     * @param actualDegrees
     * @param targetDegrees
     * @param actualValue
     * @param targetValue
     */
    function drawArcType2(statusDetermination,operationStatus,actualDegrees,targetDegrees,actualValue,targetValue){
        yelloCir1.css('z-index', 0);
        yelloCir2.css('z-index', 0);
        greenCir1.css('z-index', 2);
        greenCir2.css('z-index', 2);
        redCir1.css('z-index', 1);
        redCir2.css('z-index', 1);
        drawGreenArc(targetDegrees);
        drawFullRedArc();
        txtActualValue(actualValue);
        txtTargetValue(targetValue);
    }

    /**
     * drawArcType3
     * @param statusDetermination
     * @param operationStatus
     * @param actualDegrees
     * @param TV1Degrees
     * @param TV2Degrees
     * @param TV1
     * @param TV2
     * @param actualValue
     */
    function drawArcType3(statusDetermination,operationStatus,actualDegrees,TV1Degrees,TV2Degrees,TV1,TV2,actualValue){
        redCir1.css('z-index', 2);
        redCir2.css('z-index', 2);
        yelloCir1.css('z-index', 1);
        yelloCir2.css('z-index', 1);
        greenCir1.css('z-index', 0);
        greenCir2.css('z-index', 0);
        drawFullGreenArc();
        drawRedArc(TV1Degrees);
        drawYellowArc(TV2Degrees);
        txtActualValue(actualValue);
        txtTargetValueIntoV1(TV1);
        txtTargetValueIntoV2(TV2);

    }

    /**
     * drawArcType4
     * @param statusDetermination
     * @param operationStatus
     * @param actualDegrees
     * @param TV1Degrees
     * @param TV2Degrees
     * @param TV1
     * @param TV2
     * @param actualValue
     */
    function drawArcType4(statusDetermination,operationStatus,actualDegrees,TV1Degrees,TV2Degrees,TV1,TV2,actualValue){
        greenCir1.css('z-index', 2);
        greenCir2.css('z-index', 2);
        yelloCir1.css('z-index', 1);
        yelloCir2.css('z-index', 1);
        redCir1.css('z-index', 0);
        redCir2.css('z-index', 0);
        drawFullRedArc();
        drawGreenArc(TV1Degrees);
        drawYellowArc(TV2Degrees);
        txtActualValue(actualValue);
        txtTargetValueIntoV1(TV1);
        txtTargetValueIntoV2(TV2);
    }

    /**
     * drawRedArc
     * @param actualDegrees
     */
    function drawRedArc(actualDegrees){
        if (actualDegrees > 90) {
            var difference = actualDegrees - 90;
            transform(redCir1, 0);
            transform(redCir2, difference);
        }
        else if (actualDegrees == 90) {
            transform(redCir1, 0);
            transform(redCir2, 0);
        }
        else{
            var difference = actualDegrees - 90;
            transform(redCir1, -90);
            transform(redCir2, difference);
        }
    }


    /**
     * drawYellowArc
     * @param actualDegrees
     */
    function drawYellowArc(degrees){
        if (degrees > 90) {
            var difference = degrees - 90;
            transform(yelloCir1, 0);
            transform(yelloCir2, difference);
        }
        else if (degrees == 90) {
            transform(yelloCir1, 0);
            transform(yelloCir2, 0);
        }
        else{
            var difference = degrees - 90;
            transform(yelloCir1, -90);
            transform(yelloCir2, difference);
        }
    }

    /**
     * drawGreenArc
     * @param actualDegrees
     */
    function drawGreenArc(actualDegrees){
        if (actualDegrees > 90) {
            var difference = actualDegrees - 90;
            transform(greenCir1, 0);
            transform(greenCir2, difference);
        }
        else if (actualDegrees == 90) {
            transform(greenCir1, 0);
            transform(greenCir2, 0);
        }
        else{
            var difference = actualDegrees - 90;
            transform(greenCir1, -90);
            transform(greenCir2, difference);
        }
    }

    /**
     * drawFullGreenArc
     */
    function drawFullGreenArc(){
        transform(greenCir1, 0);
        transform(greenCir2, 90);
    }

    /**
     * drawFullRedArc
     */
    function drawFullRedArc(){
        transform(redCir1, 0);
        transform(redCir2, 90);
    }


    function txtActualValue(val){
        var num = val.toFixed(2);
       //var num = Math.round(val).toFixed(2);
        actualValueTxt.html(num);
    }

    function txtTargetValue(val){
        var num = val.toFixed(2);
       //var num = Math.round(val).toFixed(2);
        targetValueTxt.html(num);
    }

    function txtTargetValueIntoV1(val){
        var num = val.toFixed(2);
       //var num = Math.round(val).toFixed(2);
        targetValueIntoV1Txt.html(num);
    }

    function txtTargetValueIntoV2(val){
       var num = val.toFixed(2);
       //var num = Math.round(val).toFixed(2);
        targetValueIntoV2Txt.html(num);
    }
}

