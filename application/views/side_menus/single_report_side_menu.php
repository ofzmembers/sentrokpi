
<div class="sidemenu-with-padding">


    <form action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi-report/kpi-id/'.$this->uri->segment(6));?>" method="POST">
      <div class="form-group">
        <label class="txt-black">Organization</label>
        <input type="text" class="form-control" id="organization_uid" name="organization_uid" placeholder="Organization UID" value="<?php print $this->uri->segment(3); ?>" disabled="disabled">
      </div>

<?php if(isset($summary_details->kpi_id)): ?>

      <div class="form-group">
        <label class="txt-black">KPI ID</label>
         <?php if(isset($summary_details->kpi_id)): ?>
            <input type="text" class="form-control" id="division_name" name="division_name" placeholder="Division Name" value="<?php print $summary_details->kpi_id; ?>" disabled="disabled"> 
          <?php endif; ?>
      </div>
      <div class="form-group">
        <label class="txt-black">KPI Name</label>
        <?php if(isset($summary_details->kpi_name)): ?>
            <input type="text" class="form-control" id="kpi_name" name="kpi_name" placeholder="Employee Name" value="<?php print $summary_details->kpi_name; ?>" disabled="disabled">
        <?php endif; ?>
      </div>

      <div class="form-group">
        <label class="txt-black">From</label>
             <input type="text" placeholder="Pickup Date" id="datepicker1"  name="datepicker1" class="formfield_text form-control">
      </div>

      <div class="form-group">
        <label class="txt-black">To</label>
        <input type="text" placeholder="Pickup Date" id="datepicker2"  name="datepicker2" class="formfield_text form-control">
      </div>

      <button type="submit" class="btn btn-primary" name="submit">View</button>
     
 <button type="button" class="btn btn-success" name="print" onclick="printDiv('print-report-area')"> <i class="fa fa-print"></i>  Print</button>

  <?php endif; ?>   
     
    </form>


</div>




