<?php
$nextUrl = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/kpi-id/'); 
?>
<div class="sidemenu-with-padding">
    <form method="POST">
      <div class="form-group">
        <label class="txt-black">Organization</label>
        <input type="text" class="form-control" name="organization_uid" id="organization_uid" placeholder="Organization UID" value="<?php print $this->uri->segment(3); ?>" disabled="disabled">
      </div>
      <div class="form-group">
        <label class="txt-black">Division</label>
        <select class="form-control" name="division_id" id="division_id" onchange="getEmployeeListForDivisionReport(this,'<?php print $this->uri->segment(3); ?>','<?php print $nextUrl; ?>')">
            <option value="all"> -- All Divisions -- </option>
            <?php foreach($divisions as $division): ?>   
                    <option value="<?php print $division->division_id; ?>"><?php print $division->division_name; ?></option>       <?php endforeach; ?>
        </select>
      </div>
      <div class="form-group">
        <label class="txt-black">Employee</label>
        <select class="form-control" name="employee_id" id="employee_id" onchange="return getReportsSummary('<?php print $nextUrl; ?>');">
            <option value="all">-- All Employees --</option>
        </select>
      </div>


      <?php if($nextUrl): ?>
        <button type="button" id="getReportSummaryBtn" onclick="return getReportsSummary('<?php print $nextUrl; ?>');" class="btn btn-primary" name="submit">View</button>

       <!--   <button type="button" class="btn btn-success" name="print" onclick="printDiv('report-summary-table-area')"> <i class="fa fa-print"></i>  Print</button> -->

         

          <?php if($kpis):?>
               <button type="button" id="print-preview-btn-php" class="btn btn-success" name="print" onclick="printDiv2('php_table')"> <i class="fa fa-print"></i>  Print Preview</button>
           <?php endif; ?>

           <button type="button" id="print-preview-btn-js" class="btn btn-success" name="print" onclick="printDiv2('report-summary-table-area')"> <i class="fa fa-print"></i>  Print Preview</button>

      <?php endif; ?>
    
     
    </form>
</div>






