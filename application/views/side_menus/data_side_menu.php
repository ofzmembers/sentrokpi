<ul class="nav hidden-xs" id="side-menu">

<?php if ($this->session->userdata('user_type') == 'super_admin' || $this->session->userdata('user_type') == 'sys_admin'  || $this->session->userdata('user_type') == 'manager'): ?>
    <li  <?php
        if($this->uri->segment(4) == 'organization-kpi-list') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/organization-kpi-list/');?>"><i class="fa fa-edit fa-fw"></i> Organization KPI LIst</a>
    </li>

 <?php endif; ?>     
    <li <?php
        if($this->uri->segment(4) == 'division-kpi-list') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division-kpi-list/');?>"><i class="fa fa-edit fa-fw"></i> Division KPI LIst</a>
    </li>
    <li <?php
        if($this->uri->segment(4) == 'employee-kpi-list') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee-kpi-list/');?>"><i class="fa fa-edit fa-fw"></i>Employee KPI LIst</a>
    </li>
</ul>








