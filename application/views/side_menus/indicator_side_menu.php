<div class="sidemenu-with-padding">
    <form  method="POST">
      <div class="form-group">
        <label class="txt-black">Organization</label>
        <input type="text" class="form-control" name="organization_uid" id="organization_uid" placeholder="Organization UID" value="<?php print $this->uri->segment(3); ?>" disabled="disabled">
      </div>
      <div class="form-group">
        <label class="txt-black">Division</label>
        <select class="form-control" name="division_id" id="division_id" onchange="getEmployeeListForDivision(this,'<?php print $this->uri->segment(3); ?>')">
            <option value="all"> -- Please Select -- </option>
            <?php foreach($divisions as $division): ?>                                
                <option value="<?php print $division->division_id; ?>"><?php print $division->division_name; ?></option>             
            <?php endforeach; ?>
        </select>
      </div>
      <div class="form-group">
        <label class="txt-black">Employee</label>
        <select class="form-control" name="employee_id" id="employee_id">
            <option value="all">-- please select --</option>
        </select>
      </div>
      <button type="button" onclick="return getIndicators();" class="btn btn-primary" name="submit">View</button>
      <button type="button" id="print-preview-btn-php" class="btn btn-success" name="print" onclick="printDiv('indicator-view')"> <i class="fa fa-print"></i>  Print </button>
    </form>
</div>









