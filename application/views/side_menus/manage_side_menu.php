<ul class="nav hidden-xs" id="side-menu">

<?php if ($this->session->userdata('user_type') == 'super_admin' || $this->session->userdata('user_type') == 'sys_admin'): ?>
    <li  <?php
        if($this->uri->segment(4) == 'manager') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/manager/');?>"><i class="fa fa-table fa-fw"></i> Manager</a>
    </li>
<?php endif; ?>      
    <li  <?php
        if($this->uri->segment(4) == 'division') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division/');?>"><i class="fa fa-edit fa-fw"></i> Division</a>
    </li>
    <li <?php
        if($this->uri->segment(4) == 'division_head') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division_head/');?>"><i class="fa fa-edit fa-fw"></i> Division Head</a>
    </li>
    <li <?php
        if($this->uri->segment(4) == 'employee') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee/');?>"><i class="fa fa-edit fa-fw"></i>Employee</a>
    </li>
    <li <?php
        if($this->uri->segment(4) == 'kpi') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi/');?>"><i class="fa fa-edit fa-fw"></i>KPI</a>
    </li>
    <li <?php
        if($this->uri->segment(4) == 'assign_kpi_to_division') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_division/');?>"><i class="fa fa-edit fa-fw"></i>Assign KPI to Division</a>
    </li>
    <li <?php
        if($this->uri->segment(4) == 'assign_kpi_to_employee') {
            echo 'class="active"';
        } ?>>
        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_employee/');?>"><i class="fa fa-edit fa-fw"></i>Assign KPI to Employee</a>
    </li>
</ul>








