<header class="navbar-fixed-top">
    <?php $this->load->view('templates/notification_panel'); ?>
    <!-- Navigation -->
    <nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li <?php if( $main_view == 'pages/dashboard' ) { echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>dashboard/">Dashboard</a></li>


                     <?php if ($this->session->userdata('user_type') == 'super_admin' || $this->session->userdata('user_type') == 'sys_admin' || $this->session->userdata('user_type') == 'manager'): ?>
                    <li <?php if( $this->uri->segment(2) == 'report' ) { echo 'class="active"'; } ?>  ><a href="<?php print site_url('dashboard/report/organizations/') ?>">Reports</a></li>
                     <?php endif; ?> 

                    <?php if ($this->session->userdata('user_type') == 'super_admin'  || $this->session->userdata('user_type') == 'sys_admin'): ?>
                    <li <?php if( $this->uri->segment(2) == 'data' ) { echo 'class="active"'; } ?> ><a href="<?php print site_url('dashboard/data/organizations/') ?>">Data</a></li>
                    <?php endif; ?> 

                     <?php if ($this->session->userdata('user_type') == 'manager' || $this->session->userdata('user_type') == 'division_head'): ?>
                    <li <?php if( $this->uri->segment(2) == 'data' ) { echo 'class="active"'; } ?> ><a href="<?php print site_url('dashboard/data/'.$this->session->userdata('organization_uid').'/division-kpi-list/');?> ">Data</a></li>
                    <?php endif; ?>


                   <?php if ($this->session->userdata('user_type') == 'super_admin' || $this->session->userdata('user_type') == 'sys_admin'): ?>
                        <li <?php if( $this->uri->segment(2) == 'manage') { echo 'class="active"'; } ?>  ><a href="<?php print site_url('dashboard/manage/organizations/') ?>">Manage</a></li>
                        <?php endif; ?> 
                        <?php if ($this->session->userdata('user_type') == 'manager'): ?>
                        <li <?php if( $this->uri->segment(2) == 'manage') { echo 'class="active"'; } ?>  ><a href="<?php print site_url('dashboard/manage/'.$this->session->userdata('organization_uid').'/division/');?>">Manage</a></li>
                    <?php endif; ?>   
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav><!-- / Navigation -->
</header>



