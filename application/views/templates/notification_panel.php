<div class="header-status-bar">
    <div class="container-fluid">
        <a class="navbar-brand" href="<?php echo base_url(); ?>dashboard/">Sentro KPI</a>

       
        <ul class="nav navbar-top-links navbar-right" id="notification-panel">
            <li class="dropdown dropdown-title">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-question-circle fa-fw"></i>  <span class="hidden-xs">Help</span> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="#"><i class="fa fa-user fa-fw"></i> Help Center</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-phone fa-fw"></i> Contact Support</a>
                    </li>
                </ul><!-- /.dropdown-help -->
            </li><!-- /.dropdown -->
            <li class="dropdown dropdown-title">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <span class="hidden-xs"> <?php print $this->session->userdata('username'); ?></span> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="<?php print site_url('profile/');?>"><i class="fa fa-user fa-fw"></i> My Details</a>
                    </li>
                    <li>
                        <a href="<?php print site_url('password/');?>"><i class="fa fa-gear fa-fw"></i> Change Password</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?php echo site_url('login/logout');?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul><!-- /.dropdown-user -->
            </li><!-- /.dropdown -->
        </ul><!-- /.navbar-top-links -->
    </div><!-- container-->
</div><!-- header-status-bar -->