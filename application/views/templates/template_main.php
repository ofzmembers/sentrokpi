<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sentro KPI</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print base_url('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php print base_url('assets/bower_components/metisMenu/dist/metisMenu.min.css');?>" rel="stylesheet">
    <!-- Timeline CSS -->
    <link href="<?php print base_url('assets/bower_components/admin-theme/dist/css/timeline.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php print base_url('assets/bower_components/admin-theme/dist/css/sb-admin-2.css');?>" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="<?php print base_url('assets/bower_components/morrisjs/morris.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php print base_url('assets/bower_components/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">

    <link href="<?php print base_url('assets/bower_components/jquery-ui/themes/base/jquery-ui.css');?>" rel="stylesheet" type="text/css">

    <link href="<?php print base_url('assets/css/responsive-gauge-meter-kpi.css');?>" rel="stylesheet">

    <link href="<?php print base_url('assets/css/styles.css');?>" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div id="loading_sk">
    <div class="spinner" >
        <img src="<?php print base_url('assets/img/preloader.gif');?>" class="img-responsive center-block" id="">
    </div>
</div><!--   preloader area-->
<div id="wrapper">

    <?php $this->load->view($header_view); ?>

    <!-- Page Content -->
    <div id="full-page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <?php $this->load->view($main_view); ?>
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="<?php print base_url('assets/bower_components/jquery/dist/jquery.min.js');?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php print base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php print base_url('assets/bower_components/metisMenu/dist/metisMenu.min.js');?>"></script>
<!-- Morris Charts JavaScript -->
<script src="<?php print base_url('assets/bower_components/raphael/raphael-min.js');?>"></script>
<script src="<?php print base_url('assets/bower_components/morrisjs/morris.min.js');?>"></script>
<script src="<?php print base_url('assets/js/morris-data.js');?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php print base_url('assets/bower_components/admin-theme/dist/js/sb-admin-2.js');?>"></script>

<script type="text/javascript" src="<?php print base_url('assets/js/vendor/raphael-min.js');?>"></script>

<script type="text/javascript" src="<?php print base_url('assets/js/kuma-gauge.jquery.js');?>"></script>

<script type="text/javascript" src="<?php print base_url('assets/bower_components/jquery-ui/jquery-ui.js');?>"></script>

<script type="text/javascript" src="<?php print base_url('assets/js/responsive-gauge-meter-kpi.js');?>"></script>

 <script type="text/javascript" src="<?php print base_url('assets/bower_components/jQuery.print/jQuery.print.js');?>"></script>

 <script type="text/javascript" src="<?php print base_url('assets/bower_components/moment/min/moment.min.js');?>"></script>

<script type="text/javascript">
    var BASE_URL = "<?php echo base_url();?>";
</script>

<script type="text/javascript">
  

    $(window).load(function(e) {
       $("#loading_sk").fadeOut("fast");
    });

    function loadPreloader(){
      $("#loading_sk").fadeIn("fast");
    }
     function hidePreloader(){
      $("#loading_sk").fadeOut("fast");
    } 

  


    //date picker
    $("#datepicker1").datepicker({
         dateFormat: "yy-mm-dd"
    });
  
    //date picker
    $("#datepicker2").datepicker({
         dateFormat: "yy-mm-dd"
    });  
       
   
      
   
    // $(document).ready(function(){
    //     console.log("doc ready");
    //     var elementExists = document.getElementById("getReportSummaryBtn");
    //     if(elementExists){
    //         console.log("yes");
    //     }else{
    //         console.log("no");
    //     }
    // });
    // 
    //


   
</script>
<script src="<?php print base_url('assets/js/app.js');?>"></script>


</body>

</html>
