<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sentro KPI</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="<?php print site_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet"  type='text/css' media="all">
    <link href="<?php print site_url('assets/css/styles.css');?>" rel="stylesheet"  type='text/css' media="all">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="comming-soon-bg">
    <div class="pattern"></div>
    <div class="container">
        <header class="navbar-fixed-top">
            <div class="header-status-bar">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">Sentro KPI</a>
                </div><!-- container-->
            </div><!-- header-status-bar -->
        </header>
        <h1 class="text-center" id="large-header"> Sentro KPI</h1>
        <h3 class="text-center" id="small-header"> Comming Soon</h3>

    </div>
    <?php $this->load->view('templates/footer'); ?>
    <!-- jQuery -->
    <script src="<?php print site_url('assets/bower_components/jquery/dist/jquery.min.js');?>"></script>
    <!-- bootstrap js -->
    <script src="<?php print site_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
</body>
</html>
