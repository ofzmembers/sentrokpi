<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i>  Division KPI
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
         <form  method="post" accept-charset="utf-8" action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>">
            <div class="form-group filter-area row">
                <label class="col-sm-2 control-label">Filter by Division</label>
                <div class="col-sm-3">
                    <select name="division_id" class="form-control" onchange="this.form.submit()">
                         <option selected="selected" value="all"> All Divisions </option>
                        <?php foreach($divisions as $division): ?>
                            <?php if($division->division_id == $selected_division_id): ?>
                                <option selected="selected" value="<?php print $division->division_id;?>" ><?php print $division ->division_name; ?></option>
                            <?php else: ?>
                                <option value="<?php print $division->division_id;?>" ><?php print $division ->division_name; ?></option>
                            <?php endif;?>

                           
                        <?php endforeach; ?>
                    </select>
                </div>
                
            </div>   
        </form>   
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text">Division KPI </h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                      
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">

                <?php if(!$kpi_to_divisions): ?>
                    <p class="bg-primary simple-msg text-center">
                        First you'll need to assign KPIs to divisions. <i class="fa fa-arrow-circle-o-up fa-2 pull-right"></i>
                    </p>
                <?php endif; ?>

                <?php if($kpi_to_divisions): ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>KPI ID</th>
                                <th>KPI Name</th>
                                <th>Division Name</th>
                                <th>Repeat Duration</th>
                                <th>Target Value</th>
                                <th>Actual Value</th>
                                
                                <th colspan="2" class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($kpi_to_divisions as $kpi_to_division): ?>
                                    <tr>
                                        <td class="text-td"><?php print $kpi_to_division->kpi_id; ?></td>
                                        <td class="text-td"><?php print $kpi_to_division->kpi_name; ?></td>
                                        <td class="text-td"><?php print $kpi_to_division->division_name; ?></td>
                                         <td class="text-td">
                                             <?php 
                                                if($kpi_to_division->repeat_duration == 1){
                                                    print 'Daily';
                                                }
                                                else if($kpi_to_division->repeat_duration == 2){
                                                    print 'Weekly';
                                                }
                                                else if($kpi_to_division->repeat_duration == 3){
                                                     print 'Monthly';
                                                }
                                                else if($kpi_to_division->repeat_duration == 4){
                                                     print 'Quaterly';
                                                }
                                                else if($kpi_to_division->repeat_duration == 5){
                                                    print 'Annually';
                                                }
                                            ?>
                                        </td>
                                        <td class="text-td"><?php print $kpi_to_division->target_value; ?></td>
                                       
	                                        <td class="text-td">

                                                <?php if($kpi_to_division->actual_value): ?>
                                                    <input type="text" name="actual_value" value="<?php print $kpi_to_division->actual_value; ?>" id="act_val<?php print $kpi_to_division->association_id; ?>"></input>    
                                                <?php else: ?>
                                                    <input type="text" name="actual_value" id="act_val<?php print $kpi_to_division->association_id; ?>">
                                                <?php endif; ?>
	                                        	
	                                        </td>
	                                        <td>
	                                        	<input type="hidden" name="association_id" value="<?php print $kpi_to_division->association_id; ?>">
	                                            <div class="btn-group cust-group" role="group">

                                                    <?php if($kpi_to_division->actual_value): ?>
                                                        <button type="button" id="btn<?php print $kpi_to_division->association_id;?>" class="btn btn-success" onclick="return update_division_actual_row_value('<?php print $kpi_to_division->association_id; ?>','<?php print $this->uri->segment(3); ?>');"> <i class="fa fa-check"></i> </button>
                                                    <?php else: ?> 
                                                        <button type="button" id="btn<?php print $kpi_to_division->association_id;?>" class="btn btn-success" onclick="return update_division_actual_row_value('<?php print $kpi_to_division->association_id; ?>','<?php print $this->uri->segment(3); ?>');">Done </button>
                                                    <?php endif; ?>
	                                              
	                                            </div>
	                                        </td>
                                            <td>
                                                <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/edit/'.$kpi_to_division->association_id); ?>"  class="btn btn-warning" > Enter Privious Data</a>
                                            </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->