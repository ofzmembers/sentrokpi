<?php

    print_r($kpi_to_divisions_pre_data);
?>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i>  Employee KPI
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text">Employee KPI Add Previous Records  </h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                          <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee-kpi-list/edit/'.$this->uri->segment(6));?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>



                        <?php if($kpi_to_divisions_pre_data): ?>

                            <?php
                                $repeat_duration = "";
                                if($kpi_to_divisions_pre_data->repeat_duration == 1){
                                    $repeat_duration = 'Daily';
                                }
                                else if($kpi_to_divisions_pre_data->repeat_duration == 2){
                                    $repeat_duration = 'Weekly';
                                }
                                else if($kpi_to_divisions_pre_data->repeat_duration == 3){
                                    $repeat_duration = 'Monthly';
                                }
                                else if($kpi_to_divisions_pre_data->repeat_duration == 4){
                                    $repeat_duration = 'Quaterly';
                                }
                                else if($kpi_to_divisions_pre_data->repeat_duration == 5){
                                    $repeat_duration = 'Annually';
                                }
                            ?>

                        <form class="form-horizontal" action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee-kpi-list/previous-data/'.$this->uri->segment(6));?>" method="post">
                            <div class="form-group">
                                <label for="kpi_id" class="col-sm-4 control-label">KPI ID</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kpi_id" placeholder="KPI ID" value="<?php print $kpi_to_divisions_pre_data->kpi_id; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kpi_name" class="col-sm-4 control-label">KPI Name</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kpi_name" placeholder="KPI Name" value="<?php print $kpi_to_divisions_pre_data->kpi_name; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="division_name" class="col-sm-4 control-label">Division Name</label>
                                <div class="col-sm-5">


                                    <input type="text" class="form-control" name="division_name" placeholder="Division Name" value="<?php print $kpi_to_divisions_pre_data->division_name; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="repeat_duration" class="col-sm-4 control-label">Repeat Duration</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="repeat_duration" placeholder="Repeat Duration" value="<?php print $repeat_duration; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="target velue data" class="col-sm-4 control-label">Target Value</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="target velue data" placeholder="Target Value" value="<?php print $kpi_to_divisions_pre_data->target_value; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="datepicker" class="col-sm-4 control-label">Date</label>
                                <div class="col-sm-5">
                                    <?php if($kpi_to_divisions_pre_data->repeat_duration == 1): ?>
                                        <input type="text" placeholder="Pickup Date" id="datepicker_daily"  name="datepicker_daily" class="formfield_text form-control">
                                    <?php endif;?>
                                    <?php if($kpi_to_divisions_pre_data->repeat_duration == 2): ?>
                                        <input type="text" placeholder="Pickup Date" id="datepicker_weekly"  name="datepicker_weekly" class="formfield_text form-control">
                                    <?php endif;?>
                                    <?php if($kpi_to_divisions_pre_data->repeat_duration == 3): ?>
                                        <input type="text" placeholder="Pickup Date" id="datepicker_monthly"  name="datepicker_monthly" class="formfield_text form-control">
                                    <?php endif;?>
                                    <?php if($kpi_to_divisions_pre_data->repeat_duration == 4): ?>
                                        <input type="text" placeholder="Pickup Date" id="datepicker_quaterly"  name="datepicker_quaterly" class="formfield_text form-control">
                                    <?php endif;?>
                                    <?php if($kpi_to_divisions_pre_data->repeat_duration == 5): ?>
                                        <input type="text" placeholder="Pickup Date" id="datepicker_yearly"  name="datepicker_yearly" class="formfield_text form-control">
                                    <?php endif;?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="actual_value" class="col-sm-4 control-label">Actual Value</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="actual_value" placeholder="Actual Value">
                                </div>
                            </div>

                            <input type="hidden" name="sum_or_avg_target_value"  value="<?php print $kpi_to_divisions_pre_data->sum_or_avg_target_value; ?>">
                            <input type="hidden" name="accumulated_actual_value"  value="<?php print $kpi_to_divisions_pre_data->accumulated_actual_value; ?>">
                            <input type="hidden" name="target_value"  value="<?php print $kpi_to_divisions_pre_data->target_value; ?>">
                            <input type="hidden" name="association_id"  value="<?php print $kpi_to_divisions_pre_data->association_id; ?>">


                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div><!-- form-group  -->

                        </form><!-- end of form -->

                        <?php endif; ?>
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->