<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Profile
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Change Password
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Change Password</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                      
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
            	 <p class="bg-success">
                    <?php if($this->session->flashdata('user_updated')): ?>
                        <?php echo $this->session->flashdata('user_updated'); ?>
                    <?php endif; ?>
                   
                </p>

                <p class='bg-danger'>
 					<?php if($this->session->flashdata('password_change_fail')): ?>
						<?php echo $this->session->flashdata('password_change_fail'); ?>
                    <?php endif ?>
                </p>
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>
                        <form class="form-horizontal" action="<?php print site_url('password/');?>" method="post">
                            <div class="form-group">
                                <label for="first_name" class="col-sm-4 control-label">Current Password</label>
                                <div class="col-sm-5">
                                	<input type="password" class="form-control" name="current_password" placeholder="Current Password">
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <label for="last_name" class="col-sm-4 control-label">New Password</label>
                                <div class="col-sm-5">
									<input type="password" class="form-control" name="new_password" placeholder="New Password">
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <label for="username" class="col-sm-4 control-label">Confirm Password</label>
                                <div class="col-sm-5">
                                   <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a href="<?php print site_url('dashboard/');?>" class="btn btn-default">Back</a>
                                </div>
                            </div><!-- form-group  -->

                        </form><!-- end of form -->
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->