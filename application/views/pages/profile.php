<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Profile
            </li>
            <li class="active">
                <i class="fa fa-table"></i> <?php print $this->session->userdata('username'); ?>
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Profile</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                      
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
            	 <p class="bg-success">
                    <?php if($this->session->flashdata('user_updated')): ?>
                        <?php echo $this->session->flashdata('user_updated'); ?>
                    <?php endif; ?>
                </p>
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>
                        <form class="form-horizontal" action="<?php print site_url('profile/');?>" method="post">
                            <div class="form-group">
                                <label for="first_name" class="col-sm-4 control-label">First Name</label>
                                <div class="col-sm-5">
                                	<?php if($user_info->first_name): ?>
                                    	<input type="text" class="form-control" name="first_name" value="<?php print $user_info->first_name; ?>" placeholder="First Name">
                                	<?php else: ?>
										<input type="text" class="form-control" name="first_name" placeholder="First Name">
                                	<?php endif; ?>
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <label for="last_name" class="col-sm-4 control-label">Last Name</label>
                                <div class="col-sm-5">
                                	<?php if($user_info->last_name): ?>
                                    	<input type="text" class="form-control" name="last_name" value="<?php print $user_info->last_name; ?>" placeholder="Last Name">
                                    <?php else: ?>
										 <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                                	<?php endif; ?>
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <label for="username" class="col-sm-4 control-label">Username</label>
                                <div class="col-sm-5">
                                    <?php if($user_info->username): ?>
                                    	 <input type="text" class="form-control" name="username"  value="<?php print $user_info->username; ?>"  placeholder="Username">
                                    <?php else: ?>
										  <input type="text" class="form-control" name="username" placeholder="Username">
                                	<?php endif; ?>
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a href="<?php print site_url('dashboard/');?>" class="btn btn-default">Back</a>
                                </div>
                            </div><!-- form-group  -->

                        </form><!-- end of form -->
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->