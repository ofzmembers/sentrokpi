

 <div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default" id="error-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Error</h3>
            </div>
            <div class="panel-body">
                <h1 class="page-header text-center">Unauthorized Access</h1>
		        <p class="text-center">
		        	Access denied!
		        </p>
            </div>
        </div>
    </div>
</div>