

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Dashboard</h1>
        <p>


        </p>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->




    <!-- Organization panel-->
    <div class="row">

        <?php if (isset($organizations)): //super admin /sys admin ?>
         <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-8">
                            <i class="fa fa-bell fa-fw"></i> Organizations Panel
                        </div><!-- -->
                        <div class="col-sm-4">
                            <a href="<?php print site_url('dashboard/manage/organizations/'); ?>" class="btn btn-xs btn-info pull-right bg-create"><i class="fa fa-plus"></i> Manage All</a>
                        </div><!-- -->
                    </div><!-- -->
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="list-group">
                        <?php foreach($organizations as $organization): ?>
                            <li class="list-group-item clearfix">
                                <i class="fa fa-building"></i>
                                <?php print $organization->organization_name; ?>
                                <span class="pull-right">
                                    <a href="<?php print site_url('dashboard/manage/'.$organization->organization_uid.'/manager/'); ?>" class="btn btn-xs btn-info">Manage</a>
                                </span>
                            </li><!-- list-group-item -->
                        <?php endforeach; ?>
                    </ul><!-- /.list-group -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-5 -->
        <?php endif; ?>




        <?php if (isset($organizations)): //super admin /sys admin ?>
            <!-- Data panel-->
             <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-8">
                                <i class="fa fa-bell fa-fw"></i> Data Panel
                            </div><!-- -->
                            <div class="col-sm-4">
                                <a href="<?php print site_url('dashboard/data/organizations/'); ?>" class="btn btn-xs btn-info pull-right bg-create"><i class="fa fa-plus"></i> Manage All Data</a>
                            </div><!-- -->
                        </div><!-- -->
                    </div><!-- /.panel-heading -->
                    <div class="panel-body">
                        <ul class="list-group">
                            <?php foreach($organizations as $organization): ?>
                                <li class="list-group-item clearfix">
                                    <i class="fa fa-building"></i>
                                    <?php print $organization->organization_name; ?>
                                    <span class="pull-right">
                                        <a href="<?php print site_url('dashboard/data/'.$organization->organization_uid.'/organization-kpi-list/'); ?>" class="btn btn-xs btn-info">Manage</a>
                                    </span>
                                </li><!-- list-group-item -->
                            <?php endforeach; ?>
                        </ul><!-- /.list-group -->
                    </div><!-- /.panel-body -->
                </div><!-- /.panel -->
            </div><!-- /.col-lg-5 -->

            <!-- End of Organization panel-->
        <?php endif; ?>


    </div>
    <!-- End of Organization panel-->




    <!-- Organization single panel-->
    <div class="row">
    <?php if ($this->session->userdata('user_type') == 'manager'): //manager only ?>

    <?php if (isset($organization_single) && isset($divisions)): ?>
         <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-8">
                            <i class="fa fa-bell fa-fw"></i> Organization Panel
                        </div><!-- -->
                    </div><!-- -->
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="list-group">
                        <?php foreach($organization_single as $organization): ?>
                            <li class="list-group-item clearfix">
                                <i class="fa fa-building"></i>
                                <?php print $organization->organization_name; ?>
                                <span class="pull-right">
                                    <a href="<?php print site_url('dashboard/manage/'.$organization->organization_uid.'/division/'); ?>" class="btn btn-xs btn-info">Manage</a>
                                </span>
                            </li><!-- list-group-item -->
                        <?php endforeach; ?>
                    </ul><!-- /.list-group -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-5 -->
     <?php endif; ?>

 <?php endif; ?>

 <?php if (isset($organization_single) && isset($divisions)): // division head /manager?>

         <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-8">
                            <i class="fa fa-bell fa-fw"></i> Data Panel
                        </div><!-- -->
                    </div><!-- -->
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="list-group">
                        <?php foreach($organization_single as $organization): ?>
                            <li class="list-group-item clearfix">
                                <i class="fa fa-building"></i>
                                <?php print $organization->organization_name; ?>

                                <span class="pull-right">
                                    <a href="<?php print site_url('dashboard/data/'.$organization->organization_uid.'/division-kpi-list/'); ?>" class="btn btn-xs btn-info">Manage Data</a>
                                </span>
                            </li><!-- list-group-item -->
                        <?php endforeach; ?>
                    </ul><!-- /.list-group -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-5 -->
     <?php endif; ?>

    </div>
    <!-- End of Organization panel-->



    <!-- Reports single panel-->
     <!-- Organization panel-->
    <div class="row">

        <?php if (isset($organizations)): //super admin /sys admin ?>
         <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-8">
                            <i class="fa fa-bell fa-fw"></i> Reports Panel
                        </div><!-- -->
                        <div class="col-sm-4">
                            <a href="<?php print site_url('dashboard/report/organizations/'); ?>" class="btn btn-xs btn-info pull-right bg-create"><i class="fa fa-plus"></i> View All</a>
                        </div><!-- -->
                    </div><!-- -->
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="list-group">
                        <?php foreach($organizations as $organization): ?>
                            <li class="list-group-item clearfix">
                                <i class="fa fa-building"></i>
                                <?php print $organization->organization_name; ?>
                                <span class="pull-right">
                                    <a href="<?php print site_url('dashboard/report/'.$organization->organization_uid.'/kpi-report/'); ?>" class="btn btn-xs btn-info">View</a>
                                </span>
                            </li><!-- list-group-item -->
                        <?php endforeach; ?>
                    </ul><!-- /.list-group -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-5 -->
        <?php endif; ?>
        </div>


         <!-- Organization single panel-->
    <div class="row">
    <?php if ($this->session->userdata('user_type') == 'manager'): //manager only ?>

    <?php if (isset($organization_single) && isset($divisions)): ?>
         <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-8">
                            <i class="fa fa-bell fa-fw"></i> Reports Panel
                        </div><!-- -->
                    </div><!-- -->
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="list-group">
                        <?php foreach($organization_single as $organization): ?>
                            <li class="list-group-item clearfix">
                                <i class="fa fa-building"></i>
                                <?php print $organization->organization_name; ?>
                                <span class="pull-right">
                                    <a href="<?php print site_url('dashboard/report/'.$organization->organization_uid.'/kpi-report/'); ?>" class="btn btn-xs btn-info">View</a>
                                </span>
                            </li><!-- list-group-item -->
                        <?php endforeach; ?>
                    </ul><!-- /.list-group -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-5 -->
     <?php endif; ?>

 <?php endif; ?>

 </div>
    <!-- End of Reports panel-->
