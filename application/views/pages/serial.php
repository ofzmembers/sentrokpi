<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Serials
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Serials</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                        <a href="<?php print site_url('serial/generate');?>" class="create-item-btn pull-right"><i class="fa fa-plus"></i>Create</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
            	 <p class="bg-success">
                    <?php if($this->session->flashdata('serial_generated')): ?>
                        <?php echo $this->session->flashdata('serial_generated'); ?>
                    <?php endif; ?>
                </p>
                <div class="container-fluid">
                    <div class="row">

                        <?php if(!$serials): ?>
                            <p class="bg-primary simple-msg text-center">
                                First you'll need to create serials. <i class="fa fa-arrow-circle-o-up fa-2 pull-right"></i>
                            </p>
                        <?php endif; ?>

                        <?php if($serials): ?>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Key ID</th>
                                        <th width="30%">Serial Key</th>
                                        <th>Serial Key Name</th>
                                        <th>Org ID</th>
                                        <th>Organization Name</th>
                                        <th>Active Status</th>
                                        <th colspan="2" class="text-right">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($serials as $serial): ?>
                                        <tr>
                                            <td class="text-td"><?php print $serial->serial_id; ?></td>
                                            <td class="text-td"><?php print $serial->serial_key; ?></td>
                                            <td class="text-td"><?php print $serial->serial_key_name; ?></td>
                                            <td class="text-td"><?php print $serial->org_id; ?></td>
                                            <td class="text-td"><?php print $serial->org_name; ?></td>
                                            <td class="text-td"><?php print $serial->active_status; ?></td>
                                            <td>
                                                <div class="btn-group cust-group" role="group">
                                                    <a class="btn btn-warning" href="#">
                                                        <i class="fa fa-check"></i>
                                                    </a>
                                                    <a class="btn btn-danger" onclick="return doconfirm();" href="#">
                                                        <i class="fa fa-times" ></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php endif; ?>
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->