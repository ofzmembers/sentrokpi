<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sentro KPI</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print base_url('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php print base_url('assets/bower_components/metisMenu/dist/metisMenu.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php print base_url('assets/bower_components/admin-theme/dist/css/sb-admin-2.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php print base_url('assets/bower_components/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php print base_url('assets/css/styles.css');?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="comming-soon-bg">
 <div class="pattern"></div>
<div class="container">
    <header class="navbar-fixed-top">
            <div class="header-status-bar">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">Sentro KPI</a>
                </div><!-- container-->
            </div><!-- header-status-bar -->
    </header>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Change Password</h3>
                </div>
                <div class="panel-body">
                    <p class="bg-danger">
                        <?php if($this->session->flashdata('login_failed')): ?>
                            <?php echo $this->session->flashdata('login_failed'); ?>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('no_access')): ?>
                            <?php echo $this->session->flashdata('no_access'); ?>
                        <?php endif; ?>
                          <?php if($this->session->flashdata('manager_password_updated')): ?>
                            <?php echo $this->session->flashdata('manager_password_updated'); ?>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('manager_password_updated_fail')): ?>
                            <?php echo $this->session->flashdata('manager_password_updated_fail'); ?>
                        <?php endif; ?>
                    </p>
                    <form role="form"  action="<?php print site_url('setpassword/token/'.$this->uri->segment(3)); ?>" method="post">
                        <fieldset>
                            <div class="form-group">
								<input type="password" class="form-control" name="new_password" placeholder="New Password">
                            </div><!-- form-group  -->
                             <div class="form-group">           
                                 <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
                            </div><!-- form-group  -->
                           
                            <!-- Change this to a button or input when using this as a form -->
                            <button class="btn btn-lg btn-success btn-block">Set Password</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="<?php print base_url('assets/bower_components/jquery/dist/jquery.min.js');?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php print base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php print base_url('assets/bower_components/metisMenu/dist/metisMenu.min.js');?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php print base_url('assets/bower_components/admin-theme/dist/js/sb-admin-2.js');?>"></script>
</body>
</html>
