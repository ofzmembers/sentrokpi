
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Serial
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Generate
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Serial Generate</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                        <a href="<?php print site_url('serial/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
            	 <p class="bg-success">
                    <?php if($this->session->flashdata('user_updated')): ?>
                        <?php echo $this->session->flashdata('user_updated'); ?>
                    <?php endif; ?>
                </p>
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>
                        <form class="form-horizontal" action="<?php print site_url('serial/generate');?>" method="post">

                            <div class="form-group">
                                <label for="division" class="col-sm-4 control-label">Organization</label>
                                <div class="col-sm-5 selectContainer">
                                    <select class="form-control"  name="organization" id="organization">
                                        <option value="" disabled selected> -- Please Select -- </option>
                                        <?php foreach($organizations as $organization): ?>
                                            <option value="<?php print $organization->organization_id; ?>" ><?php print $organization->organization_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <button type="submit" class="btn btn-primary">Generate And Download</button>
                                </div>
                            </div><!-- form-group  -->

                        </form><!-- end of form -->
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->