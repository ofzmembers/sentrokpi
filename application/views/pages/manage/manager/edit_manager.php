
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Manager
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Edit Manager</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/manager/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>
                            <form class="form-horizontal" action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/manager/edit/'.$manager_details->manager_id);?>" method="post">
                            <div class="form-group">
                                <label for="manager_id" class="col-sm-4 control-label">Manager ID</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="manager_id" value="<?php print $manager_details->manager_id ; ?>" placeholder="Manager ID" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="first_name" class="col-sm-4 control-label">First Name</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="first_name" value="<?php print $manager_details->first_name ; ?>" placeholder="First Name">
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <label for="last_name" class="col-sm-4 control-label">Last Name</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="last_name" value="<?php print $manager_details->last_name ; ?>" placeholder="Last Name">
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <label for="username" class="col-sm-4 control-label">Username</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="username"  value="<?php print $manager_details->username ; ?>" placeholder="Username">
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <label for="email" class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-5">
                                    <input type="email" class="form-control" name="email"  value="<?php print $manager_details->email ; ?>" placeholder="Email">
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <label for="organization_name" class="col-sm-4 control-label">Organization Unique ID</label>
                                <div class="col-sm-5">
                                    <input type="email" class="form-control" name="organization_uid" value="<?php print $this->uri->segment(3); ?>" disabled="disabled">
                                </div>
                            </div><!-- form-group  -->
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div><!-- form-group  -->
                        </form><!-- end of form -->
                       
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->