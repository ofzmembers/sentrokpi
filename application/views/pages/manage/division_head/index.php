
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Division Head
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text">Division Head</h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division_head/create');?>" class="create-item-btn pull-right"><i class="fa fa-plus"></i>Create</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">

                <p class="bg-success">
                    <?php if($this->session->flashdata('division_head_created')): ?>
                        <?php echo $this->session->flashdata('division_head_created'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('division_head_updated')): ?>
                        <?php echo $this->session->flashdata('division_head_updated'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('division_head_deleted')): ?>
                        <?php echo $this->session->flashdata('division_head_deleted'); ?>
                    <?php endif; ?>
                </p>

                <?php if(!$division_heads): ?>
                    <p class="bg-primary simple-msg text-center">
                        First you'll need to add some Division Heads. <i class="fa fa-arrow-circle-o-up fa-2 pull-right"></i>
                    </p>
                <?php endif; ?>

                <?php if($division_heads): ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Division Head ID</th>
                                <th>Full Name</th>
                                <th>Division Name</th>
                                <th>Email</th>
                                <th colspan="2" class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($division_heads as $division_head): ?>
                                    <tr>
                                        <td class="text-td"><?php print $division_head->division_head_id; ?></td>
                                        <td class="text-td">
                                        	<?php print $division_head->first_name; ?> &nbsp;
                                        	<?php print $division_head->last_name; ?>
                                        </td>
                                        
                                        <td class="text-td"><?php print $division_head->division_name; ?></td>
                                        <td class="text-td"><?php print $division_head->email; ?></td>
                                        <td>
                                            <div class="btn-group cust-group" role="group">
                                                <a class="btn btn-warning" 
                                                href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/edit/'.$division_head->division_head_id); ?>"><i class="fa fa-pencil"></i> </a>
                                                <a class="btn btn-danger" onclick="return doconfirm();" 
                                                href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/delete/'.$division_head->division_head_id); ?>">
                                                <i class="fa fa-trash-o" ></i> </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->