<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
            <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Division Head
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Create Division Head</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division_head/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
				<?php if(!$divisions): ?>
					<p class="bg-primary simple-msg text-center">
						First you'll need to add some divisions. <i class="fa fa-arrow-circle-o-up fa-2 pull-right"></i>
					</p>
				<?php endif; ?>
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>
                        <form class="form-horizontal" action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division_head/create');?>" method="post">
                            
	
                		<?php if($divisions): ?>
				
							<div class="form-group">
								<label for="first_name" class="col-sm-4 control-label">First Name</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" name="first_name" placeholder="First Name">
								</div>
							</div>
							<div class="form-group">
								<label for="last_name" class="col-sm-4 control-label">Last Name</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" name="last_name" placeholder="Last Name">
								</div>
							</div>
							<div class="form-group">
								<label for="username" class="col-sm-4 control-label">Username</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" name="username" placeholder="Username">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-4 control-label">Email</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" name="email" placeholder="Email">
								</div>
							</div>
							<div class="form-group">
								<label for="division" class="col-sm-4 control-label">Division</label>
								<div class="col-sm-5 selectContainer">
									<select class="form-control" name="division_name">
										<option value="">Select</option>
										<?php foreach($divisions as $division): ?>
											<option value="<?php print $division->division_id; ?>"><?php print $division->division_name; ?></option>                                 
										<?php endforeach; ?>	
									</select>
								</div>
							</div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div><!-- form-group  -->

  						<?php endif; ?>

                        </form><!-- end of form -->
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->