<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
            <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Employee
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Edit Employee</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>
                        <form class="form-horizontal" action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee/edit/'.$employee_details->employee_id);?>" method="post">
                            
                            <div class="form-group">
                                <label for="division_name" class="col-sm-4 control-label">Employee Name</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="employee_name" value="<?php print $employee_details ->employee_name; ?>" placeholder="Employee Name">
                                </div>
                            </div>
                          	<div class="form-group">
								<label for="division" class="col-sm-4 control-label">Division</label>
								<div class="col-sm-5 selectContainer">
									<select class="form-control" name="division_name">
										<?php foreach($divisions as $division): ?>
											<?php if($employee_details ->division_id == $division->division_id): ?>
												<option selected="selected" value="<?php print $employee_details->division_id; ?>">
												<?php print $employee_details->division_name; ?>
												</option>     
											<?php else: ?>
												<option value="<?php print $division->division_id; ?>"><?php print $division->division_name; ?></option>
											<?php endif; ?>                  
										<?php endforeach; ?>
									</select>
								</div>
							</div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div><!-- form-group  -->
                        </form><!-- end of form -->
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->