
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Employee
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="row filter-area">
             <form  method="post" accept-charset="utf-8" action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>">
              
                 <div class="form-group">
                    <label class="col-sm-2 control-label"> Filter By Division</label>
                    <div class="col-sm-3">
                      

                         <select name="division_id" class="form-control" onchange="this.form.submit()">
                         <option selected="selected" value="all"> All Divisions </option>
                        <?php foreach($divisions as $division): ?>
                            <?php if($division->division_id == $selected_division_id): ?>
                                <option selected="selected" value="<?php print $division->division_id;?>" ><?php print $division ->division_name; ?></option>
                            <?php else: ?>
                                <option value="<?php print $division->division_id;?>" ><?php print $division ->division_name; ?></option>
                            <?php endif;?>

                           
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </form>    
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text">Employee</h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee/create');?>" class="create-item-btn pull-right"><i class="fa fa-plus"></i>Create</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">

                <p class="bg-success">
                    <?php if($this->session->flashdata('employee_created')): ?>
                        <?php echo $this->session->flashdata('employee_created'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('employee_updated')): ?>
                        <?php echo $this->session->flashdata('employee_updated'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('employee_deleted')): ?>
                        <?php echo $this->session->flashdata('employee_deleted'); ?>
                    <?php endif; ?>
                </p>

                <?php if(!$employees): ?>
                    <p class="bg-primary simple-msg text-center">
                        First you'll need to add some employees. <i class="fa fa-arrow-circle-o-up fa-2 pull-right"></i>
                    </p>
                <?php endif; ?>

                <?php if($employees): ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Employee ID</th>
                                <th>Employee Full Name</th>
                                <th>Division Name</th>
                                <th colspan="2" class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($employees as $employee): ?>
                                    <tr>
                                        <td class="text-td"><?php print $employee->employee_id; ?></td>
                                        <td class="text-td"><?php print $employee->employee_name; ?></td>
                                        <td class="text-td"><?php print $employee->division_name; ?></td>
                                        <td>
                                            <div class="btn-group cust-group" role="group">
                                                <a class="btn btn-warning" 
                                                href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/edit/'.$employee->employee_id); ?>"><i class="fa fa-pencil"></i> </a>
                                                <a class="btn btn-danger" onclick="return doconfirm();" 
                                                href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/delete/'.$employee->employee_id); ?>">
                                                <i class="fa fa-trash-o" ></i> </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->