<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
            <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> KPI
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Edit KPI</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>
                        <form class="form-horizontal" action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi/edit/'.$kpi_details ->kpi_id);?>" method="post">
                            <div class="form-group">
                                <label for="kpi_name" class="col-sm-4 control-label">KPI Name</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kpi_name" value="<?php print $kpi_details ->kpi_name; ?>" placeholder="KPI Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="organization_name" class="col-sm-4 control-label">Organization UID</label>
                                <div class="col-sm-5">
                                    <input type="email" class="form-control" name="organization_uid" value="<?php print $this->uri->segment(3); ?>" disabled="disabled">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="repeat_duration" class="col-sm-4 control-label">Repeat Duration</label>
                                <div class="col-sm-5 selectContainer">
                                    <select class="form-control" name="repeat_duration">
                                     
                                        <option value="1" 
                                        	<?php if($kpi_details ->repeat_duration == 1): ?>
                                        		selected="selected"
                                        	<?php endif; ?>
                                        >Daily</option>
                                        <option value="2" 
                                        	<?php if($kpi_details ->repeat_duration == 2): ?>
                                        		selected="selected"
                                        	<?php endif; ?>
                                        >Weekly</option>
                                        <option value="3" 
                                        	<?php if($kpi_details ->repeat_duration == 3): ?>
                                        		selected="selected"
                                        	<?php endif; ?>
                                        >Monthly</option>
                                        <option value="4" 
                                        	<?php if($kpi_details ->repeat_duration == 4): ?>
                                        		selected="selected"
                                        	<?php endif; ?>
                                        >Quaterly</option>
                                        <option value="5" 
                                        	<?php if($kpi_details ->repeat_duration == 5): ?>
                                        		selected="selected"
                                        	<?php endif; ?>
                                        >Annually</option>
                                       
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="repeat_duration" class="col-sm-4 control-label">KPI start month</label>
                                <div class="col-sm-5 selectContainer">
                                    <select class="form-control" name="start_month">
                                        <option value=""> --Please Select--</option>
                                        <option value="1"
                                            <?php if($kpi_details ->start_month == 1): ?>
                                                selected="selected"
                                            <?php endif; ?>
                                            > January</option>
                                        <option value="4"
                                            <?php if($kpi_details ->start_month == 4): ?>
                                                selected="selected"
                                            <?php endif; ?>
                                            >April</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group"></div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="kpi_value" id="kpi_value" value="kpi_by_value" <?php if($kpi_details ->status_determination == 'by_value'){
                                            	print 'checked';
                                            } 
                                            ?> >
                                            By Value
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kpi_name" class="col-sm-4 control-label">Value</label>
                                <div class="col-sm-5">
                                    <?php if($kpi_details ->status_determination == 'by_value'): ?>
                                    		<input type="text" class="form-control" name="value_by_value1" value="<?php print $kpi_details ->value1; ?>" placeholder="Value 1">
                                    <?php else: ?>
 										<input type="text" class="form-control" name="value_by_value1" placeholder="Value 1">
                                    <?php endif;?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="operation_status" class="col-sm-4 control-label">Operation Status</label>
                                <div class="col-sm-5 selectContainer">
                                    <select class="form-control" name="operation_status">
                                     <?php if($kpi_details ->status_determination == 'by_value'): ?>
                                         <option value="1" 
                                        	<?php if($kpi_details ->operation_status == 1): ?>
                                        		selected="selected"
                                        	<?php endif; ?>
                                        > A / T >= V </option>
                                         <option value="2" 
                                        	<?php if($kpi_details ->operation_status == 3): ?>
                                        		selected="selected"
                                        	<?php endif; ?>
                                        >  A / T < V </option>

                                    <?php else:?>
                                    	<option value=""> -- Please Select -- </option>
                                        <option value="1"> A / T >= V </option>
                                        <option value="2"> A / T < V </option>
                                    <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                           
                            <div class="form-group"></div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="kpi_value" id="kpi_value" value="kpi_by_range"
                                            <?php if($kpi_details ->status_determination == 'by_range'){
                                            	print 'checked';
                                            } 
                                            ?>
                                            >
                                            By Range
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="value_one" class="col-sm-4 control-label">Value 1</label>
                                <div class="col-sm-5"> 
                                    <?php if($kpi_details ->status_determination == 'by_range'): ?>
                                    	<input type="text" class="form-control" name="value_one" value="<?php print $kpi_details ->value1; ?>" placeholder="Value 1">
                                    <?php else: ?>
 										 <input type="text" class="form-control" name="value_one" placeholder="Value 1" >
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="value_two" class="col-sm-4 control-label">Value 2</label>
                                <div class="col-sm-5">
                                    
                                    <?php if($kpi_details ->status_determination == 'by_range'): ?>
                                    	<input type="text" class="form-control" name="value_two" value="<?php print $kpi_details ->value2; ?>" placeholder="Value 2">
                                    <?php else: ?>
 										<input type="text" class="form-control" name="value_two" placeholder="Value 2" >
                                    <?php endif;?>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label for="operation_status" class="col-sm-4 control-label">Operation Status</label>
                                <div class="col-sm-5 selectContainer">
                                    <select class="form-control" name="operation_status2">
                                     <?php if($kpi_details ->status_determination == 'by_range'): ?>
                                         <option value="1" 
                                            <?php if($kpi_details ->operation_status == 1): ?>
                                                selected="selected"
                                            <?php endif; ?> >
                                        A / T > V1 > A / T > V2 > A / T </option>
                                         <option value="2" 
                                            <?php if($kpi_details ->operation_status == 2): ?>
                                                selected="selected"
                                            <?php endif; ?>>
                                        A / T < V1 < A / T < V2 < A / T </option>
                                        

                                    <?php else:?>
                                        <option value=""> -- Please Select -- </option>
                                        <option value="1"> A / T > V1 > A / T > V2 > A / T </option>
                                        <option value="2"> A / T < V1 < A / T < V2 < A / T </option>
                                    <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div><!-- form-group  -->
                        </form><!-- end of form -->
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->