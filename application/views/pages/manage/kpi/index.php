
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> KPI
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text">KPI</h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi/create');?>" class="create-item-btn pull-right"><i class="fa fa-plus"></i>Create</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">

                <p class="bg-success">
                    <?php if($this->session->flashdata('kpi_created')): ?>
                        <?php echo $this->session->flashdata('kpi_created'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('kpi_updated')): ?>
                        <?php echo $this->session->flashdata('kpi_updated'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('kpi_deleted')): ?>
                        <?php echo $this->session->flashdata('kpi_deleted'); ?>
                    <?php endif; ?>
                </p>

                <?php if(!$kpis): ?>
                    <p class="bg-primary simple-msg text-center">
                        First you'll need to add some KPIs. <i class="fa fa-arrow-circle-o-up fa-2 pull-right"></i>
                    </p>
                <?php endif; ?>

                <?php if($kpis): ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>KPI ID</th>
                                <th>KPI Name</th>
                                <th>Repeat Duration</th>
                                <th>Status Determination</th>
                                <th>Value 1</th>
                                <th>Value 2</th>
                                <th colspan="2" class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($kpis as $kpi): ?>
                                    <tr>
                                        <td class="text-td"><?php print $kpi->kpi_id; ?></td>
                                        <td class="text-td"><?php print $kpi->kpi_name; ?></td>
                                        <td class="text-td">
                                            <?php 
                                                if($kpi->repeat_duration == 1){
                                                    print 'Daily';
                                                }
                                                else if($kpi->repeat_duration == 2){
                                                    print 'Weekly';
                                                }
                                                else if($kpi->repeat_duration == 3){
                                                     print 'Monthly';
                                                }
                                                else if($kpi->repeat_duration == 4){
                                                     print 'Quaterly';
                                                }
                                                else if($kpi->repeat_duration == 5){
                                                    print 'Annually';
                                                }
                                            ?>

                                        </td>
                                        <td class="text-td"><?php print $kpi->status_determination; ?></td>
                                        <td class="text-td"><?php print $kpi->value1; ?></td>
                                        <td class="text-td"><?php print $kpi->value2; ?></td>
                                        <td>
                                            <div class="btn-group cust-group" role="group">
                                                <a class="btn btn-warning" 
                                                href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/edit/'.$kpi->kpi_id); ?>"><i class="fa fa-pencil"></i> Edit / View </a>
                                                <a class="btn btn-danger" onclick="return doconfirm();" 
                                                href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/delete/'.$kpi->kpi_id); ?>">
                                                <i class="fa fa-trash-o" ></i> </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->