
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Organizations
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Create Organization</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                        <a href="<?php print site_url('dashboard/manage/organizations/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>
                        <form class="form-horizontal" action="<?php print site_url('dashboard/manage/organizations/create');?>" method="post">
                            <div class="form-group">
                                <label for="organization_name" class="col-sm-4 control-label">Organization Name</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="organization_name" placeholder="Organization Name">
                                </div>
                            </div>
                              <div class="form-group">
                                <label for="organization_name" class="col-sm-4 control-label">Organization Unique ID</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="organization_uid" placeholder="Organization UID">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->