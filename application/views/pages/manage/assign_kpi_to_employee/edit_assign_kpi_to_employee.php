<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
            <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Assign KPI to Employees
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-8">
                        <h2 class="panel-heading-text">Edit KPI to Employees</h2>
                    </div><!-- -->
                    <div class="col-xs-4">
                        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_employee/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="row">
                        <?php echo validation_errors("<p class='bg-danger'>"); ?>
                        <form class="form-horizontal" onsubmit="return checkTargetMatch();"  action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_employee/edit/'.$kpi_to_employee_info->association_id);?>" method="post">
							<div class="form-group">
								<label for="division" class="col-sm-4 control-label">Employee</label>
								<div class="col-sm-5 selectContainer">
									<input type="text" class="form-control" name="employee_name" value="<?php print $kpi_to_employee_info ->employee_name; ?>" placeholder="Target Value" disabled="disabled"> 
								</div>
							</div>

							<div class="form-group">
								<label for="division" class="col-sm-4 control-label">KPI Name</label>
								<div class="col-sm-5 selectContainer">
									<input type="text" class="form-control" name="kpi_name" value="<?php print $kpi_to_employee_info ->kpi_name; ?>" placeholder="KPI name" disabled="disabled">
								</div>
							</div>
							
							<div class="form-group">
								
                                <label for="target_value" class="col-sm-4 control-label">Target Value</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="target_value" id="target_value" value="<?php print $kpi_to_employee_info->target_value; ?>"  placeholder="Target Value">
                                </div>
                                <label class="col-sm-3 control-label txt-left">Max Available Balance :- </label>
                                <label class="col-sm-2 control-label txt-left" id="balanceTarget"><?php print ($kpi_to_employee_info_balance->final + $kpi_to_employee_info->target_value)  ; ?></label>
                                <input type="hidden" value="<?php print $kpi_to_employee_info->employee_id; ?>" id="employee_id" name="employee_id" >
                               

							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-4">
									<button type="submit" class="btn btn-primary">Assign</button>
									<button type="reset" class="btn btn-default">Reset</button>
								</div>
							</div><!-- form-group  -->
                        </form><!-- end of form -->
                    </div>
                </div><!-- container-fluid-->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->