<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Manage
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Assign KPI to Employee
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="row filter-area">
         <form  method="post" id="employee-kpi-list" accept-charset="utf-8" action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>">
           
                 <div class="form-group">
                    <label class="col-sm-2 control-label"> Filter By Division</label>
                    <div class="col-sm-3">
                        <select name="division_id" class="form-control" onchange="divisionSelectDataSubmit();">
                         <option selected="selected" value="all"> All Divisions </option>
                        <?php foreach($divisions as $division): ?>
                            <?php if($division->division_id == $selected_division_id): ?>
                                <option selected="selected" value="<?php print $division->division_id;?>" ><?php print $division ->division_name; ?></option>
                            <?php else: ?>
                                <option value="<?php print $division->division_id;?>" ><?php print $division ->division_name; ?></option>
                            <?php endif;?>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-1 control-label">Employee</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="employee_id" id="employee_id" onchange="this.form.submit()">
                             <option selected="selected" value="all"> All Employees </option>
                            <?php foreach($employeeList as $employee): ?>
                                <?php if($employee->employee_id == $selected_employee_id): ?>
                                    <option selected="selected" value="<?php print $employee->employee_id;?>" ><?php print $employee ->employee_name; ?></option>
                                <?php else: ?>
                                    <option value="<?php print $employee->employee_id;?>" ><?php print $employee ->employee_name; ?></option>
                                <?php endif;?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_employee'); ?>" class="btn btn-warning">Reset Filters</a>
                </div>



        </form> 
        </div>  

        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text">Assign KPI to Employee</h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                        <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_employee/create');?>" class="create-item-btn pull-right"><i class="fa fa-plus"></i>Assign</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">

                <p class="bg-success">
                    <?php if($this->session->flashdata('kpi_to_employee_created')): ?>
                        <?php echo $this->session->flashdata('kpi_to_employee_created'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('kpi_to_employee_updated')): ?>
                        <?php echo $this->session->flashdata('kpi_to_employee_updated'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('kpi_to_employee_deleted')): ?>
                        <?php echo $this->session->flashdata('kpi_to_employee_deleted'); ?>
                    <?php endif; ?>
                </p>

                <?php if(!$kpi_to_employees): ?>
                    <p class="bg-primary simple-msg text-center">
                        First you'll need to assign KPIs to employees. <i class="fa fa-arrow-circle-o-up fa-2 pull-right"></i>
                    </p>
                <?php endif; ?>

                <?php if($kpi_to_employees): ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>KPI ID</th>
                                <th>KPI Name</th>
                                <th>Employee Name</th>
                                <th>Target Value</th>
                                <th>Repeat Duration</th>
                                <th colspan="2" class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($kpi_to_employees as $kpi_to_employee): ?>
                                    <tr>
                                        <td class="text-td"><?php print $kpi_to_employee->kpi_id; ?></td>
                                        <td class="text-td"><?php print $kpi_to_employee->kpi_name; ?></td>
                                        <td class="text-td"><?php print $kpi_to_employee->employee_name; ?></td>
                                        <td class="text-td"><?php print $kpi_to_employee->target_value; ?></td>
                                        <td class="text-td">
                                             <?php 
                                                if($kpi_to_employee->repeat_duration == 1){
                                                    print 'Daily';
                                                }
                                                else if($kpi_to_employee->repeat_duration == 2){
                                                    print 'Weekly';
                                                }
                                                else if($kpi_to_employee->repeat_duration == 3){
                                                     print 'Monthly';
                                                }
                                                else if($kpi_to_employee->repeat_duration == 4){
                                                     print 'Quaterly';
                                                }
                                                else if($kpi_to_employee->repeat_duration == 5){
                                                    print 'Annually';
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <div class="btn-group cust-group" role="group">
                                                <a class="btn btn-warning" 
                                                href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/edit/'.$kpi_to_employee->association_id); ?>"><i class="fa fa-pencil"></i> </a>
                                                <a class="btn btn-danger" onclick="return doconfirm();" 
                                                href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/delete/'.$kpi_to_employee->association_id); ?>">
                                                <i class="fa fa-trash-o" ></i> </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->