
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Report
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> KPI
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text">KPI</h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                    	 <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/organizations/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">


<?php if(isset($division_name_report)): ?>
	<label>Division Name :- <?php print $division_name_report ?></label>
<?php else: ?>
	<label>Division Name :- * </label>
<?php endif; ?>

<br>
<?php if(isset($employee_name_report)): ?>
	<label>Employee Name :- <?php print $employee_name_report ?></label>
<?php else: ?>
	<label>Employee Name :- * </label>
<?php endif; ?>

<br>
 <?php if(!$kpis): ?>
    <p class="bg-primary simple-msg text-center">
        No Any Results. 
    </p>
<?php endif; ?>

 <div id="php_table">
	 <?php if($kpis): ?>
	    <div class="table-responsive">
	        <table class="table table-hover">
	            <thead>
	            <tr>
	                <th>KPI ID</th>
	                <th>KPI Name</th>
	                <th>Repeat Duration</th>
	                <th>Target Value</th>
	                <th>Actual Value</th>
	                <th>Accumulated Target</th>
	                <th>Accumulated Actual</th>
	                <th>Varience</th>
	                <th>Varience %</th>
	               
	                <th colspan="2" class="text-right">Actions</th>
	            </tr>
	            </thead>
	            <tbody>
	                <?php foreach($kpis as $kpi): ?>
	                    <tr>
	                        <td class="text-td"><?php print $kpi->kpi_id; ?></td>
	                        <td class="text-td"><?php print $kpi->kpi_name; ?></td>
	                        <td class="text-td">
	                            <?php 
	                                if($kpi->repeat_duration == 1){
	                                    print 'Daily';
	                                }
	                                else if($kpi->repeat_duration == 2){
	                                    print 'Weekly';
	                                }
	                                else if($kpi->repeat_duration == 3){
	                                     print 'Monthly';
	                                }
	                                else if($kpi->repeat_duration == 4){
	                                     print 'Quaterly';
	                                }
	                                else if($kpi->repeat_duration == 5){
	                                    print 'Annually';
	                                }
	                            ?>

	                        </td>
	                        <td  class="text-td"><?php print $kpi->target; ?></td>
	                        <td  class="text-td"><?php print $kpi->actual; ?></td>

	                        <td  class="text-td"><?php print $kpi->accumulated_target; ?></td>
	                        <td  class="text-td"><?php print $kpi->accumulated_actual; ?></td>
	                        <td  class="text-td"><?php print $kpi->variation; ?></td>
	                        <td  
									<?php 
										$t = $kpi->target;
										$a = $kpi->actual; 
										$cssClass = '';

										if($kpi->status_determination == 'by_value'){
											// $v = $kpi->value1;
											$v = $kpi->value1;

											// Operation Status A/T > V  {= 1}
											if($kpi->operation_status == 1){
												// T*V > A ----------- red
												if($t * $v > $a){
													//print 'red'." ".'T*V > A';
													$cssClass = 'red-txt-op';
												}
												// T*V < A ----------- green
												else{
													//print 'green'." ".'T*V < A';
													$cssClass = 'green-txt-op';
												}

											}
											//Operation Status A/T < V  {= 2}
											else{
												// T*V < A ----------- red
												if($t * $v < $a){
													//print 'red'." ".'T*V < A';
													$cssClass = 'red-txt-op';
												}
												// T*V > A ----------- green
												else{
													//print 'green'." ".'T*V > A';
													$cssClass = 'green-txt-op';
												}
												
											}
											
										}
										else{
											// $v1 = $kpi->value1;
											$v1 = $kpi->value1;
											$v2 = $kpi->value2;

											//Operation Status A/T > V1 > A/T > V2 > A/T   {= 1}

											if($kpi->operation_status == 1){
												// T*V1 > A ----------- red	
												if($t * $v1 > $a){
													//print 'red'." ".'T*V1 > A';
													$cssClass = 'red-txt-op';
												}
												// T*V1 < A < T*V2 ---- yello
												else if( ($t * $v1) < $a &&  $a < ($t * $v2)){
													//print 'yello'." ".'T*V1 < A < T*V2';
													$cssClass = 'yellow-txt-op';
												}
												// T*V2 < A ----------- green
												else{
													//print 'green'." ".'T*V2 < A';
													$cssClass = 'green-txt-op';
												}
											}
											//Operation Status A/T < V1 < A/T < V2 < A/T
											else{
												// T*V1 < A ----------- red
												if($t * $v1 < $a){
													//print 'red'." ".'T*V1 < A';
													$cssClass = 'red-txt-op';
												}
												// T*V1 > A > T*V2 ---- yellow
												else if(($t * $v1) > $a &&  $a > ($t * $v2)){
													//print 'yellow'." ".'T*V1 > A > T*V2';
													$cssClass = 'yellow-txt-op';
												}
												// T*V2 > A ----------- green
												else{
													//print 'green'." ".'T*V2 > A';
													$cssClass = 'green-txt-op';
												}
											}
										}
										print 'class="text-td txt-white '.$cssClass.'"';
									?>

							>
								<?php print $kpi->variation_presage; ?>	
									
	                        </td>
	                       		
	                        <td>
	                            <div class="btn-group cust-group" role="group">
	                                <a class="btn btn-warning" 
	                                href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/kpi-id/'.$kpi->kpi_id); ?>"> View  Detail Report</a> 

	                                
	                            </div>
	                        </td>
	                    </tr>
	                <?php endforeach; ?>
	            </tbody>
	        </table>
	    </div>
	<?php endif; ?>
</div>


            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
