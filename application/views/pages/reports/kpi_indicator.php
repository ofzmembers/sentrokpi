<?php
date_default_timezone_set("Asia/Bangkok");
$date = new DateTime('now');
$printTime = $date->format('Y/M/d - H:i A');
?>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Report
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> KPI Indicators
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text"> KPI Indicators - <span class="internal-time"><?php print $printTime; ?></span></h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                    	 <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/organizations/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body" id="indicator-view">

				<div class="col-xs-12">
				    <div class="row gauge-area-row" id="guage-area">
				        <p class="bg-primary simple-msg text-center">
                        	<i class="fa fa-2 pull-left fa-arrow-circle-o-left left-arrow-kpi-indicator"></i> First you'll need to do query from left side menu. 
                    	</p>
				    </div>
				</div>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<?php 
print "<script language=\"javascript\">setTimeout(function(){ getIndicators(); }, 400);</script>";
?>




