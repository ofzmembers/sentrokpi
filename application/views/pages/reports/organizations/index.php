
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Report
            </li>
            <li class="active">
                <i class="fa fa-table"></i> Organizations
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text">Organizations</h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                    
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">

                <p class="bg-success">
                    <?php if($this->session->flashdata('organization_created')): ?>
                        <?php echo $this->session->flashdata('organization_created'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('organization_updated')): ?>
                        <?php echo $this->session->flashdata('organization_updated'); ?>
                    <?php endif; ?>

                    <?php if($this->session->flashdata('organization_deleted')): ?>
                        <?php echo $this->session->flashdata('organization_deleted'); ?>
                    <?php endif; ?>
                </p>

                <?php if(!$organizations): ?>
                    <p class="bg-primary simple-msg text-center">
                        First you'll need to add some organizations. <i class="fa fa-arrow-circle-o-up fa-2 pull-right"></i>
                    </p>
                <?php endif; ?>

                <?php if($organizations): ?>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Org ID</th>
                                <th>Organization Name</th>
                                <th>Organization UID</th>
                                <th colspan="2" class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($organizations as $organization): ?>
                                    <tr>
                                        <td class="text-td"><?php print $organization->organization_id; ?></td>
                                        <td class="text-td"><?php print $organization->organization_name; ?></td>
                                         <td class="text-td"><?php print $organization->organization_uid; ?></td>
                                        <td>
                                            <div class="btn-group cust-group" role="group">
                                                <a class="btn btn-warning" 
                                                href="<?php print site_url('dashboard/report/'.$organization->organization_uid.'/kpi-report/');?>"> <i class="fa fa-print"></i> View Reports
                                                 </a>

                                                 <a class="btn btn-danger" 
                                                href="<?php print site_url('dashboard/report/'.$organization->organization_uid.'/kpi-indicator/');?>"> <i class="fa fa-pie-chart"></i> View Indicators
                                                 </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->