<?php
$nextUrl = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/kpi-id/');
date_default_timezone_set("Asia/Bangkok");
$date = new DateTime('now');
$printTime = $date->format('Y/M/d - H:i A');

?>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Report
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> KPI
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi-report/download/');?>">
            <div class="form-group filter-area row">

            
                <input type="hidden" class="form-control" name="organization_uid" id="organization_uid" placeholder="Organization UID" value="<?php print $this->uri->segment(3); ?>">
               
                <label class="col-sm-1 control-label">Division</label>
                <div class="col-sm-2">
                    <select class="form-control" name="division_id" id="division_id" onchange="getEmployeeListForDivisionReport(this,'<?php print $this->uri->segment(3); ?>','<?php print $nextUrl; ?>')">
                        <option value="all"> -- All Divisions -- </option>
                        <?php foreach($divisions as $division): ?>   
                        <option value="<?php print $division->division_id; ?>"><?php print $division->division_name; ?></option>       <?php endforeach; ?>
                    </select>
                </div>

                <label class="col-sm-1 control-label">Employee</label>
                <div class="col-sm-2">
                    <select class="form-control" name="employee_id" id="employee_id" onchange="return getReportsSummary('<?php print $nextUrl; ?>');">
                    <option value="all">-- All Employees --</option>
                    </select>
                </div>

                <label class="col-sm-1 control-label">Date Range</label>
                <div class="col-sm-2">
                    <input type="text" placeholder="From" id="datepicker1"  name="datepicker1" class="formfield_text form-control" onchange="return getReportsSummary('<?php print $nextUrl; ?>');">
                </div>

                <div class="col-sm-2">
                    <input type="text" placeholder="To" id="datepicker2"  name="datepicker2" class="formfield_text form-control" onchange="return getReportsSummary('<?php print $nextUrl; ?>');">
                </div>



                <button type="submit" id="print-preview-btn-php" class="btn btn-success" name="submit"> <i class="fa fa-print"></i>  Print</button>

                    
            </div>   
        </form>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text">KPI - <span class="internal-time"><?php print $printTime; ?></span></h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                    	 <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/organizations/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">

            	<div class="col-xs-12">
				    <div class="row gauge-area-row" id="report-summary-area">
				      <!--   <p class="bg-primary simple-msg text-center" id="report-summary-area-msg">
                        	<i class="fa fa-2 pull-left fa-arrow-circle-o-left left-arrow-kpi-indicator"></i> First you'll need to do query from left side menu. 
                    	</p> -->

                    	 <p class="bg-primary simple-msg text-center" id="report-no-result-found">
                        	<i class="fa fa-2 pull-left fa-arrow-circle-o-left left-arrow-kpi-indicator"></i> No Result Found
                    	</p>

                    	<div id="report-summary-table-area">
						    <div class="table-responsive">
						        <table class="table table-hover">
						            <thead>
						            <tr>
						                <th>KPI ID</th>
                                        <th>KPI Name</th>
                                        <th>Repeat Duration</th>
                                        <th>Target Value</th>
                                        <th>Actual Value</th>
                                        <th>Variance</th>
                                        <th>Variance %</th>
                                        <th>YTD target</th>
                                        <th>YTD actual</th>
                                        <th>YTD Variance</th>
                                        <th>YTD Variance %</th>
						                <th colspan="2" class="text-right no-print">Actions</th>
						            </tr>
						            </thead>
						            <tbody id="report-summar-tbody">
						                   
						               
						            </tbody>
						        </table>
						    </div>
					</div>

				    </div>
				</div>


            <!-- load data 1st time-->
            <div class="col-xs-12">
                <div class="row">
                     <div class="backend-data-table">
                        <?php if(!$kpis): ?>
                            <p class="bg-primary simple-msg text-center">
                                No Any Results. 
                            </p>
                        <?php endif; ?>

                        <div id="php_table">
                             <?php if($kpis): ?>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>KPI ID</th>
                                            <th>KPI Name</th>
                                            <th>Repeat Duration</th>
                                            <th>Target Value</th>
                                            <th>Actual Value</th>
                                            <th>Variance</th>
                                            <th>Variance %</th>
                                            <th>YTD target</th>
                                            <th>YTD actual</th>
                                            <th>YTD Variance</th>
                                            <th>YTD Variance %</th>
                                           
                                            <th colspan="2" class="text-right no-print">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($kpis as $kpi): ?>
                                                <tr>
                                                    <td class="text-td"><?php print $kpi->kpi_id; ?></td>
                                                    <td class="text-td"><?php print $kpi->kpi_name; ?></td>
                                                    <td class="text-td">
                                                        <?php 
                                                            if($kpi->repeat_duration == 1){
                                                                print 'Daily';
                                                            }
                                                            else if($kpi->repeat_duration == 2){
                                                                print 'Weekly';
                                                            }
                                                            else if($kpi->repeat_duration == 3){
                                                                 print 'Monthly';
                                                            }
                                                            else if($kpi->repeat_duration == 4){
                                                                 print 'Quaterly';
                                                            }
                                                            else if($kpi->repeat_duration == 5){
                                                                print 'Annually';
                                                            }
                                                        ?>

                                                    </td>
                                                    <td  class="text-td"><?php print number_format($kpi->target,2); ?></td>
                                                    <td  class="text-td"><?php print number_format($kpi->actual,2); ?></td>

                                                   
                                                    <td  class="text-td"><?php print number_format($kpi->variation,2); ?></td>
                                                    <td  
                                                            <?php 
                                                                $t = $kpi->target;
                                                                $a = $kpi->actual; 
                                                                $cssClass = '';

                                                                if($kpi->status_determination == 'by_value'){
                                                                    // $v = $kpi->value1;
                                                                    $v = $kpi->value1;

                                                                    // Operation Status A/T > V  {= 1}
                                                                    if($kpi->operation_status == 1){
                                                                        // T*V > A ----------- red
                                                                        if($t * $v > $a){
                                                                            //print 'red'." ".'T*V > A';
                                                                            $cssClass = 'red-txt-op';
                                                                        }
                                                                        // T*V < A ----------- green
                                                                        else{
                                                                            //print 'green'." ".'T*V < A';
                                                                            $cssClass = 'green-txt-op';
                                                                        }

                                                                    }
                                                                    //Operation Status A/T < V  {= 2}
                                                                    else{
                                                                        // T*V < A ----------- red
                                                                        if($t * $v < $a){
                                                                            //print 'red'." ".'T*V < A';
                                                                            $cssClass = 'red-txt-op';
                                                                        }
                                                                        // T*V > A ----------- green
                                                                        else{
                                                                            //print 'green'." ".'T*V > A';
                                                                            $cssClass = 'green-txt-op';
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else{
                                                                    // $v1 = $kpi->value1;
                                                                    $v1 = $kpi->value1;
                                                                    $v2 = $kpi->value2;

                                                                    //Operation Status A/T > V1 > A/T > V2 > A/T   {= 1}

                                                                    if($kpi->operation_status == 1){
                                                                        // T*V1 < A < T*V2 
                                                                        // T * V1  < A && A < T * V2 --------yellow
                                                                        if(($t * $v1 < $a) && ($t * $v2 > $a)){
                                                                            //print 'red'." ".'T*V1 > A';
                                                                            $cssClass = 'yellow-txt-op';
                                                                        }
                                                                        // T*V1 < A > T*V2 
                                                                        // T * V1  < A && A > T * V2 ---- green
                                                                        else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                                                            //$cssClass = 'yellow-txt-op';
                                                                            $cssClass = 'green-txt-op';
                                                                        }
                                                                        // T*V1 >A < T*V2 
                                                                        // T * V1  > A && A < T * V2 ----------- red 
                                                                        else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                                                            $cssClass = 'red-txt-op';
                                                                        }
                                                                    }
                                                                    //Operation Status A/T < V1 < A/T < V2 < A/T
                                                                    else{
                                                                        // T * V1  < A && A < T * V2
                                                                        // T * V1  < A  < T * V2
                                                                        if(($t * $v1 < $a) && ($t * $v2 > $a)){
                                                            
                                                                           $cssClass = 'yellow-txt-op';
                                                                        }
                                                                        // T * V1  < A > T * V2
                                                                       // T * V1  < A && A > T * V2 ---- red
                                                                        else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                                                            //print 'yellow'." ".'T*V1 > A > T*V2';
                                                                           $cssClass = 'red-txt-op';
                                                                        }
                                                                        // T*V1 >A < T*V2----------- green
                                                                        // T * V1  > A && A < T * V2
                                                                         else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                                                            //print 'green'." ".'T*V2 > A';
                                                                           $cssClass = 'green-txt-op';
                                                                        }
                                                                    }
                                                                }
                                                                print 'class="text-td txt-white '.$cssClass.'"';
                                                            ?>

                                                    >
                                                        <?php print $kpi->variation_presage; ?> 
                                                            
                                                    </td>

                                                    <td  class="text-td"><?php print number_format($kpi->YTD_target,2); ?></td>
                                                    <td  class="text-td"><?php print number_format($kpi->YTD_actual,2); ?></td>
                                                    <td  class="text-td"><?php print number_format($kpi->YTD_variation,2); ?></td>
                                                   
                                                     <td  
                                                            <?php 
                                                                $t = $kpi->YTD_target;
                                                                $a = $kpi->YTD_actual; 
                                                                $cssClass = '';

                                                                if($kpi->status_determination == 'by_value'){
                                                                    // $v = $kpi->value1;
                                                                    $v = $kpi->value1;

                                                                    // Operation Status A/T > V  {= 1}
                                                                    if($kpi->operation_status == 1){
                                                                        // T*V > A ----------- red
                                                                        if($t * $v > $a){
                                                                            //print 'red'." ".'T*V > A';
                                                                            $cssClass = 'red-txt-op';
                                                                        }
                                                                        // T*V < A ----------- green
                                                                        else{
                                                                            //print 'green'." ".'T*V < A';
                                                                            $cssClass = 'green-txt-op';
                                                                        }

                                                                    }
                                                                    //Operation Status A/T < V  {= 2}
                                                                    else{
                                                                        // T*V < A ----------- red
                                                                        if($t * $v < $a){
                                                                            //print 'red'." ".'T*V < A';
                                                                            $cssClass = 'red-txt-op';
                                                                        }
                                                                        // T*V > A ----------- green
                                                                        else{
                                                                            //print 'green'." ".'T*V > A';
                                                                            $cssClass = 'green-txt-op';
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else{
                                                                    // $v1 = $kpi->value1;
                                                                    $v1 = $kpi->value1;
                                                                    $v2 = $kpi->value2;

                                                                    //Operation Status A/T > V1 > A/T > V2 > A/T   {= 1}

                                                                    if($kpi->operation_status == 1){
                                                                        // T*V1 < A < T*V2 
                                                                        // T * V1  < A && A < T * V2 --------yellow
                                                                        if(($t * $v1 < $a) && ($t * $v2 > $a)){
                                                                            //print 'red'." ".'T*V1 > A';
                                                                            $cssClass = 'yellow-txt-op';
                                                                        }
                                                                        // T*V1 < A > T*V2 
                                                                        // T * V1  < A && A > T * V2 ---- green
                                                                        else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                                                            //$cssClass = 'yellow-txt-op';
                                                                            $cssClass = 'green-txt-op';
                                                                        }
                                                                        // T*V1 >A < T*V2 
                                                                        // T * V1  > A && A < T * V2 ----------- red 
                                                                        else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                                                            $cssClass = 'red-txt-op';
                                                                        }
                                                                    }
                                                                    //Operation Status A/T < V1 < A/T < V2 < A/T
                                                                    else{
                                                                        // T * V1  < A && A < T * V2
                                                                        // T * V1  < A  < T * V2
                                                                        if(($t * $v1 < $a) && ($t * $v2 > $a)){
                                                            
                                                                           $cssClass = 'yellow-txt-op';
                                                                        }
                                                                        // T * V1  < A > T * V2
                                                                       // T * V1  < A && A > T * V2 ---- red
                                                                        else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                                                            //print 'yellow'." ".'T*V1 > A > T*V2';
                                                                           $cssClass = 'red-txt-op';
                                                                        }
                                                                        // T*V1 >A < T*V2----------- green
                                                                        // T * V1  > A && A < T * V2
                                                                         else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                                                            //print 'green'." ".'T*V2 > A';
                                                                           $cssClass = 'green-txt-op';
                                                                        }
                                                                    }
                                                                }
                                                                print 'class="text-td txt-white '.$cssClass.'"';
                                                            ?>

                                                    >
                                                        <?php print number_format($kpi->YTD_variation_presage,2); ?> 
                                                            
                                                    </td>
                                                        
                                                    <td class="no-print">
                                                        <div class="btn-group cust-group" role="group">
                                                            <a class="btn btn-warning" 
                                                            href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/kpi-id/'.$kpi->kpi_id); ?>"> View  Detail Report</a> 

                                                            
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>
                        </div><!-- /php table-->
                        
                </div><!-- / backend-data-table-->
                    
                </div><!-- / row -->

            </div><!-- / col-xs-12-->


 

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
