


<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i> Report
            </li>
             <li>
                <i class="fa fa-building"></i> <?php print $this->uri->segment(3); ?>
            </li>
            <li class="active">
                <i class="fa fa-table"></i> KPI Detail Report
            </li>
        </ol>
    </div><!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text"> KPI Detail Report</h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                    	 <a href="<?php print site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi-report/');?>" class="back-item-btn pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">

                    <?php if(!$results): ?>
                        <p class="bg-primary simple-msg text-center">
                            No Any Results. 
                        </p>
                    <?php endif; ?>

                    <?php if($results): ?>
                    <div id="print-report-area">
                    	<table class="table table-hover">
                    		<tr class="danger">
                    			<th>Record id</th>
                    			<th>Target Value</th>
                                <th>Actual Value</th>
                    			<th>Date/Time</th>
                    		</tr>
                    		<?php foreach ($results as $data) : ?>
                    		 	<tr>
                    		 		<td><?php print $data->record_id ; ?></td>
                    		 		<td><?php print $data->target_value ; ?></td>
                                    <td><?php print $data->actual_value ; ?></td>
                    		 		<td><?php print $data->created_tme ; ?></td>
                    		 	</tr>
                    		<?php endforeach; ?>	
                    	</table>
                    </div>
                    	<ul class="pagination">
                    		<?php foreach ($links as $link) : ?>
                    	    	<li><?php  print $link; ?></li>
                    	    <?php endforeach; ?>	
                    	 </ul>
                    <?php endif; ?>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->


        <?php if($summary_details): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="panel-heading-text"> KPI Summary Report</h2>
                    </div><!-- -->
                    <div class="col-sm-4">
                         
                    </div><!-- -->
                </div><!-- -->
            </div>
            <div class="panel-body">
                        <table class="table table-hover">
                            <tr class="success">
                                <th>KPI ID</th>
                                <th>KPI Name</th>
                                <th>Accumulated Target Value</th>
                                <th>Accumulated Actual Value</th> 
                            </tr>
                            <tr>
                                <td><?php print $summary_details->kpi_id ; ?></td>
                                <td><?php print $summary_details->kpi_name ; ?></td>
                                <td><?php print $summary_details->total_target ; ?></td>
                                <td><?php print $summary_details->total_actual ; ?></td>
                            </tr>
                        </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <?php endif; ?>



    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


