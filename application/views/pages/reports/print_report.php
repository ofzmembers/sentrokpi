
<?php if($kpis): ?>

        <table class="table" border="1" cellpadding="2" >
        <thead>
            <tr nobr="true">
                <th>KPI ID</th>
                <th>KPI Name</th>
                <th>Repeat Duration</th>
                <th>Target Value</th>
                <th>Actual Value</th>
                <th>Variance</th>
                <th>Variance %</th>
                <th>YTD target</th>
                <th>YTD actual</th>
                <th>YTD Variance</th>
                <th>YTD Variance %</th>


            </tr>
        </thead>

            <?php foreach($kpis as $kpi): ?>
                <tr nobr="true">
                    <td class="text-td"><?php print $kpi->kpi_id; ?></td>
                    <td class="text-td"><?php print $kpi->kpi_name; ?></td>
                    <td class="text-td">
                        <?php
                        if($kpi->repeat_duration == 1){
                            print 'Daily';
                        }
                        else if($kpi->repeat_duration == 2){
                            print 'Weekly';
                        }
                        else if($kpi->repeat_duration == 3){
                            print 'Monthly';
                        }
                        else if($kpi->repeat_duration == 4){
                            print 'Quaterly';
                        }
                        else if($kpi->repeat_duration == 5){
                            print 'Annually';
                        }
                        ?>

                    </td>
                    <td  class="text-td"><?php print number_format($kpi->target,2); ?></td>
                    <td  class="text-td"><?php print number_format($kpi->actual,2); ?></td>
                    <td  class="text-td"><?php print number_format($kpi->variation,2); ?></td>
                    <td
                        <?php
                        $t = $kpi->target;
                        $a = $kpi->actual;
                        $cssClass = '';

                        if($kpi->status_determination == 'by_value'){
                            // $v = $kpi->value1;
                            $v = $kpi->value1;

                            // Operation Status A/T > V  {= 1}
                            if($kpi->operation_status == 1){
                                // T*V > A ----------- red
                                if($t * $v > $a){
                                    //print 'red'." ".'T*V > A';
                                    $cssClass = 'red';
                                }
                                // T*V < A ----------- green
                                else{
                                    //print 'green'." ".'T*V < A';
                                    $cssClass = 'green';
                                }

                            }
                            //Operation Status A/T < V  {= 2}
                            else{
                                // T*V < A ----------- red
                                if($t * $v < $a){
                                    //print 'red'." ".'T*V < A';
                                    $cssClass = 'red';
                                }
                                // T*V > A ----------- green
                                else{
                                    //print 'green'." ".'T*V > A';
                                    $cssClass = 'green';
                                }

                            }

                        }
                        else{
                            // $v1 = $kpi->value1;
                            $v1 = $kpi->value1;
                            $v2 = $kpi->value2;

                            //Operation Status A/T > V1 > A/T > V2 > A/T   {= 1}

                            if($kpi->operation_status == 1){
                                // T*V1 < A < T*V2
                                // T * V1  < A && A < T * V2 --------yellow
                                if(($t * $v1 < $a) && ($t * $v2 > $a)){
                                    //print 'red'." ".'T*V1 > A';
                                    $cssClass = 'yellow';
                                }
                                // T*V1 < A > T*V2
                                // T * V1  < A && A > T * V2 ---- green
                                else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                    //$cssClass = 'yellow';
                                    $cssClass = 'green';
                                }
                                // T*V1 >A < T*V2
                                // T * V1  > A && A < T * V2 ----------- red
                                else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                    $cssClass = 'red';
                                }
                            }
                            //Operation Status A/T < V1 < A/T < V2 < A/T
                            else{
                                // T * V1  < A && A < T * V2
                                // T * V1  < A  < T * V2
                                if(($t * $v1 < $a) && ($t * $v2 > $a)){

                                    $cssClass = 'yellow';
                                }
                                // T * V1  < A > T * V2
                                // T * V1  < A && A > T * V2 ---- red
                                else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                    //print 'yellow'." ".'T*V1 > A > T*V2';
                                    $cssClass = 'red';
                                }
                                // T*V1 >A < T*V2----------- green
                                // T * V1  > A && A < T * V2
                                else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                    //print 'green'." ".'T*V2 > A';
                                    $cssClass = 'green';
                                }
                            }
                        }
                        print 'class="text-td txt-white '.$cssClass.'"';
                        ?>
                        <?php print 'style="background-color:'.$cssClass.'"'; ?>
                        >
                        <?php print $kpi->variation_presage; ?>

                    </td>

                    <td  class="text-td"><?php print number_format($kpi->YTD_target,2); ?></td>
                    <td  class="text-td"><?php print number_format($kpi->YTD_actual,2); ?></td>
                    <td  class="text-td"><?php print number_format($kpi->YTD_variation,2); ?></td>

                    <td
                        <?php
                        $t = $kpi->YTD_target;
                        $a = $kpi->YTD_actual;
                        $cssClass = '';

                        if($kpi->status_determination == 'by_value'){
                            // $v = $kpi->value1;
                            $v = $kpi->value1;

                            // Operation Status A/T > V  {= 1}
                            if($kpi->operation_status == 1){
                                // T*V > A ----------- red
                                if($t * $v > $a){
                                    //print 'red'." ".'T*V > A';
                                    $cssClass = 'red';
                                }
                                // T*V < A ----------- green
                                else{
                                    //print 'green'." ".'T*V < A';
                                    $cssClass = 'green';
                                }

                            }
                            //Operation Status A/T < V  {= 2}
                            else{
                                // T*V < A ----------- red
                                if($t * $v < $a){
                                    //print 'red'." ".'T*V < A';
                                    $cssClass = 'red';
                                }
                                // T*V > A ----------- green
                                else{
                                    //print 'green'." ".'T*V > A';
                                    $cssClass = 'green';
                                }

                            }

                        }
                        else{
                            // $v1 = $kpi->value1;
                            $v1 = $kpi->value1;
                            $v2 = $kpi->value2;

                            //Operation Status A/T > V1 > A/T > V2 > A/T   {= 1}

                            if($kpi->operation_status == 1){
                                // T*V1 < A < T*V2
                                // T * V1  < A && A < T * V2 --------yellow
                                if(($t * $v1 < $a) && ($t * $v2 > $a)){
                                    //print 'red'." ".'T*V1 > A';
                                    $cssClass = 'yellow';
                                }
                                // T*V1 < A > T*V2
                                // T * V1  < A && A > T * V2 ---- green
                                else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                    //$cssClass = 'yellow';
                                    $cssClass = 'green';
                                }
                                // T*V1 >A < T*V2
                                // T * V1  > A && A < T * V2 ----------- red
                                else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                    $cssClass = 'red';
                                }
                            }
                            //Operation Status A/T < V1 < A/T < V2 < A/T
                            else{
                                // T * V1  < A && A < T * V2
                                // T * V1  < A  < T * V2
                                if(($t * $v1 < $a) && ($t * $v2 > $a)){

                                    $cssClass = 'yellow';
                                }
                                // T * V1  < A > T * V2
                                // T * V1  < A && A > T * V2 ---- red
                                else if( ($t * $v1) < $a &&  $a > ($t * $v2)){
                                    //print 'yellow'." ".'T*V1 > A > T*V2';
                                    $cssClass = 'red';
                                }
                                // T*V1 >A < T*V2----------- green
                                // T * V1  > A && A < T * V2
                                else if(($t * $v1 > $a) && ($t * $v2 > $a)){
                                    //print 'green'." ".'T*V2 > A';
                                    $cssClass = 'green';
                                }
                            }
                        }
                        print 'class="text-td txt-white '.$cssClass.'"';
                        ?>
                       <?php print 'style="background-color:'.$cssClass.'"'; ?>
                        >
                        <?php print number_format($kpi->YTD_variation_presage,2); ?>

                    </td>


                </tr>
            <?php endforeach; ?>
        </table>
<?php endif; ?>
