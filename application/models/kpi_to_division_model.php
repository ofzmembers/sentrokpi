<?php

class Kpi_to_division_model extends  CI_Model{

	/**
	 * get_kpi_to_division
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $user_type     [description]
	 * @param  [type] $user_id       [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_to_division($org_uniq_name,$user_type,$user_id){
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

  		if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager'){

  			$this->db->select('k.kpi_id,k.kpi_name,d.division_name,ka.association_id,ka.target_value,k.repeat_duration,ka.actual_value');
  			$this->db->from('kpi k,kpi_association ka, division d');
  			$this->db->where('k.organization_id',$organization_id);
  			$this->db->where('d.division_id = ka.division_id');
  			$this->db->where('k.kpi_id = ka.kpi_id');
  			$this->db->where('ka.employee_id = 0');
  			$this->db->where('ka.active_status',1);
  			$this->db->where('k.active_status',1);
  			$query = $this->db->get();
  			return $query->result();

  		}else if($user_type == 'division_head'){
  			$this->db->select('k.kpi_id,k.kpi_name,d.division_name,ka.association_id,ka.target_value,k.repeat_duration,ka.actual_value');
  			$this->db->from('kpi k,kpi_association ka, division d,divisional_head dh');
  			$this->db->where('k.organization_id',$organization_id);
  			$this->db->where('dh.division_id = d.division_id');
  			$this->db->where('d.division_id = ka.division_id');
  			$this->db->where('dh.user_id',$user_id);
  			$this->db->where('k.kpi_id = ka.kpi_id');
  			$this->db->where('ka.employee_id = 0');
  			$this->db->where('ka.active_status',1);
  			$this->db->where('k.active_status',1);
  			$query = $this->db->get();
  			return $query->result();
  		}
  		else{

  			return false;

  		}
	}

	/**
	 * [get_kpi_to_division_filter_by_division description]
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $user_type     [description]
	 * @param  [type] $user_id       [description]
	 * @param  [type] $data          [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_to_division_filter_by_division($org_uniq_name,$user_type,$user_id,$data){
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

        if($data['division_id'] == 'all'){
        		if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager'){

		  			$this->db->select('k.kpi_id,k.kpi_name,d.division_name,ka.association_id,ka.target_value,k.repeat_duration,ka.actual_value');
		  			$this->db->from('kpi k,kpi_association ka, division d');
		  			$this->db->where('k.organization_id',$organization_id);
		  			$this->db->where('d.division_id = ka.division_id');
		  			$this->db->where('k.kpi_id = ka.kpi_id');
		  			$this->db->where('ka.employee_id = 0');
		  			$this->db->where('ka.active_status',1);
		  			$this->db->where('k.active_status',1);
		  			$query = $this->db->get();
		  			return $query->result();

		  		}
		  		else if($user_type == 'division_head'){
		  			$this->db->select('k.kpi_id,k.kpi_name,d.division_name,ka.association_id,ka.target_value,k.repeat_duration,ka.actual_value');
		  			$this->db->from('kpi k,kpi_association ka, division d,divisional_head dh');
		  			$this->db->where('k.organization_id',$organization_id);
		  			$this->db->where('dh.division_id = d.division_id');
		  			$this->db->where('d.division_id = ka.division_id');
		  			$this->db->where('dh.user_id',$user_id);
		  			$this->db->where('k.kpi_id = ka.kpi_id');
		  			$this->db->where('ka.employee_id = 0');
		  			$this->db->where('ka.active_status',1);
		  			$this->db->where('k.active_status',1);
		  			$query = $this->db->get();
		  			return $query->result();
		  		}
		  		else{

		  			return false;

		  		}
        }
        else{
    		if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager' || $user_type == 'division_head'){

	  			$this->db->select('k.kpi_id,k.kpi_name,d.division_name,ka.association_id,ka.target_value,k.repeat_duration,ka.actual_value');
	  			$this->db->from('kpi k,kpi_association ka, division d');
	  			$this->db->where('k.organization_id',$organization_id);
	  			$this->db->where('d.division_id = ka.division_id');
	  			$this->db->where('ka.division_id',$data['division_id']);
	  			$this->db->where('k.kpi_id = ka.kpi_id');
	  			$this->db->where('ka.employee_id = 0');
	  			$this->db->where('ka.active_status',1);
	  			$this->db->where('k.active_status',1);
	  			$query = $this->db->get();
	  			return $query->result();

	  		}
	  		else{
	  			return false;
		  	}
        }
  		
	}


	/**
	 * get single kpi records from RECORDS table for update previous data
	 */
	public function get_single_kpi_to_division($org_uniq_name,$user_type,$user_id,$association_id){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

  		if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager' || $user_type == 'division_head'){

  			$this->db->select('k.kpi_id,k.kpi_name,d.division_name,ka.association_id,r.target_value,k.repeat_duration,r.actual_value,r.created_time,r.record_id');
  			$this->db->from('kpi k,kpi_association ka, division d, records r');
  			$this->db->where('k.organization_id',$organization_id);
  			$this->db->where('d.division_id = ka.division_id');
  			$this->db->where('k.kpi_id = ka.kpi_id');
  			$this->db->where('r.association_id = ka.association_id');
  			$this->db->where('ka.association_id',$association_id);
  			$this->db->where('ka.active_status',1);
  			$this->db->where('k.active_status',1);
  			$query = $this->db->get();
  			return $query->result();

  		}
  		else{

  			return false;

  		}

	}




	/**
	 * get_kpi_list_for_divisions from ajax
	 * @param  [type] $user_id       [description]
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_list_for_division($div_id,$org_uid){

		$query = $this->db->query("SELECT `kpi_id`,`kpi_name` FROM `kpi` WHERE `kpi_id` in (SELECT `kpi_id` FROM `kpi` where `organization_id` in (
SELECT `organization_id` FROM `division` WHERE `division_id` = ".$div_id.") and  `kpi_id` not in ( SELECT `kpi_id` FROM `kpi_association` Where `division_id` = ".$div_id.")) ");

		

  			return $query->result();
	}

	/**
	 * assign_kpi_to_division 
	 * @param  [type] $data          [description]
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function assign_kpi_to_division($data,$org_uniq_name){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);
		
		$data = array(
			'division_id' =>   $data['division_id'],
			'organization_id' =>   $organization_id,
			'kpi_id' => $data['kpi_id'],
			'target_value' => $data['target_value']
			
		);

		$insert_query = $this->db->insert('kpi_association',$data);

	

		return  $insert_query;
	}

	/**
	 * get_kpi_to_division_info 
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $user_type     [description]
	 * @param  [type] $user_id       [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_to_division_info($kpi_association_id,$org_uniq_name,$user_type,$user_id){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		$this->db->select('k.kpi_id,k.kpi_name,d.division_name,ka.association_id,d.division_id,ka.target_value');
		$this->db->from('kpi k,kpi_association ka, division d');
		$this->db->where('k.organization_id',$organization_id);
		$this->db->where('d.division_id = ka.division_id');
		$this->db->where('k.kpi_id = ka.kpi_id');
		$this->db->where('ka.association_id',$kpi_association_id);
		$this->db->where('ka.active_status',1);
		$this->db->where('k.active_status',1);
		$query = $this->db->get();
		return $query->row();

	}

	/**
	 * update_kpi_to_division_info 
	 * @param  [type] $kpi_association_id [description]
	 * @param  [type] $data               [description]
	 * @param  [type] $org_uniq_name      [description]
	 * @return [type]                     [description]
	 */
	public function update_kpi_to_division_info($kpi_association_id,$data,$org_uniq_name){
		$data = array(
			'target_value' => $data['target_value']
		);

		$this->db->where('association_id',$kpi_association_id);
		$this->db->update('kpi_association',$data);
		return  true;
	}

	/**
	 * delete_kpi_to_division
	 * @param  [type] $kpi_association_id [description]
	 * @param  [type] $data               [description]
	 * @param  [type] $org_uniq_name      [description]
	 * @return [type]                     [description]
	 */
	public function delete_kpi_to_division($kpi_association_id,$data,$org_uniq_name){
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		$this->db->select('k.kpi_id,k.kpi_name,d.division_name,ka.association_id,d.division_id,ka.target_value');
		$this->db->from('kpi k,kpi_association ka, division d');
		$this->db->where('k.organization_id',$organization_id);
		$this->db->where('d.division_id = ka.division_id');
		$this->db->where('k.kpi_id = ka.kpi_id');
		$this->db->where('ka.association_id',$kpi_association_id);
		$this->db->where('ka.active_status',1);
		$this->db->where('k.active_status',1);
		$query = $this->db->get();
        if ($query->num_rows() > 0){
			$this->db->where('association_id',$kpi_association_id);
			$this->db->update('kpi_association',$data);
			return true;
        }
        else{
        	return false;
        }
	}


	public function getKPIAssociateDataForPreviousData($org_uniq_name,$user_type,$user_id,$association_id){
		//return the organization id
		$organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager' || $user_type == 'division_head'){

			$this->db->select('k.kpi_id,k.kpi_name,d.division_name,ka.association_id,r.target_value,k.repeat_duration,ka.sum_or_avg_target_value,ka.accumulated_actual_value,r.actual_value,r.created_time,r.record_id');
			$this->db->from('kpi k,kpi_association ka, division d, records r');
			$this->db->where('k.organization_id',$organization_id);
			$this->db->where('d.division_id = ka.division_id');
			$this->db->where('k.kpi_id = ka.kpi_id');
			$this->db->where('r.association_id = ka.association_id');
			$this->db->where('ka.association_id',$association_id);
			$this->db->where('ka.active_status',1);
			$this->db->where('k.active_status',1);
			$this->db->limit(1);
			$query = $this->db->get();
			return $query->row();

		}
		else{

			return false;

		}

	}


	/**
	 *
	 */
	public function saveKPIAssociateDataForPreviousData($data){

		if($data){
			$this->db->insert('records',$data);
			return true;
		}else{
			return false;
		}

	}


}
