<?php

class Organization_model extends  CI_Model{

     /**
     * Get all organization details
     * @return mixed
     */
    public function get_organizations($user_id, $user_type){

        if($user_type == 'super_admin' || $user_type == 'sys_admin'){
            $this->db->where('active_status',1);
            $query = $this->db->get('organization');
            return $query->result();
        }

        if($user_type == 'manager'){
            $this->db->select('o.*');
            $this->db->from('manager m,organization o');
            $this->db->where('m.user_id',$user_id);
            $this->db->where('m.organization_id = o.organization_id');
            $this->db->where('m.active_status',1);
            $query = $this->db->get();
            return $query->result();
        }
        if( $user_type == 'division_head'){
            $this->db->select('o.*');
            $this->db->from('divisional_head dh,organization o,division d');
            $this->db->where('dh.user_id',$user_id);
            $this->db->where('dh.division_id = d.division_id');
            $this->db->where('d.organization_id = o.organization_id');
            $this->db->where('dh.active_status',1);
            $query = $this->db->get();
            return $query->result();
        }
       
    }


     /**
     * Create Organization
     * @param $data
     * @return mixed
     */
    public function create_organization($data){
        $insert_query = $this->db->insert('organization',$data);
        return $insert_query;
    }

     /**
     * Get organization details
     * @return mixed
     */
    public function get_organization($user_id, $user_type,$org_uniq_name){

        if($user_type == 'super_admin' || $user_type == 'sys_admin'){
            $this->db->where('active_status',1);
            $this->db->where('organization_uid',$org_uniq_name);
            $query = $this->db->get('organization');
            return $query->result();
        }

    }

     /**
     * Get organization details for edit
     * @param $project_id
     * @return mixed
     */
    public function get_organization_info($organization_id){
        $this->db->where('organization_id',$organization_id);
        $get_data = $this->db->get('organization');
        return $get_data->row();
    }

    /**
     * Update organization data
     * @param $organization_id
     * @param $data
     * @return bool
     */
    public function update_organization_info($organization_id,$data){
        $this->db->where('organization_id',$organization_id);
        $this->db->update('organization',$data);
        return true;
    }

     /**
     * Delete organization
     * @param $project_id
     * @return bool
     */
    public function  delete_organization($organization_id,$data){
        $this->db->where('organization_id',$organization_id);
        $this->db->update('organization',$data);
        return true;
    }


    /**
     * return organization ID when pass the organization UID
     * @param  [type] $organization_uid [description]
     * @return [type]                   [description]
     */
    public function get_organization_id($organization_uid){
        $this->db->where('organization_uid',$organization_uid);
        $query = $this->db->get('organization');
        return $query->row()->organization_id;
    }

    /**
     * return organization ID when pass the organization UID
     * @param  [type] $organization_uid [description]
     * @return [type]                   [description]
     */
    public function get_organization_name($organization_id){
        $this->db->where('organization_id',$organization_id);
        $query = $this->db->get('organization');
        return $query->row()->organization_name;
    }

    /**
     * get_organization_id_by_user(manager id)
     * @param $user_id
     * @return mixed
     */
    public function get_organization_id_by_user($user_id){
        $this->db->where('user_id',$user_id);
        $query = $this->db->get('manager');
        return $query->row()->organization_id;
    }


}