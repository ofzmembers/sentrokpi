<?php

class Auth_model extends  CI_Model{

    public function login_user($username,$password){
        //return $username." ".$password;
        $this->db->where('username', $username);
        $result =  $this->db->get('user');
        $ret = $result->row();

        $db_password = $ret->password;
        $post_password = $password;

        $return_data = array(
            'user_id' => $ret->user_id,
            'user_type' => $ret->user_type,
            'username' => $ret->username
        );


        if($db_password == $post_password){
            return $return_data;
        }else{
            return false;
        }
    }

    public function is_org_access($org_uniq_name,$user_id,$user_type){


        if($user_type == 'super_admin' || $user_type == 'sys_admin'){

            $this->db->select('*');
            $this->db->from('organization');
            $this->db->where('organization_uid',$org_uniq_name);
            $this->db->where('active_status',1);
            $query = $this->db->get();

            if($query->num_rows() > 0){
                return true;
            } else {
                 return false;
            }
        }
        else if($user_type == 'manager' ){

            $this->db->select('o.*');
            $this->db->from('manager m,organization o');
            $this->db->where('m.user_id',$user_id);
            $this->db->where('m.organization_id = o.organization_id');
            $this->db->where('o.organization_uid',$org_uniq_name);
            $this->db->where('m.active_status',1);
            $query = $this->db->get();

            if($query->num_rows() > 0){
                return true;
            } else {
                 return false;
            }
        }
        else if($user_type == 'division_head'){
            $this->db->select('o.*');
            $this->db->from('divisional_head dh,organization o,division d');
            $this->db->where('dh.user_id',$user_id);
            $this->db->where('dh.division_id = d.division_id');
            $this->db->where('d.organization_id = o.organization_id');
            $this->db->where('o.organization_uid',$org_uniq_name);
            $this->db->where('dh.active_status',1);
            $query = $this->db->get();
            if($query->num_rows() > 0){
                return true;
            } else {
                 return false;
            }
        }
      
    }


    public function is_section($section_val,$user_id,$user_type,$method_name){

        //Method = report
        if($method_name == 'report'){
            $sections_admin = array('kpi-report','kpi-indicator');
            $sections_manager = array('kpi-report','kpi-indicator');

            if($user_type == 'super_admin' || $user_type == 'sys_admin'){
                 foreach ($sections_admin as $section){
                    if($section == $section_val){
                        return true;
                    }
                 } 
            }
            else if($user_type == 'manager'){
                 foreach ($sections_manager as $section){
                    if($section == $section_val){
                        return true;
                    }
                 } 
            }
            else{
                 return false;
            }
        }

        //Method = data
        if($method_name == 'data'){

            $sections_admin = array('organization-kpi-list','division-kpi-list','employee-kpi-list');
            $sections_manager = array('organization-kpi-list','division-kpi-list','employee-kpi-list');
            $sections_head = array('division-kpi-list','employee-kpi-list');

            if($user_type == 'super_admin' || $user_type == 'sys_admin'){
                 foreach ($sections_admin as $section){
                    if($section == $section_val){
                        return true;
                    }
                 } 
            }
            else if($user_type == 'manager'){
                 foreach ($sections_manager as $section){
                    if($section == $section_val){
                        return true;
                    }
                 } 
            } 
            else if($user_type == 'division_head' ){
                foreach ($sections_head as $section){
                    if($section == $section_val){
                        return true;
                    }
                 } 
            } 
            else{
                return false;
            }
        }

         //Method = manage
        if($method_name == 'manage'){
            $sections_admin = array('manager','division','division_head','employee','kpi','assign_kpi_to_division','assign_kpi_to_employee');
            $sections_manager = array('division','division_head','employee','kpi','assign_kpi_to_division','assign_kpi_to_employee');

            if($user_type == 'super_admin' || $user_type == 'sys_admin'){
                 foreach ($sections_admin as $section){
                    if($section == $section_val){
                        return true;
                    }
                 } 
            }
            else if($user_type == 'manager' || $user_type == 'division_head'){
                 foreach ($sections_admin as $section){
                    if($section == $section_val){
                        return true;
                    }
                 } 
            }  
            else{
                return false;
            }
        }
        else{
             return false;
        }

    
    }


    public function is_action($action,$user_id,$user_type,$section_val,$method_name){


        if($method_name == 'data'){

            $sections_admin = array('organization-kpi-list','division-kpi-list','employee-kpi-list');
            $sections_manager = array('organization-kpi-list','division-kpi-list','employee-kpi-list');
            $sections_head = array('division-kpi-list','employee-kpi-list');
             
            if($user_type == 'super_admin' || $user_type == 'sys_admin'){
                foreach ($sections_admin as $section){
                    if($section == $section_val){
                        if($action == ''){
                            return true;   
                        }
                        else if($action == 'edit'){
                            return true;   
                        }
                        else if($action == 'previous-data'){
                            return true;
                        }
                        else{
                            return false;
                        }
                    }
                } 
            }
            else if($user_type == 'manager'){
                foreach ($sections_manager as $section){
                    if($section == $section_val){
                         if($action == ''){
                            return true;   
                        }else if($action == 'edit'){
                            return true;   
                        }else if($action == 'previous-data'){
                             return true;
                        }else{
                            return false;
                        }
                    }
                } 
            }
            else if($user_type == 'division_head'){
                foreach ($sections_head as $section){
                    if($section == $section_val){
                         if($action == ''){
                            return true;   
                        }else if($action == 'edit'){
                            return true;   
                        }else if($action == 'previous-data'){
                             return true;
                        }
                         else{
                            return false;
                        }
                    }
                } 
            }
            else{

                return false;
            }
        }

        else if($method_name == 'manage'){
            
            $sections_admin = array('manager','division','division_head','employee','kpi','assign_kpi_to_division','assign_kpi_to_employee','organization-kpi-list');
            $sections_manager = array('division','division_head','employee','kpi','assign_kpi_to_division','assign_kpi_to_employee');
             
            if($user_type == 'super_admin' || $user_type == 'sys_admin'){
                foreach ($sections_admin as $section){
                    if($section == $section_val){
                         if($action == ''){
                            return true;   
                        }else if($action == 'create'){
                            return true;
                        }else if($action == 'edit'){
                            return true;    
                        }else if($action == 'delete'){
                            return true;
                        }else{
                            return false;
                        }
                    }
                } 
            }
            else if($user_type == 'manager'){
                foreach ($sections_manager as $section){
                    if($section == $section_val){
                         if($action == ''){
                            return true;   
                        }else if($action == 'create'){
                            return true;
                        }else if($action == 'edit'){
                            return true;    
                        }else if($action == 'delete'){
                            return true;
                        }else{
                            return false;
                        }
                    }
                } 
            }
            else{

                return false;
            }
        }

        else if($method_name == 'report'){
            
            $sections_admin = array('kpi-report','kpi-indicator');
            $sections_manager = array('kpi-report','kpi-indicator');
             
            if($user_type == 'super_admin' || $user_type == 'sys_admin'){
                foreach ($sections_admin as $section){
                    if($section == $section_val){
                        if($action == ''){
                            return true;   
                        }else if($action == 'kpi-id'){
                            return true;
                        }else if($action == 'kpi-report-print'){
                            return true;
                        }else if($action == 'download'){
                            return true;
                        }else{
                            return false;
                        }
                    }
                } 
            }
            else if($user_type == 'manager'){
                foreach ($sections_manager as $section){
                    if($section == $section_val){
                         if($action == ''){
                            return true;   
                        }else if($action == 'kpi-id'){
                            return true;
                        }else if($action == 'kpi-report-print'){
                            return true;
                        }else if($action == 'download'){
                            return true;
                        }else{
                            return false;
                        }
                    }
                } 
            }
            else{

                return false;
            }
        }


        else{

            return false;

        }

       

       
    }

    


}