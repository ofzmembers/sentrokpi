<?php

class Kpi_model extends  CI_Model{


	/**
	 * get_kpis 
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function get_kpis($org_uniq_name,$user_type,$user_id){
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

  		if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager'){

  			$this->db->select('*');
  			$this->db->from('kpi');
  			$this->db->where('organization_id',$organization_id);
  			$this->db->where('active_status',1);
  			$query = $this->db->get();
  			return $query->result();

  		}
  		else{

  			return false;

  		}

	}




	/**
	 * [get_single_kpi_details_for_organization_report description]
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $user_type     [description]
	 * @param  [type] $user_id       [description]
	 * @return [type]                [description]
	 */
	public function get_single_kpi_details_for_organization_report($organization_id,$kpi_id){


		$query = $this->db->query("SELECT k.`kpi_id`,k.`kpi_name`,k.`repeat_duration`,sum(r.`target_value`) as `total_target`,sum(r.`actual_value`) as `total_actual` FROM `kpi_association` ka ,`kpi` k,`records` r WHERE  ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`kpi_id` = ".$kpi_id." and  ka.`association_id` = r.`association_id` and ka.`active_status` = 1 and  k.`active_status` = 1 Group By ka.`kpi_id`");

        if ($query->num_rows() > 0){
			 return $query->row();
        }
        else{
        	return false;
        }
  		
	}



	/**
	 * create_kpi
	 * @param  [type] $data          [description]
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $kpi_by_val    [description]
	 * @return [type]                [description]
	 */
	public function create_kpi($data,$org_uniq_name,$kpi_by_val){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);
		if($kpi_by_val == "kpi_by_value"){
			 $data = array(
					'organization_id' => $organization_id,
					'kpi_name' =>$data['kpi_name'],
					'repeat_duration' => $data['repeat_duration'],
					'start_month' => $data['start_month'],
					'status_determination'=>'by_value',
					'operation_status'=>$data['operation_status'],
					'value1' =>$data['value_by_value1'],
					'value2' =>0,


            );

			$insert_query = $this->db->insert('kpi',$data);
        	return  $insert_query;
		}
		else if($kpi_by_val == 'kpi_by_range'){
			 $data = array(

			 	'organization_id' => $organization_id,
			 	'kpi_name' => $data['kpi_name'],
			 	'repeat_duration' => $data['repeat_duration'],
				 'start_month' => $data['start_month'],
			 	'status_determination' => 'by_range',
			 	'operation_status' => $data['operation_status2'],
			 	'value1' => $data['value_one'],
			 	'value2' => $data['value_two']
			 	

            );

			$insert_query = $this->db->insert('kpi',$data);
        	return  $insert_query;
		}
		else{
			return false;
		}

	}

	/**
	 * get_kpi_info
	 * @param  [type] $kpi_id        [description]
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_info($kpi_id,$org_uniq_name){
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		$this->db->select('*');
		$this->db->from('kpi');
		$this->db->where('organization_id',$organization_id);
		$this->db->where('kpi_id',$kpi_id);
		$this->db->where('active_status',1);
		$query = $this->db->get();
		return $query->row();
	}

	/**
	 * update_kpi_info
	 * @param  [type] $data          [description]
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $kpi_by_val    [description]
	 * @return [type]                [description]
	 */
	public function update_kpi_info($data,$org_uniq_name,$kpi_by_val,$kpi_id){
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		if($kpi_by_val == 'kpi_by_value'){
			 $data = array(

			 	'organization_id' =>  $organization_id,
			 	'kpi_name' => $data['kpi_name'],
			 	'description' => '',
			 	'repeat_duration' => $data['repeat_duration'],
				 'start_month' => $data['start_month'],
			 	'status_determination' => 'by_value',
			 	'operation_status' => $data['operation_status'],
			 	'value1' => $data['value_by_value1'],
			 	'value2' => ''

            );

			$this->db->where('kpi_id',$kpi_id);
			$this->db->update('kpi',$data);
			return  true;
		}
		else if($kpi_by_val == 'kpi_by_range'){
			 $data = array(

			 	'organization_id' => $organization_id,
			 	'kpi_name' => $data['kpi_name'],
			 	'description' => '' ,
			 	'repeat_duration' => $data['repeat_duration'],
				 'start_month' => $data['start_month'],
			 	'status_determination' => 'by_range',
			 	'operation_status' => $data['operation_status2'],
			 	'value1' => $data['value_one'],
			 	'value2' => $data['value_two']
            );

			$this->db->where('kpi_id',$kpi_id);
			$this->db->update('kpi',$data);
			return  true;
		}
		else{
			return false;
		}
	}

	/**
	 * delete_kpi
	 * @param  [type] $kpi_id        [description]
	 * @param  [type] $data          [description]
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function delete_kpi($kpi_id,$data,$org_uniq_name){
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		$this->db->select('*');
		$this->db->from('kpi');
		$this->db->where('organization_id',$organization_id);
		$this->db->where('kpi_id',$kpi_id);
		$this->db->where('active_status',1);
		$query = $this->db->get();
        if ($query->num_rows() > 0){
			$this->db->where('kpi_id',$kpi_id);
			$this->db->update('kpi',$data);
			return true;
        }
        else{
        	return false;
        }
	}




	/**
	 * get_kpi_list_for_employee ajax
	 * @param  [type] $employee_id [description]
	 * @param  [type] $org_uid     [description]
	 * @return [type]              [description]
	 */
	public function get_kpi_list_for_employee($employee_id,$org_uid){

			//return the organization id
        	$organization_id = $this->organization_model->get_organization_id($org_uid);

  			$this->db->select('k.kpi_id,k.kpi_name,ka.association_id,ka.target_value,k.repeat_duration,e.employee_name,ka.actual_value');
  			$this->db->from('kpi k,kpi_association ka, employee e');
  			$this->db->where('k.organization_id',$organization_id);
  			$this->db->where('ka.employee_id = e.employee_id');
  			$this->db->where('e.employee_id',$employee_id);
  			$this->db->where('k.kpi_id = ka.kpi_id');
  			$this->db->where('ka.active_status',1);
  			$this->db->where('k.active_status',1);
  			$query = $this->db->get();
  			return $query->result();
	}



		/**
	 * [get_kpi_list_for_organization_report description]
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $user_type     [description]
	 * @param  [type] $user_id       [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_list_for_organization_report($org_uniq_name,$user_type,$user_id){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

	


		$query = $this->db->query("select D.`kpi_id`, D.`kpi_name`, D.`repeat_duration`, D.`target`, D.`actual`, D.variation,ROUND( D.variation_presage,2) as variation_presage, D.`value1`, D.`value2`, D.`status_determination`, D.`operation_status`, D.YTD_target , ROUND(D.YTD_actual,2) as YTD_actual, ROUND((D.YTD_actual-D.YTD_target),2 )as YTD_variation, ROUND(((D.YTD_actual-D.YTD_target)/D.YTD_target)*100 , 2) as YTD_variation_presage From ( select C.`kpi_id`, C.`kpi_name`, C.`repeat_duration`, C.`target`, C.`actual`, C.variation, C.variation_presage, C.`value1`, C.`value2`, C.`status_determination`, C.`operation_status`, C.accumulated_target + C.`target` as YTD_target , C.accumulated_actual + C.`actual` as YTD_actual From ( select A.`kpi_id`, A.`kpi_name`, A.`repeat_duration`, A.`target`, A.`actual`, A.`value1`, A.`value2` , A.`status_determination`, A.`operation_status`, COALESCE( B.`total_target`,0) as accumulated_target, COALESCE( B.`total_actual`,0) as accumulated_actual, (A.`actual`-A.`target`) as variation, ((A.`actual`-A.`target`)/A.`target`)*100 as variation_presage From (SELECT k.`kpi_id` as kpi_id, k.`kpi_name` as kpi_name, k.`repeat_duration`, sum(ka.`target_value`) as `target`, sum(ka.`actual_value`) as `actual`, sum(k.`value1`) as `value1`, sum(k.`value2`) as `value2`, k.`status_determination`, k.`operation_status` FROM `kpi_association` ka , `kpi` k WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = 0 Group By ka.`association_id`) A left join (SELECT k.`kpi_id`, k.`kpi_name`, k.`repeat_duration`, sum(r.`target_value`) as `total_target`, sum(r.`actual_value`) as `total_actual`, sum(k.`value1`) , sum(k.`value2`), k.`status_determination`, k.`operation_status` FROM `kpi_association` ka , `kpi` k,`records` r WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = 0 and r.`association_id` = ka.`association_id` Group By ka.`association_id`) B ON A.kpi_id = B.kpi_id ) AS C ) AS D");

  		return $query->result();
	}




	/**
	 * get_kpi_for_reports
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $user_type     [description]
	 * @param  [type] $user_id       [description]
	 * @param  [type] $data          [description]
	 * @return [type]                [description]
	 */
	//public function get_kpi_for_reports($org_uniq_name,$user_type,$user_id,$data){
	public function get_kpi_for_reports($organization_uid,$division_id,$employee_id,$from_date,$to_date){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($organization_uid);

      

		if($division_id == "all" && $employee_id == "all"){


			$query = $this->db->query("select D.`kpi_id`, D.`kpi_name`, D.`repeat_duration`, D.`target`, D.`actual`, D.variation,ROUND( D.variation_presage,2) as variation_presage, D.`value1`, D.`value2`, D.`status_determination`, D.`operation_status`, D.YTD_target ,ROUND( D.YTD_actual,2) as YTD_actual, ROUND((D.YTD_actual-D.YTD_target),2) as YTD_variation, ROUND(((D.YTD_actual-D.YTD_target)/D.YTD_target)*100 , 2) as YTD_variation_presage From ( select C.`kpi_id`, C.`kpi_name`, C.`repeat_duration`, C.`target`, C.`actual`, C.variation, C.variation_presage, C.`value1`, C.`value2`, C.`status_determination`, C.`operation_status`, C.accumulated_target + C.`target` as YTD_target , C.accumulated_actual + C.`actual` as YTD_actual From ( select A.`kpi_id`, A.`kpi_name`, A.`repeat_duration`, A.`target`, A.`actual`, A.`value1`, A.`value2` , A.`status_determination`, A.`operation_status`, COALESCE( B.`total_target`,0) as accumulated_target, COALESCE( B.`total_actual`,0) as accumulated_actual, (A.`actual`-A.`target`) as variation, ((A.`actual`-A.`target`)/A.`target`)*100 as variation_presage From (SELECT k.`kpi_id` as kpi_id, k.`kpi_name` as kpi_name, k.`repeat_duration`, sum(ka.`target_value`) as `target`, sum(ka.`actual_value`) as `actual`, sum(k.`value1`) as `value1`, sum(k.`value2`) as `value2`, k.`status_determination`, k.`operation_status` FROM `kpi_association` ka , `kpi` k WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = 0 and (ka.`created_time` BETWEEN '$from_date' AND '$to_date')  Group By ka.`association_id`) A left join (SELECT k.`kpi_id`, k.`kpi_name`, k.`repeat_duration`, sum(r.`target_value`) as `total_target`, sum(r.`actual_value`) as `total_actual`, sum(k.`value1`) , sum(k.`value2`), k.`status_determination`, k.`operation_status` FROM `kpi_association` ka , `kpi` k,`records` r WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = 0 and r.`association_id` = ka.`association_id` and (ka.`created_time` BETWEEN '$from_date' AND '$to_date')  Group By ka.`association_id`) B ON A.kpi_id = B.kpi_id ) AS C ) AS D");
			//$query = $this->db->query("select D.`kpi_id`, D.`kpi_name`, D.`repeat_duration`, D.`target`, D.`actual`, D.variation,ROUND( D.variation_presage,2) as variation_presage, D.`value1`, D.`value2`, D.`status_determination`, D.`operation_status`, D.YTD_target ,ROUND( D.YTD_actual,2) as YTD_actual, ROUND((D.YTD_actual-D.YTD_target),2) as YTD_variation, ROUND(((D.YTD_actual-D.YTD_target)/D.YTD_target)*100 , 2) as YTD_variation_presage From ( select C.`kpi_id`, C.`kpi_name`, C.`repeat_duration`, C.`target`, C.`actual`, C.variation, C.variation_presage, C.`value1`, C.`value2`, C.`status_determination`, C.`operation_status`, C.accumulated_target + C.`target` as YTD_target , C.accumulated_actual + C.`actual` as YTD_actual From ( select A.`kpi_id`, A.`kpi_name`, A.`repeat_duration`, A.`target`, A.`actual`, A.`value1`, A.`value2` , A.`status_determination`, A.`operation_status`, COALESCE( B.`total_target`,0) as accumulated_target, COALESCE( B.`total_actual`,0) as accumulated_actual, (A.`actual`-A.`target`) as variation, ((A.`actual`-A.`target`)/A.`target`)*100 as variation_presage From (SELECT k.`kpi_id` as kpi_id, k.`kpi_name` as kpi_name, k.`repeat_duration`, sum(ka.`target_value`) as `target`, sum(ka.`actual_value`) as `actual`, sum(k.`value1`) as `value1`, sum(k.`value2`) as `value2`, k.`status_determination`, k.`operation_status` FROM `kpi_association` ka , `kpi` k WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = 0  Group By ka.`association_id`) A left join (SELECT k.`kpi_id`, k.`kpi_name`, k.`repeat_duration`, sum(r.`target_value`) as `total_target`, sum(r.`actual_value`) as `total_actual`, sum(k.`value1`) , sum(k.`value2`), k.`status_determination`, k.`operation_status` FROM `kpi_association` ka , `kpi` k,`records` r WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = 0 and r.`association_id` = ka.`association_id` Group By ka.`association_id`) B ON A.kpi_id = B.kpi_id ) AS C ) AS D");

  			return $query->result();

		}
		else if($employee_id == "all" && is_numeric($division_id)){


		$query = $this->db->query("select D.`kpi_id`, D.`kpi_name`, D.`repeat_duration`, D.`target`, D.`actual`, D.variation,ROUND( D.variation_presage,2) as variation_presage, D.`value1`, D.`value2`, D.`status_determination`, D.`operation_status`, D.YTD_target ,ROUND( D.YTD_actual,2) as YTD_actual, ROUND((D.YTD_actual-D.YTD_target),2) as YTD_variation,ROUND( ((D.YTD_actual-D.YTD_target)/D.YTD_target)*100 ,2) as YTD_variation_presage From ( select C.`kpi_id`, C.`kpi_name`, C.`repeat_duration`, C.`target`, C.`actual`, C.variation, C.variation_presage, C.`value1`, C.`value2`, C.`status_determination`, C.`operation_status`, C.accumulated_target + C.`target` as YTD_target , C.accumulated_actual + C.`actual` as YTD_actual From ( SELECT a.`kpi_id`, a.`kpi_name`, a.`repeat_duration`, a.`target` , a.`actual`, a.`value1`, a.`value2` , a.`status_determination`, a.`operation_status`, a.`accumulated_target` , a.`accumulated_actual`, (a.`actual`- a.`target` ) as variation, ((a.`actual`- a.`target` )/ a.`target` )*100 as variation_presage FROM ( SELECT X.`kpi_id`, X.`kpi_name`, X.`repeat_duration`, X.`target` , X.`actual`, X.`value1`, X.`value2` , X.`status_determination`, X.`operation_status`, COALESCE( Y.accumulated_target,0) as accumulated_target , COALESCE( Y.accumulated_actual ,0) as accumulated_actual FROM (SELECT k.`kpi_id` as kpi_id, k.`kpi_name` as kpi_name, k.`repeat_duration` as repeat_duration, ka.`target_value` as target , ka.`actual_value` as actual, sum(k.`value1`) as value1, sum(k.`value2`) as value2, k.`status_determination` as status_determination, k.`operation_status` as operation_status FROM `kpi` k join `kpi_association` ka on k.`kpi_id` = ka.`kpi_id` WHERE ka.`organization_id` = ".$organization_id." and ka.`employee_id` = 0 and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`division_id` = ".$division_id." and (ka.`created_time` BETWEEN '$from_date' AND '$to_date') group by k.`kpi_id` ) as X left join (SELECT k.`kpi_id` as kpi_id, k.`kpi_name` as kpi_name, k.`repeat_duration` as repeat_duration, ka.`target_value` as target , ka.`actual_value` as actual, sum(k.`value1`) as value1, sum(k.`value2`) as value2, k.`status_determination` as status_determination, k.`operation_status` as operation_status, sum(r.`target_value`) as accumulated_target , sum(r.`actual_value`) as accumulated_actual FROM `kpi_association` ka , `kpi` k, `records` r WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`employee_id` = 0 and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`division_id` = ".$division_id." and r.`association_id` = ka.`association_id` and (ka.`created_time` BETWEEN '$from_date' AND '$to_date') group by k.`kpi_id`) as Y on X.kpi_id = Y.kpi_id ) as a ) AS C ) AS D ");

			

			return $query->result();	
		}
		else if(is_numeric($division_id) && is_numeric($employee_id)){
			
			
			$query = $this->db->query("select D.`kpi_id`, D.`kpi_name`, D.`repeat_duration`, D.`target`, D.`actual`, D.variation,ROUND( D.variation_presage,2) as variation_presage, D.`value1`, D.`value2`, D.`status_determination`, D.`operation_status`, D.YTD_target , ROUND( D.YTD_actual,2) as YTD_actual, ROUND((D.YTD_actual-D.YTD_target),2) as YTD_variation,ROUND( ((D.YTD_actual-D.YTD_target)/D.YTD_target)*100,2) as YTD_variation_presage From ( select C.`kpi_id`, C.`kpi_name`, C.`repeat_duration`, C.`target`, C.`actual`, C.variation, C.variation_presage, C.`value1`, C.`value2`, C.`status_determination`, C.`operation_status`, C.accumulated_target + C.`target` as YTD_target , C.accumulated_actual + C.`actual` as YTD_actual From ( SELECT a.`kpi_id`, a.`kpi_name`, a.`repeat_duration`, a.`target` , a.`actual`, a.`value1`, a.`value2` , a.`status_determination`, a.`operation_status`, a.`accumulated_target` , a.`accumulated_actual`, (a.`actual`- a.`target` ) as variation, ((a.`actual`- a.`target` )/ a.`target` )*100 as variation_presage FROM ( SELECT X.`kpi_id`, X.`kpi_name`, X.`repeat_duration`, X.`target` , X.`actual`, X.`value1`, X.`value2` , X.`status_determination`, X.`operation_status`, COALESCE( Y.accumulated_target,0) as accumulated_target , COALESCE( Y.accumulated_actual ,0) as accumulated_actual FROM (SELECT k.`kpi_id` as kpi_id, k.`kpi_name` as kpi_name, k.`repeat_duration` as repeat_duration, ka.`target_value` as target , ka.`actual_value` as actual, sum(k.`value1`) as value1, sum(k.`value2`) as value2, k.`status_determination` as status_determination, k.`operation_status` as operation_status FROM `kpi` k join `kpi_association` ka on k.`kpi_id` = ka.`kpi_id` WHERE ka.`organization_id` = ".$organization_id." and ka.`employee_id` != 0 and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = ".$employee_id." and (ka.`created_time` BETWEEN '$from_date' AND '$to_date') group by k.`kpi_id` ) as X left join (SELECT k.`kpi_id` as kpi_id, k.`kpi_name` as kpi_name, k.`repeat_duration` as repeat_duration, ka.`target_value` as target , ka.`actual_value` as actual, sum(k.`value1`) as value1, sum(k.`value2`) as value2, k.`status_determination` as status_determination, k.`operation_status` as operation_status, sum(r.`target_value`) as accumulated_target , sum(r.`actual_value`) as accumulated_actual FROM `kpi_association` ka , `kpi` k, `records` r WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`employee_id` != 0 and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = ".$employee_id." and r.`association_id` = ka.`association_id` and (ka.`created_time` BETWEEN '$from_date' AND '$to_date') group by k.`kpi_id`) as Y on X.kpi_id = Y.kpi_id ) as a ) AS C ) AS D ");
			
			return $query->result();
		}	
		else{
			
			$query = $this->db->query("select D.`kpi_id`, D.`kpi_name`, D.`repeat_duration`, D.`target`, D.`actual`, D.variation, ROUND( D.variation_presage,2) as variation_presage, D.`value1`, D.`value2`, D.`status_determination`, D.`operation_status`, D.YTD_target ,ROUND( D.YTD_actual,2) as YTD_actual, ROUND((D.YTD_actual-D.YTD_target),2) as YTD_variation, ROUND(((D.YTD_actual-D.YTD_target)/D.YTD_target)*100 , 2) as YTD_variation_presage From ( select C.`kpi_id`, C.`kpi_name`, C.`repeat_duration`, C.`target`, C.`actual`, C.variation, C.variation_presage, C.`value1`, C.`value2`, C.`status_determination`, C.`operation_status`, C.accumulated_target + C.`target` as YTD_target , C.accumulated_actual + C.`actual` as YTD_actual From ( select A.`kpi_id`, A.`kpi_name`, A.`repeat_duration`, A.`target`, A.`actual`, A.`value1`, A.`value2` , A.`status_determination`, A.`operation_status`, COALESCE( B.`total_target`,0) as accumulated_target, COALESCE( B.`total_actual`,0) as accumulated_actual, (A.`actual`-A.`target`) as variation, ((A.`actual`-A.`target`)/A.`target`)*100 as variation_presage From (SELECT k.`kpi_id` as kpi_id, k.`kpi_name` as kpi_name, k.`repeat_duration`, sum(ka.`target_value`) as `target`, sum(ka.`actual_value`) as `actual`, sum(k.`value1`) as `value1`, sum(k.`value2`) as `value2`, k.`status_determination`, k.`operation_status` FROM `kpi_association` ka , `kpi` k WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = 0 and (ka.`created_time` BETWEEN '$from_date' AND '$to_date') Group By ka.`association_id`) A left join (SELECT k.`kpi_id`, k.`kpi_name`, k.`repeat_duration`, sum(r.`target_value`) as `total_target`, sum(r.`actual_value`) as `total_actual`, sum(k.`value1`) , sum(k.`value2`), k.`status_determination`, k.`operation_status` FROM `kpi_association` ka , `kpi` k,`records` r WHERE ka.`organization_id` = ".$organization_id." and k.`kpi_id` = ka.`kpi_id` and ka.`active_status` = 1 and k.`active_status` = 1 and ka.`employee_id` = 0 and r.`association_id` = ka.`association_id` and (ka.`created_time` BETWEEN '$from_date' AND '$to_date') Group By ka.`association_id`) B ON A.kpi_id = B.kpi_id ) AS C ) AS D");

  			return $query->result();
		}
		

	}



	/**
	 * get_kpi_for_indicators
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $user_type     [description]
	 * @param  [type] $user_id       [description]
	 * @param  [type] $data          [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_for_indicators($organization_uid ,$division_id,$employee_id){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($organization_uid);

		if($division_id == "all" && $employee_id == "all"){

			$query = $this->db->query("select A.`kpi_id`,A.`kpi_name`,A.`repeat_duration`,A.`target`,A.`actual`,A.`value1`,A.`value2` ,A.`status_determination`,A.`operation_status`,COALESCE( B.`total_target`,0) as accumulated_target,COALESCE( B.`total_actual`,0) as accumulated_actual,(A.`actual`-A.`target`) as variation, ((A.`actual`-A.`target`)/A.`target`)*100 as variation_presage From 
				(SELECT k.`kpi_id` as kpi_id, k.`kpi_name` as kpi_name, k.`repeat_duration`, sum(ka.`target_value`) as `target`,sum(ka.`actual_value`) as `actual`,k.`value1` as `value1`,k.`value2` as `value2`,k.`status_determination`,k.`operation_status`
				 FROM `kpi_association` ka ,`kpi` k 
				 WHERE ka.`organization_id` = ".$organization_id." 
				 and k.`kpi_id` = ka.`kpi_id` 
				 and ka.`active_status` = 1
				 and  k.`active_status` = 1  
				 and ka.`employee_id` = 0 
				 Group By ka.`kpi_id`) 
				 A left join 
				 (SELECT k.`kpi_id`,k.`kpi_name`,k.`repeat_duration`, sum(r.`target_value`) as `total_target`,sum(r.`actual_value`) as `total_actual`,k.`value1` ,k.`value2`,k.`status_determination`,k.`operation_status`
				  FROM `kpi_association` ka , `kpi` k,`records` r 
				  WHERE  ka.`organization_id` = ".$organization_id."  and k.`kpi_id` = ka.`kpi_id` and ka.`active_status` = 1 and  k.`active_status` = 1  and ka.`employee_id` = 0 and  r.`association_id` = ka.`association_id`  Group By ka.`kpi_id`) B ON A.kpi_id = B.kpi_id
			");

  			return $query->result();

		}
		else if($employee_id == "all" && is_numeric($division_id)){

			$query = $this->db->query("
				SELECT a.`kpi_id`,a.`kpi_name`,a.`repeat_duration`,
					a.`target` ,
					a.`actual`,
					a.`value1`,
					a.`value2` ,
					a.`status_determination`,
					a.`operation_status`,
					a.`accumulated_target` ,
					a.`accumulated_actual`,
					(a.`actual`- a.`target` ) as variation, 
					((a.`actual`- a.`target` )/ a.`target` )*100 as variation_presage 
					FROM ( 
					SELECT
					X.`kpi_id`,
					X.`kpi_name`,
					X.`repeat_duration`,
					X.`target` ,
					X.`actual`,
					X.`value1`,
					X.`value2` ,
					X.`status_determination`,
					X.`operation_status`,
					COALESCE( Y.accumulated_target,0) as accumulated_target ,
					COALESCE( Y.accumulated_actual ,0) as accumulated_actual 
					FROM
					(SELECT 
					k.`kpi_id` as kpi_id,
					k.`kpi_name` as kpi_name,
					k.`repeat_duration` as repeat_duration,
					ka.`target_value` as target ,
					ka.`actual_value` as actual,
					k.`value1` as value1,
					k.`value2` as value2,
					k.`status_determination` as status_determination,
					k.`operation_status` as operation_status
					FROM 
					`kpi` k join `kpi_association` ka on k.`kpi_id` = ka.`kpi_id`
					WHERE  
					ka.`organization_id` = ".$organization_id."   and 
					ka.`employee_id` = 0  and 
					ka.`active_status` = 1 and  
					k.`active_status` = 1  and  
					ka.`division_id` = ".$division_id."  
					group by k.`kpi_id` ) as X left join 
					(SELECT 
					k.`kpi_id` as kpi_id,
					k.`kpi_name` as kpi_name,
					k.`repeat_duration` as repeat_duration,
					ka.`target_value` as target ,
					ka.`actual_value` as actual,
					k.`value1` as value1,
					k.`value2` as value2,
					k.`status_determination` as status_determination,
					k.`operation_status` as operation_status,
					sum(r.`target_value`) as accumulated_target ,
					sum(r.`actual_value`) as accumulated_actual 
					FROM 
					`kpi_association` ka ,
					`kpi` k,
					`records` r  
					WHERE  
					ka.`organization_id` = ".$organization_id."   and 
					k.`kpi_id` = ka.`kpi_id` and ka.`employee_id` = 0  and 
					ka.`active_status` = 1 and  k.`active_status` = 1  and  
					ka.`division_id` = ".$division_id."  and 
					r.`association_id` = ka.`association_id` 
					group by k.`kpi_id`) as Y 
					on X.kpi_id = Y.kpi_id
					) as a

			");

			
			return $query->result();	
		}
		else if(is_numeric($division_id) && is_numeric($employee_id)){
			
			
			$query = $this->db->query("
				
					SELECT 
					a.`kpi_id`,
					a.`kpi_name`,
					a.`repeat_duration`,
					a.`target` ,
					a.`actual`,
					a.`value1`,
					a.`value2` ,
					a.`status_determination`,
					a.`operation_status`,
					a.`accumulated_target` ,
					a.`accumulated_actual`,
					(a.`actual`- a.`target` ) as variation, 
					((a.`actual`- a.`target` )/ a.`target` )*100 as variation_presage 
					FROM ( 
					SELECT
					X.`kpi_id`,
					X.`kpi_name`,
					X.`repeat_duration`,
					X.`target` ,
					X.`actual`,
					X.`value1`,
					X.`value2` ,
					X.`status_determination`,
					X.`operation_status`,
					COALESCE( Y.accumulated_target,0) as accumulated_target ,
					COALESCE( Y.accumulated_actual ,0) as accumulated_actual 
					FROM
					(SELECT 
					k.`kpi_id` as kpi_id,
					k.`kpi_name` as kpi_name,
					k.`repeat_duration` as repeat_duration,
					ka.`target_value` as target ,
					ka.`actual_value` as actual,
					k.`value1` as value1,
					k.`value2` as value2,
					k.`status_determination` as status_determination,
					k.`operation_status` as operation_status
					FROM 
					`kpi` k join `kpi_association` ka on k.`kpi_id` = ka.`kpi_id`
					WHERE  
					ka.`organization_id` =  ".$organization_id."    and 
					ka.`employee_id` != 0  and 
					ka.`active_status` = 1 and  
					k.`active_status` = 1  and  
					ka.`employee_id` = ".$employee_id." 
					group by k.`kpi_id` ) as X left join 
					(SELECT 
					k.`kpi_id` as kpi_id,
					k.`kpi_name` as kpi_name,
					k.`repeat_duration` as repeat_duration,
					ka.`target_value` as target ,
					ka.`actual_value` as actual,
					k.`value1` as value1,
					k.`value2` as value2,
					k.`status_determination` as status_determination,
					k.`operation_status` as operation_status,
					sum(r.`target_value`) as accumulated_target ,
					sum(r.`actual_value`) as accumulated_actual 
					FROM 
					`kpi_association` ka ,
					`kpi` k,
					`records` r  
					WHERE  
					ka.`organization_id` =  ".$organization_id."    and 
					k.`kpi_id` = ka.`kpi_id` and ka.`employee_id` != 0  and 
					ka.`active_status` = 1 and  k.`active_status` = 1  and  
					ka.`employee_id` = ".$employee_id." and 
					r.`association_id` = ka.`association_id` 
					group by k.`kpi_id`) as Y 
					on X.kpi_id = Y.kpi_id
					) as a			

			");


			return $query->result();
		}	
		else{
			return false;
		}

	}


	/**
	 * [get_available_employee_kpi_target_balance description]
	 * ajax
	 * @param  [type] $organization_uid [description]
	 * @param  [type] $kpi_id           [description]
	 * @return [type]                   [description]
	 */
	public function get_available_employee_kpi_target_balance($organization_uid ,$kpi_id,$employee_id){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($organization_uid);

        //get divisioon id
        $division_id = $this->employee_model->get_division_id($employee_id);
        
        $kpi_id = $kpi_id;

        $query = $this->db->query("SELECT COALESCE(SUM(`target_value`),0) -(SELECT COALESCE(SUM(`target_value`),0) as `available_balance` from kpi_association where `organization_id` =  ".$organization_id." and `kpi_id` = ".$kpi_id." and `division_id` = ".$division_id." and `employee_id` != 0) as `final` from kpi_association where `organization_id` =  ".$organization_id." and `kpi_id` = ".$kpi_id." and `division_id` = ".$division_id." and `employee_id` = 0 ");

        return $query->result();

		
	}

	/**
	 * [get_kpi_id description]
	 * @param  [type] $kpi_association_id [description]
	 * @return [type]                     [description]
	 */
	public function get_kpi_id($kpi_association_id){
		$this->db->select('*');
        $this->db->from('kpi_association');
        $this->db->where('association_id',$kpi_association_id);
        $query = $this->db->get();
        $row = $query->row(); 
        return $row->kpi_id;
	}





}