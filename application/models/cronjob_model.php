<?php

class Cronjob_model extends  CI_Model{

	// repeat_duration
	// 1 = daily
	// 2 = Weekly
	// 3 = Monthly
	// 4 = Quaterly
	// 5 = Annually
	// 

    //1  0	0	*	*	* curl http://107.170.33.220/test/sentrokpi.com/cronjob/daily
    //2  0	0	*	*	0 curl http://107.170.33.220/test/sentrokpi.com/cronjob/weekly
    //3  0	0	1	*	* curl http://107.170.33.220/test/sentrokpi.com/cronjob/monthly
    //4  0	0	1	1	* curl http://107.170.33.220/test/sentrokpi.com/cronjob/annually
    //5  0	0	1	*/4	* curl http://107.170.33.220/test/sentrokpi.com/cronjob/quaterly


	public function update_tables($daily_repeat){
		date_default_timezone_set('Asia/Calcutta');

		$now_time = date("Y-m-d H:i:s");
		$this->db->select('ka.target_value,ka.actual_value,ka.sum_or_avg_target_value,ka.accumulated_actual_value,ka.association_id');
        $this->db->from('kpi_association ka,kpi k');
        $this->db->where('ka.kpi_id = k.kpi_id ');
        $this->db->where('k.repeat_duration',$daily_repeat); 
        $this->db->where('ka.active_status',1); 
        $this->db->where('k.active_status',1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
        	foreach ($query->result() as $row) {

        		$records_data = array(
					 'association_id' => $row->association_id,
					 'target_value' => $row->target_value,
					 'actual_value' => $row->actual_value,
					 'sum_or_avg_target_value' => $row->sum_or_avg_target_value + $row->target_value,
					 'accumulated_actual_value' => $row->accumulated_actual_value + $row->actual_value,
					 'created_time' => $now_time
				);

			    $this->db->insert('records',$records_data);

			    $kpi_association_data = array(
				  'sum_or_avg_target_value' =>   $row->sum_or_avg_target_value + $row->target_value,
				  'accumulated_actual_value' =>  $row->accumulated_actual_value + $row->actual_value,
				  'actual_value' => 0
				);
				$this->db->where('association_id',$row->association_id);
    			$this->db->update('kpi_association',$kpi_association_data);
    			
			}


        } else {
             return false;
        }

	}


}