<?php

class Division_head_model extends  CI_Model{

  	/**
     * Get all Division heads details
     * @return mixed
     */
    public function get_division_heads($org_uniq_name,$user_type,$user_id){

    	//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

  		if($user_type == 'super_admin' || $user_type == 'sys_admin'){

  			$this->db->select('d.*,u.*,dh.*');
  			$this->db->from('user u, divisional_head dh,division d,organization o');
  			$this->db->where('u.user_id = dh.user_id');
  			$this->db->where('dh.division_id = d.division_id');
  			$this->db->where('d.organization_id = o.organization_id');
  			$this->db->where('o.organization_id',$organization_id);
  			$this->db->where('dh.active_status',1);
  			$query = $this->db->get();
  			return $query->result();

  		}
  		else if($user_type == 'manager'){

  			$this->db->select('d.*,u.*,dh.*');
  			$this->db->from('user u, divisional_head dh,division d,organization o');
  			$this->db->where('u.user_id = dh.user_id');
  			$this->db->where('dh.division_id = d.division_id');
  			$this->db->where('d.organization_id = o.organization_id');
  			$this->db->where('o.organization_id',$organization_id);
  			$this->db->where('dh.active_status',1);
  			$query = $this->db->get();
  			return $query->result();

  		}
  		else{

  			return false;

  		}
       
    }

    /**
     * Create Divisional Head
     * @param $data
     * @return mixed
     */
    public function create_division_head($data,$org_uniq_name){

    	 //save user data to user table 
        $user_data = array(
           'first_name' => $data['first_name'],
           'last_name' => $data['last_name'],
           'email' => $data['email'],
           'username' => $data['username'],
           'user_type' => 'division_head',
           'active_status' => 1
        );

        //get user id after insert the user data
        $inserted_user_id = $this->user_model->create_user($user_data);
    	
    	  $divisional_head_data = array(
           'user_id' => $inserted_user_id,
           'division_id' => $data['division_id'],
           'active_status' => 1
        );

    	//insert data into divisional head table
        $insert_query = $this->db->insert('divisional_head',$divisional_head_data);
        return  $insert_query;

    }

     /**
     * Get division heads details for edit
     * @param $division_head_id
     * @return mixed
     */
    public function get_division_head_info($division_head_id,$org_uniq_name){

    	//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		$this->db->select('d.*,u.*,dh.*');
		$this->db->from('user u, divisional_head dh,division d,organization o');
		$this->db->where('u.user_id = dh.user_id');
		$this->db->where('dh.division_id = d.division_id');
		$this->db->where('dh.division_head_id',$division_head_id);
		$this->db->where('d.organization_id = o.organization_id');
		$this->db->where('o.organization_id',$organization_id);
		$this->db->where('dh.active_status',1);
		$query = $this->db->get();
		return $query->row();

    }

     /**
     * Update Division Head data
     * @param $division_head_id
     * @param $data
     * @return bool
     */
    public function update_division_head_info($division_head_id,$data,$org_uniq_name){
       
       //return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);


         //update user data to user table 
        $user_data = array(
           'first_name' => $data['first_name'],
           'last_name' => $data['last_name'],
           'email' => $data['email'],
           'username' => $data['username'],
           'user_type' => 'division_head',
           'active_status' => 1
        );

		
		$user_id = $this->division_head_model->get_user_id_using_division_head_id($division_head_id);

		
		if($user_id){
        $this->db->where('user_id',$user_id);
        $this->db->update('user',$user_data);// update user table

        $divisional_head_data = array(
          'division_id' => $data['division_id'],
          'active_status' => 1
        );

        $this->db->where('user_id',$user_id);
        $this->db->update('divisional_head',$divisional_head_data);
        return  true;

		} 
		else {

			return false;
		}

    }

    /**
     * delete division head 
     * @param  [type] $division_head_id [description]
     * @param  [type] $data             [description]
     * @param  [type] $org_uniq_name    [description]
     * @return [type]                   [description]
     */
    public function delete_division_head($division_head_id,$data,$org_uniq_name){

    	 //return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

    		//check the division and organization match or not
    		$this->db->select('d.*,u.*,dh.*');
    		$this->db->from('user u, divisional_head dh,division d,organization o');
    		$this->db->where('u.user_id = dh.user_id');
    		$this->db->where('dh.division_id = d.division_id');
    		$this->db->where('dh.division_head_id',$division_head_id);
    		$this->db->where('d.organization_id = o.organization_id');
    		$this->db->where('o.organization_id',$organization_id);
    		$this->db->where('dh.active_status',1);
        $query = $this->db->get();

        if ($query->num_rows() > 0){
    			//update active status of the division head table
    			$this->db->where('division_head_id',$division_head_id);
    			$this->db->update('divisional_head',$data);

    			//update active status of the user table
    			$user_id = $this->division_head_model->get_user_id_using_division_head_id($division_head_id);
    			$this->db->where('user_id',$user_id);
    			$this->db->update('user',$data);
    			return true;
        }
        else{
        	return false;
        }
    }

	/**
	 * get_user_id_using_division_head_id description
	 * @return [type] [description]
	 */
    public function get_user_id_using_division_head_id($division_head_id){
    	$this->db->select('user_id');
		$this->db->from('divisional_head');
		$this->db->where('division_head_id',$division_head_id);
		$this->db->where('active_status',1);
		$query = $this->db->get();
		$row = $query->row(); 
		return $row->user_id;
    }

	/**
	 * get_division_id_by_user
	 * @param $user_id
	 */
	public function get_division_id_by_user($user_id){
		$this->db->where('user_id',$user_id);
		$query = $this->db->get('divisional_head');
		return $query->row()->division_id;
	}


}