<?php

class Manager_model extends  CI_Model{

 	/**
     * Get all managers details
     * @return mixed
     */
    public function get_managers($org_uniq_name){
        $this->db->select('u.*, m.* ,o.*');
        $this->db->from('user u, manager m,organization o');
        $this->db->where('m.user_id = u.user_id');
        $this->db->where('o.organization_id = m.organization_id');
        $this->db->where('o.organization_uid',$org_uniq_name);
        $this->db->where('u.active_status',1);
        $this->db->where('m.active_status',1);
        $query = $this->db->get();
        return $query->result();
    }


     /**
     * Create Manager
     * @param $data
     * @return mixed
     */
    public function create_manager($data,$org_uniq_name){



        //save user data to user table and return user id
        $user_data = array(
           'first_name' => $data['first_name'],
           'last_name' => $data['last_name'],
           'email' => $data['email'],
           'username' => $data['username'],
           'pwd_uniq_id' => $data['pwd_unique_id'],
           'user_type' => 'manager',
           'active_status' => 1
        );

        $inserted_user_id = $this->user_model->create_user($user_data);

        //return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

        //add into manager table
        $manager_data = array(
            'user_id' => $inserted_user_id,
            'organization_id' => $organization_id,
            'active_status' => 1

        );

        $insert_query = $this->db->insert('manager',$manager_data);
        return  $insert_query;

    }


     /**
     * Update manager data
     * @param $manager_id
     * @param $data
     * @return bool
     */
    public function update_manager_info($manager_id,$data,$org_uniq_name){
       
        //get user id according to manager id
        $return_user_id = $this->user_model->get_manager_user_id($manager_id);

        //update user table
        $user_data = array(
           'first_name' => $data['first_name'],
           'last_name' => $data['last_name'],
           'email' => $data['email'],
           'username' => $data['username'],
           'user_type' => 'user',
           'active_status' => 1
        );

        $this->user_model->update_manager_user($return_user_id,$user_data);

        //return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

        //update manager table
        $manager_data = array(
            'organization_id' => $organization_id,
            'active_status' => 1
        );

        $this->db->where('manager_id',$manager_id);
        $this->db->update('manager',$manager_data);
        return  true;


    }


    /**
     * Get manager details for edit
     * @param $project_id
     * @return mixed
     */
    public function get_manager_info($manager_id,$org_uniq_name){

        //get user id according to manager id
        $return_user_id = $this->user_model->get_manager_user_id($manager_id);

        $this->db->select('u.*, m.* ,o.*');
        $this->db->from('user u, manager m,organization o');
        $this->db->where('m.user_id = u.user_id');
        $this->db->where('o.organization_id = m.organization_id');
        $this->db->where('o.organization_uid',$org_uniq_name);
        $this->db->where('m.manager_id', $manager_id);
        $this->db->where('u.active_status',1);
        $this->db->where('m.active_status',1);
        $query = $this->db->get();
        return $query->row();

    }


     /**
     * Delete manager
     * @param $manager_id
     * @return bool
     */
    public function  delete_manager($manager_id,$data,$org_uniq_name){

    	//check the manager and organization match or not
 		$this->db->select('u.*, m.* ,o.*');
        $this->db->from('user u, manager m,organization o');
        $this->db->where('m.user_id = u.user_id');
        $this->db->where('o.organization_id = m.organization_id');
        $this->db->where('o.organization_uid',$org_uniq_name);
        $this->db->where('m.manager_id', $manager_id);
        $this->db->where('u.active_status',1);
        $this->db->where('m.active_status',1);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
			//update active status of the manager table
			$this->db->where('manager_id',$manager_id);
			$this->db->update('manager',$data);

			//update active status of the user table
			$return_user_id = $this->user_model->get_manager_user_id($manager_id);
			$this->db->where('user_id',$return_user_id);
			$this->db->update('user',$data);
			return true;
        }else{
        	return false;
        }
       
    }


/**
 * [set_password_manager description]
 * @param [type] $data [description]
 */
    public function set_password_manager($data){



        //check the password set done or not before one time
        $this->db->select('username');
        $this->db->from('user');
        $this->db->where('active_pwd = 1');
        $this->db->where('pwd_uniq_id',$data['pwdUnique_id']);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return  false;
        }else{
             //update user table

           // $options = ['modernie'=> 001];
           // $encripted_pass = password_hash($data['new_password'],PASSWORD_BCRYPT,$options);

           // $password = password_hash($data['new_password'], PASSWORD_BCRYPT);


            $manager_data = array(
                'password' => $data['new_password'],
                'active_pwd' => 1
            );

            $this->db->where('pwd_uniq_id',$data['pwdUnique_id']);
            $this->db->update('user',$manager_data);
            return  true;
        }

       
    }


}