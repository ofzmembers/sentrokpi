<?php

class Ajax_model extends  CI_Model{

	/**
	 * update_actual_value_kpi_to_division 
	 * @param  [type] $data          [description]
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function update_actual_value_kpi_to_division($actual_value,$association_id,$organization_uid){
		
		$data = array(
			'actual_value' =>  $actual_value
		);

		$this->db->where('association_id',$association_id);
		$this->db->update('kpi_association',$data);
		return true;

	}


	/**
	 * update_actual_value_kpi_to_employee 
	 * @param  [type] $data          [description]
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function update_actual_value_kpi_to_employee($actual_value,$association_id,$organization_uid){
		
		$data = array(
			'actual_value' =>  $actual_value,
		);

		$this->db->where('association_id',$association_id);
		$this->db->update('kpi_association',$data);
		return true;

	}



    /**
     * [update_actual_value_kpi_records - for previous data enter part
     * @param  [type] $actual_value     [description]
     * @param  [type] $record_id        [description]
     * @param  [type] $organization_uid [description]
     * @return [type]                   [description]
     */
	public function update_actual_value_kpi_records_to_division($actual_value,$record_id,$association_id,$organization_uid){
		$data = array(
			'actual_value' =>  $actual_value,
		);

		$this->db->where('record_id',$record_id);
		$this->db->where('association_id',$association_id);
		$this->db->update('records',$data);
		return true;
	}

	/**
	 * [update_actual_value_kpi_records_to_employee description]
	 * - for previous data enter part- employees
	 * @param  [type] $actual_value     [description]
	 * @param  [type] $record_id        [description]
	 * @param  [type] $association_id   [description]
	 * @param  [type] $organization_uid [description]
	 * @return [type]                   [description]
	 */
	public function update_actual_value_kpi_records_to_employee($actual_value,$record_id,$association_id,$organization_uid){
		$data = array(
			'actual_value' =>  $actual_value,
		);

		$this->db->where('record_id',$record_id);
		$this->db->where('association_id',$association_id);
		$this->db->update('records',$data);
		return true;
	}


}