<?php

class User_model extends  CI_Model{

	/**
	 * Create new user
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function create_user($data){

		


		$insert_query = $this->db->insert('user',$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	/**
	 * get  user_id from parsing manager_id
	 * @param  [type] $manager_id [description]
	 * @return [type]             [description]
	 */
	public function get_manager_user_id($manager_id){
		$this->db->select('user_id');
		$this->db->from('manager');
		$this->db->where('manager_id',$manager_id);
        $query = $this->db->get();
        return $query->row()->user_id;  
	}

	/**
	 * Update user data from manager section
	 * @param  [type] $user_id [description]
	 * @param  [type] $data    [description]
	 * @return [type]          [description]
	 */
	public function update_manager_user($user_id,$data){
		$this->db->where('user_id',$user_id);
		$this->db->update('user',$data);
		return true;
	}

	/**
	 * [get_user_info description]
	 * @param  [type] $user_id [description]
	 * @return [type]          [description]
	 */
	public function get_user_info($user_id){
		$this->db->select('first_name,last_name,username,user_id');
        $this->db->from('user');
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();
        return $query->row(); 
	}

	/**
	 * [update_user description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function update_user($user_id,$data){
		$this->db->where('user_id',$user_id);
		$this->db->update('user',$data);
		return true;
	}

	/**
	 * [update_user_password
	 * @param  [type] $user_id [description]
	 * @param  [type] $data    [description]
	 * @return [type]          [description]
	 */
	public function update_user_password($user_id,$data){

		$current_password = $data['current_password'];
		$new_password = $data['new_password'];
		$confirm_password = $data['confirm_password'];

		$this->db->select('password');
		$this->db->from('user');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
        if ($query->num_rows() > 0){

        	$row = $query->row(); 
        	$password =$row->password;
			
			if($password == $current_password){
				 $data = array(
					'password' => $this->input->post('new_password'),
            	);
				$this->db->where('user_id',$user_id);
				$this->db->update('user',$data);
				return true;
			}
			else{
				return false;
			}
			
        }
        else{
        	return false;
        }
	}


	/**
	 * [get_user_unique_id  for forget password process]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function get_user_unique_id($data){

		$this->db->select('pwd_uniq_id');
        $this->db->from('user');
        $this->db->where('email',$data['email']);
        $this->db->where('active_pwd = 1');
        $query = $this->db->get();
        return $query->row()->pwd_uniq_id;  

	}

	/**
	 * [reset_user_password description]
	 * @param  [type] $unique_id [description]
	 * @param  [type] $data      [description]
	 * @return [type]            [description]
	 */
	public function reset_user_password($unique_id,$data){
		$this->db->where('pwd_uniq_id',$unique_id);
		$this->db->update('user',$data);
		return true;
	}

	/**
	 * [update_reset_user_password description]
	 * @param  [type] $pwdUnique_id [description]
	 * @param  [type] $data         [description]
	 * @return [type]               [description]
	 */
	public function update_reset_user_password($pwdUnique_id,$data){

		$current_password = $data['current_password'];
		$new_password = $data['new_password'];
		$confirm_password = $data['confirm_password'];

		$this->db->select('password');
		$this->db->from('user');
		$this->db->where('pwd_uniq_id',$pwdUnique_id);
		$query = $this->db->get();
        if ($query->num_rows() > 0){

        	$row = $query->row(); 
        	$password =$row->password;
			
			if($password == $current_password){
				 $data = array(
					'password' => $this->input->post('new_password'),
					'active_pwd' => 1,
            	);
				$this->db->where('pwd_uniq_id',$pwdUnique_id);
				$this->db->update('user',$data);
				return true;
			}
			else{
				return false;
			}
			
        }
        else{
        	return false;
        }


	}


}