<?php

class Kpi_to_employee_model extends  CI_Model{

	/**
	 * get_kpi_to_division
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $user_type     [description]
	 * @param  [type] $user_id       [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_to_employee($org_uniq_name,$user_type,$user_id){
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

  		if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager'){

  			$this->db->select('k.kpi_id,k.kpi_name,ka.association_id,ka.target_value,k.repeat_duration,e.employee_name,ka.actual_value');
  			$this->db->from('kpi k,kpi_association ka, employee e');
  			$this->db->where('k.organization_id',$organization_id);
  			$this->db->where('ka.employee_id = e.employee_id');
  			$this->db->where('k.kpi_id = ka.kpi_id');
  			$this->db->where('ka.active_status',1);
  			$this->db->where('k.active_status',1);
  			$query = $this->db->get();
  			return $query->result();

  		}
  		else{
  			return false;
  		}
	}


	
public function get_kpi_to_employee_filter_by_employee($org_uniq_name,$user_type,$user_id,$data){
		
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		if($data['division_id'] == 'all'){
			if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager'){

	  			$this->db->select('k.kpi_id,k.kpi_name,ka.association_id,ka.target_value,k.repeat_duration,e.employee_name,ka.actual_value');
	  			$this->db->from('kpi k,kpi_association ka, employee e');
	  			$this->db->where('k.organization_id',$organization_id);
	  			$this->db->where('ka.employee_id = e.employee_id');
	  			$this->db->where('k.kpi_id = ka.kpi_id');
	  			$this->db->where('ka.active_status',1);
	  			$this->db->where('k.active_status',1);
	  			$query = $this->db->get();
	  			return $query->result();

	  		}
	  		else{
	  			return false;
	  		}
		}
		else{
			if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager'){

	  			$this->db->select('k.kpi_id,k.kpi_name,ka.association_id,ka.target_value,k.repeat_duration,e.employee_name,ka.actual_value');
	  			$this->db->from('kpi k,kpi_association ka, employee e');
	  			$this->db->where('k.organization_id',$organization_id);
	  			$this->db->where('ka.employee_id = e.employee_id');
	  			$this->db->where('ka.division_id',$data['division_id']);
	  			$this->db->where('k.kpi_id = ka.kpi_id');
	  			$this->db->where('ka.active_status',1);
	  			$this->db->where('k.active_status',1);
	  			$query = $this->db->get();
	  			return $query->result();

	  		}
	  		else{
	  			return false;
  			}
		}
	}

	public function get_kpi_to_employee_filter_by_division_employee($org_uniq_name,$user_type,$user_id,$data){
		
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		if($data['employee_id'] == 'all'){
			if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager'){

	  			$this->db->select('k.kpi_id,k.kpi_name,ka.association_id,ka.target_value,k.repeat_duration,e.employee_name,ka.actual_value');
	  			$this->db->from('kpi k,kpi_association ka, employee e');
	  			$this->db->where('k.organization_id',$organization_id);
	  			$this->db->where('ka.employee_id = e.employee_id');
	  			$this->db->where('k.kpi_id = ka.kpi_id');
	  			$this->db->where('ka.active_status',1);
	  			$this->db->where('k.active_status',1);
	  			$query = $this->db->get();
	  			return $query->result();

	  		}
	  		else{
	  			return false;
	  		}
		}
		else{
			if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager'){

	  			$this->db->select('k.kpi_id,k.kpi_name,ka.association_id,ka.target_value,k.repeat_duration,e.employee_name,ka.actual_value');
	  			$this->db->from('kpi k,kpi_association ka, employee e');
	  			$this->db->where('k.organization_id',$organization_id);
	  			$this->db->where('ka.employee_id = e.employee_id');
	  			$this->db->where('ka.employee_id',$data['employee_id']);
	  			$this->db->where('k.kpi_id = ka.kpi_id');
	  			$this->db->where('ka.active_status',1);
	  			$this->db->where('k.active_status',1);
	  			$query = $this->db->get();
	  			return $query->result();

	  		}
	  		else{
	  			return false;
  			}
		}
	}


	/**
	 * get_kpi_list_for_employee from ajax
	 * @param  [type] $user_id       [description]
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_list_for_employee($user_id,$org_uniq_name){

		$query = $this->db->query("SELECT `kpi_id`,`kpi_name` FROM   `kpi` WHERE  `kpi_id` in ( SELECT `kpi_id` FROM   `kpi_association` WHERE  `division_id` in (SELECT `division_id` FROM `employee` WHERE `employee_id`= ".$user_id.") and `kpi_id` not in (SELECT  `kpi_id`  FROM  `kpi_association`  WHERE  `employee_id` = ".$user_id." and `active_status` = 1))

		");

		

  			return $query->result();
	}


	/**
	 * assign_kpi_to_employee 
	 * @param  [type] $data          [description]
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function assign_kpi_to_employee($data,$org_uniq_name){


		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		//get division id from employee table
		$this->db->select('division_id');
		$this->db->from('employee');
		$this->db->where('employee_id',$data['employee_id']);
		$query = $this->db->get();
        if ($query->num_rows() > 0){

        	$row = $query->row(); 
        	$division_id =$row->division_id;
			$data = array(
				'employee_id' =>   $data['employee_id'],
				'kpi_id' => $data['kpi_id'],
				'target_value' => $data['target_value'],
				'division_id' => $division_id,
				'organization_id' => $organization_id
			);

			$insert_query = $this->db->insert('kpi_association',$data);
			return  $insert_query;
			
        }
        else{
        	return false;
        }
		
		
	}

	/**
	 * get_kpi_to_employee_info 
	 * @param  [type] $org_uniq_name [description]
	 * @param  [type] $user_type     [description]
	 * @param  [type] $user_id       [description]
	 * @return [type]                [description]
	 */
	public function get_kpi_to_employee_info($kpi_association_id,$org_uniq_name,$user_type,$user_id){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		$this->db->select('k.kpi_id,k.kpi_name,ka.association_id,ka.target_value,k.repeat_duration,e.employee_name,e.employee_id');
		$this->db->from('kpi k,kpi_association ka, employee e');
		$this->db->where('k.organization_id',$organization_id);
		$this->db->where('ka.employee_id = e.employee_id');
		$this->db->where('ka.division_id = e.division_id');
		$this->db->where('ka.association_id',$kpi_association_id);
		$this->db->where('k.kpi_id = ka.kpi_id');
		$this->db->where('ka.active_status',1);
		$this->db->where('k.active_status',1);
		$query = $this->db->get();
		return $query->row();

	}


	/**
	 *  get_single_kpi_to_employee from records table
	 *  get single kpi records from RECORDS table for update previous data
	 */
	public function get_single_kpi_to_employee($org_uniq_name,$user_type,$user_id,$association_id){

		$organization_id = $this->organization_model->get_organization_id($org_uniq_name);

  		if($user_type == 'super_admin' || $user_type == 'sys_admin' || $user_type == 'manager'){

  			$this->db->select('k.kpi_id,k.kpi_name,ka.association_id,r.target_value,k.repeat_duration,e.employee_name,r.actual_value,r.created_time,r.record_id');
  			$this->db->from('kpi k,kpi_association ka, employee e,records r');
  			$this->db->where('k.organization_id',$organization_id);
  			$this->db->where('ka.employee_id = e.employee_id');
  			$this->db->where('k.kpi_id = ka.kpi_id');
  			$this->db->where('r.association_id = ka.association_id');
  			$this->db->where('ka.association_id',$association_id);
  			$this->db->where('ka.active_status',1);
  			$this->db->where('k.active_status',1);
  			$query = $this->db->get();
  			return $query->result();

  		}
  		else{

  			return false;

  		}
	}

	/**
	 * update_kpi_to_employee_info 
	 * @param  [type] $kpi_association_id [description]
	 * @param  [type] $data               [description]
	 * @param  [type] $org_uniq_name      [description]
	 * @return [type]                     [description]
	 */
	public function update_kpi_to_employee_info($kpi_association_id,$data,$org_uniq_name){
		$data = array(
			'target_value' => $data['target_value']
		);

		$this->db->where('association_id',$kpi_association_id);
		$this->db->update('kpi_association',$data);
		return  true;
	}

	/**
	 * delete_kpi_to_employee
	 * @param  [type] $kpi_association_id [description]
	 * @param  [type] $data               [description]
	 * @param  [type] $org_uniq_name      [description]
	 * @return [type]                     [description]
	 */
	public function delete_kpi_to_employee($kpi_association_id,$data,$org_uniq_name){
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

		$this->db->select('k.kpi_id,k.kpi_name,ka.association_id,ka.target_value,k.repeat_duration,e.employee_name');
		$this->db->from('kpi k,kpi_association ka, employee e');
		$this->db->where('k.organization_id',$organization_id);
		$this->db->where('ka.employee_id = e.employee_id');
		$this->db->where('ka.association_id',$kpi_association_id);
		$this->db->where('k.kpi_id = ka.kpi_id');
		$this->db->where('ka.active_status',1);
		$this->db->where('k.active_status',1);
		$query = $this->db->get();
        if ($query->num_rows() > 0){
			$this->db->where('association_id',$kpi_association_id);
			$this->db->update('kpi_association',$data);
			return true;
        }
        else{
        	return false;
        }
	}


	/**
	 * [get_available_employee_kpi_target_balance_to_edit description]
	 * @param  [type] $kpi_association_id [description]
	 * @param  [type] $org_uniq_name      [description]
	 * @param  [type] $user_type          [description]
	 * @param  [type] $user_id            [description]
	 * @return [type]                     [description]
	 */
	public function get_available_employee_kpi_target_balance_to_edit($kpi_association_id,$org_uniq_name,$user_type,$user_id){

		//edit section Assign KPI to employee 
		//
		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

        //get divisioon id
        $division_id = $this->division_model->get_division_id($kpi_association_id);
        
        //get kpi Id
        $kpi_id = $this->kpi_model->get_kpi_id($kpi_association_id);

        $query = $this->db->query("SELECT COALESCE(SUM(`target_value`),0) -(SELECT COALESCE(SUM(`target_value`),0) as `available_balance` from kpi_association where `organization_id` =  ".$organization_id." and `kpi_id` = ".$kpi_id." and `division_id` = ".$division_id." and `employee_id` != 0) as `final` from kpi_association where `organization_id` =  ".$organization_id." and `kpi_id` = ".$kpi_id." and `division_id` = ".$division_id." and `employee_id` = 0 ");

        $res =  $query->result();
        $row = $res[0];
        return $row;

        

	}


}
