<?php

class Employee_model extends  CI_Model{
	
	/**
	 * get_employees description]
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
    public function get_employees($org_uniq_name){
        $this->db->select('e.*, d.* ,o.*');
        $this->db->from('employee e, division d,organization o');
        $this->db->where('e.division_id = d.division_id');
        $this->db->where('o.organization_id = d.organization_id');
        $this->db->where('o.organization_uid',$org_uniq_name);
        $this->db->where('e.active_status',1);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * [get_employees_filter_by_employee description]
     * @param  [type] $org_uniq_name [description]
     * @param  [type] $user_type     [description]
     * @param  [type] $user_id       [description]
     * @param  [type] $data          [description]
     * @return [type]                [description]
     */
    public function get_employees_filter_by_employee($org_uniq_name,$user_type,$user_id,$data){
        if($data['division_id'] == 'all' || $data['division_id'] == ''){
            $this->db->select('e.*, d.* ,o.*');
            $this->db->from('employee e, division d,organization o');
            $this->db->where('e.division_id = d.division_id');
            $this->db->where('o.organization_id = d.organization_id');
            $this->db->where('o.organization_uid',$org_uniq_name);
            $this->db->where('e.active_status',1);
            $query = $this->db->get();
            return $query->result(); 
        }
        else if($data['division_id']){
            $this->db->select('e.*, d.* ,o.*');
            $this->db->from('employee e, division d,organization o');
            $this->db->where('e.division_id = d.division_id');
            $this->db->where('o.organization_id = d.organization_id');
            $this->db->where('o.organization_uid',$org_uniq_name);
            $this->db->where('e.division_id',$data['division_id']);
            $this->db->where('e.active_status',1);
            $query = $this->db->get();
            return $query->result();
        }else{
            $this->db->select('e.*, d.* ,o.*');
            $this->db->from('employee e, division d,organization o');
            $this->db->where('e.division_id = d.division_id');
            $this->db->where('o.organization_id = d.organization_id');
            $this->db->where('o.organization_uid',$org_uniq_name);
            $this->db->where('e.active_status',1);
            $query = $this->db->get();
            return $query->result();
        }
    }

    /**
     * create_employee 
     * @param  [type] $data          [description]
     * @param  [type] $org_uniq_name [description]
     * @return [type]                [description]
     */
    public function create_employee($data,$org_uniq_name){
		//add into employee table
        $employee_data = array(
            'employee_name' => $data['employee_name'],
            'division_id' => $data['division_id'],
            'active_status' => 1
        );

        $insert_query = $this->db->insert('employee',$employee_data);
        return  $insert_query;
    }

    /**
     * get_employee_info 
     * @param  [type] $employee_id   [description]
     * @param  [type] $org_uniq_name [description]
     * @return [type]                [description]
     */
    public function get_employee_info($employee_id,$org_uniq_name){
		$this->db->select('e.*, d.* ,o.*');
        $this->db->from('employee e, division d,organization o');
        $this->db->where('e.division_id = d.division_id');
        $this->db->where('o.organization_id = d.organization_id');
        $this->db->where('o.organization_uid',$org_uniq_name);
        $this->db->where('e.employee_id',$employee_id);
        $this->db->where('e.active_status',1);
        $query = $this->db->get();
        return $query->row();
    }

    /**
     * update_employee_info
     * @param  [type] $employee_id   [description]
     * @param  [type] $data          [description]
     * @param  [type] $org_uniq_name [description]
     * @return [type]                [description]
     */
    public function update_employee_info($employee_id,$data,$org_uniq_name){

 		$employee_data = array(
			'employee_name' => $data['employee_name'],
			'division_id' => $data['division_id'],
			'active_status' => 1
        );

        $this->db->where('employee_id',$employee_id);
        $this->db->update('employee',$employee_data);
        return  true;
    }

    /**
     * delete_employee 
     * @param  [type] $employee_id   [description]
     * @param  [type] $data          [description]
     * @param  [type] $org_uniq_name [description]
     * @return [type]                [description]
     */
    public function delete_employee($employee_id,$data,$org_uniq_name){

    	//check this employee available
    	$this->db->select('e.*, d.* ,o.*');
        $this->db->from('employee e, division d,organization o');
        $this->db->where('e.division_id = d.division_id');
        $this->db->where('o.organization_id = d.organization_id');
        $this->db->where('o.organization_uid',$org_uniq_name);
        $this->db->where('e.employee_id',$employee_id);
        $this->db->where('e.active_status',1);
        $query = $this->db->get();
   
        if ($query->num_rows() > 0){
        	$employee_data = array(
				'active_status' => 0
	        );

	        $this->db->where('employee_id',$employee_id);
	        $this->db->update('employee',$employee_data);
	        return  true;
        }
        else{
        	return false;
        }
    }


    /**
     * get_employee_list_for_division ajax
     * @param  [type] $div_id  [description]
     * @param  [type] $org_uid [description]
     * @return [type]          [description]
     */
    public function get_employee_list_for_division($div_id,$org_uid){
        if($div_id == 'all' || $div_id == '' ){
            $this->db->select('e.*');
            $this->db->from('employee e, division d,organization o');
            $this->db->where('e.division_id = d.division_id');
            $this->db->where('o.organization_id = d.organization_id');
            $this->db->where('o.organization_uid',$org_uid);
            $this->db->where('e.active_status',1);
            $query = $this->db->get();
            return $query->result(); 
        }
        else{
            $this->db->select('e.*');
            $this->db->from('employee e, division d,organization o');
            $this->db->where('e.division_id = d.division_id');
            $this->db->where('o.organization_id = d.organization_id');
            $this->db->where('o.organization_uid',$org_uid);
            $this->db->where('d.division_id',$div_id);
            $this->db->where('e.active_status',1);
            $query = $this->db->get();
            return $query->result();  
        }
       
    }



    public function get_employee_name($employee_id){
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where('employee_id',$employee_id);
        $query = $this->db->get();
        $row = $query->row(); 
        return $row->employee_name;
    }

    /**
     * [get_division_id description]
     * @param  [type] $employee_id [description]
     * @return [type]              [description]
     */
    public function get_division_id($employee_id){
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where('employee_id',$employee_id);
        $query = $this->db->get();
        $row = $query->row(); 
        return $row->division_id;
    }

}