<?php

class Division_model extends  CI_Model{


    
     /**
     * Get all organization details
     * @return mixed
     */
    public function get_divisions($org_uniq_name,$user_type,$user_id){
        //return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);


		if($user_type == 'super_admin' || $user_type == 'sys_admin'){
			$this->db->distinct();
			$this->db->select('d.*');
			$this->db->from('user u, manager m,organization o,division d');
			$this->db->where('m.user_id = u.user_id');
			$this->db->where('o.organization_id = m.organization_id');
			$this->db->where('d.organization_id = m.organization_id');
			$this->db->where('o.organization_uid',$org_uniq_name);
			$this->db->where('u.active_status',1);
			$this->db->where('m.active_status',1);
			$this->db->where('d.active_status',1);
			$query = $this->db->get();
			return $query->result();
		}
		else if($user_type == 'manager'){
			$this->db->distinct();
			$this->db->select('d.*');
			$this->db->from('user u, manager m,organization o,division d');
			$this->db->where('m.user_id = u.user_id');
			$this->db->where('o.organization_id = m.organization_id');
			$this->db->where('d.organization_id = m.organization_id');
			$this->db->where('u.user_id',$user_id);
			$this->db->where('o.organization_uid',$org_uniq_name);
			$this->db->where('u.active_status',1);
			$this->db->where('m.active_status',1);
			$this->db->where('d.active_status',1);
			$query = $this->db->get();
			return $query->result();
		}
        else if($user_type == 'division_head'){
            $this->db->select('d.*');
            $this->db->from('divisional_head dh,division d, organization o');
            $this->db->where('dh.division_id = d.division_id');
            $this->db->where('d.organization_id = o.organization_id');
            $this->db->where('o.organization_id',$organization_id);
            $this->db->where('dh.user_id',$user_id);
            $this->db->where('d.active_status',1);
            $this->db->where('dh.active_status',1);
            $this->db->where('o.active_status',1);
            $query = $this->db->get();
            return $query->result();

        }else{
			return false;
		}
       
    }


    /**
     * Create Division
     * @param $data
     * @return mixed
     */
    public function create_division($data,$org_uniq_name){

    	//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

        //save division data
        $division_data = array(
           'division_name' => $data['division_name'],
           'organization_id' =>  $organization_id,
           'active_status' => 1
        );

        $insert_query = $this->db->insert('division',$division_data);
        return  $insert_query;

    }


      /**
     * Get division details for edit
     * @param $division_id
     * @return mixed
     */
    public function get_division_info($division_id,$org_uniq_name){
		$this->db->distinct();
		$this->db->select('d.*');
		$this->db->from('user u, manager m,organization o,division d');
		$this->db->where('m.user_id = u.user_id');
		$this->db->where('o.organization_id = m.organization_id');
		$this->db->where('o.organization_uid',$org_uniq_name);
		$this->db->where('d.division_id',$division_id);
		$this->db->where('u.active_status',1);
		$this->db->where('m.active_status',1);
		$this->db->where('d.active_status',1);
		$query = $this->db->get();
		return $query->row();

    }

     /**
     * Update Division data
     * @param $division_id
     * @param $data
     * @return bool
     */
    public function update_division_info($division_id,$data,$org_uniq_name){
       
       //return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);

        //save division data
        $division_data = array(
           'division_name' => $data['division_name'],
           'organization_id' =>  $organization_id,
           'active_status' => 1
        );

        $this->db->where('division_id',$division_id);
        $this->db->update('division',$division_data);
        return  true;
    }

     /**
     * Delete Division
     * @param $division_id
     * @return bool
     */
    public function  delete_division($division_id,$data,$org_uniq_name){

    	//check the division and organization match or not
 		$this->db->distinct();
		$this->db->select('d.*');
		$this->db->from('user u, manager m,organization o,division d');
		$this->db->where('m.user_id = u.user_id');
		$this->db->where('o.organization_id = m.organization_id');
		$this->db->where('o.organization_uid',$org_uniq_name);
		$this->db->where('d.division_id',$division_id);
		$this->db->where('u.active_status',1);
		$this->db->where('m.active_status',1);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
			//update active status of the division table
			$this->db->where('division_id',$division_id);
			$this->db->update('division',$data);
			return true;
        }else{
        	return false;
        }
       
    }

    /**
     * get_division_name 
     * @param  [type] $division_id [description]
     * @return [type]              [description]
     */
    public function get_division_name($division_id){
        $this->db->select('*');
        $this->db->from('division');
        $this->db->where('division_id',$division_id);
        $query = $this->db->get();
        $row = $query->row(); 
        return $row->division_name;

    }


    /**
     * [get_division_id description]
     * @param  [type] $employee_id [description]
     * @return [type]              [description]
     */
    public function get_division_id($kpi_association_id){
        $this->db->select('*');
        $this->db->from('kpi_association');
        $this->db->where('association_id',$kpi_association_id);
        $query = $this->db->get();
        $row = $query->row(); 
        return $row->division_id;
    }

    /**
     * get_org_id_by_division_id
     * @param $division_id
     * @return mixed
     */
    public function get_org_id_by_division_id($division_id){
        $this->db->where('division_id',$division_id);
        $query = $this->db->get('division');
        return $query->row()->organization_id;
    }
   

   


}