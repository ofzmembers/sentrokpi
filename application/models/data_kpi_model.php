<?php

class Data_kpi_model extends  CI_Model{

	/**
	 * get_organization_kpi_list
	 * @param  [type] $org_uniq_name [description]
	 * @return [type]                [description]
	 */
	public function get_organization_kpi_list($org_uniq_name){

		//return the organization id
        $organization_id = $this->organization_model->get_organization_id($org_uniq_name);
        
	}

	public function getRecords($limit=null,$offset=NULL){



	  $this->db->select('record_id,target_value,actual_value,sum_or_avg_target_value,accumulated_actual_value,created_time');
	  $this->db->from('records');
	  $this->db->limit($limit, $offset);
	  $query = $this->db->get();
	  return $query->result();

		
		
	 }

	 public function totalRecords(){
	  return $this->db->count_all('records');
	 }



	 public function getRecordsParameted($limit=null,$offset=NULL,$dataSubmit){


		$kpi_id = $dataSubmit['kpi_id'];
		$organization_id = $dataSubmit['organization_id'];
		
		$this->db->select('r.*,,r.`created_time` as createTime,k.*');
		$this->db->from('kpi_association ka, records r,kpi k');
		$this->db->where('ka.kpi_id',$kpi_id);
		$this->db->where('k.organization_id',$organization_id);
		$this->db->where('ka.association_id = r.association_id');
		$this->db->where('ka.kpi_id = k.kpi_id');
		$this->db->limit($limit, $offset);
		$query = $this->db->get();
		return $query->result();


	  // $this->db->select('record_id,target_value,actual_value,sum_or_avg_target_value,accumulated_actual_value,created_time');
	  // $this->db->from('records');
	  // $this->db->limit($limit, $offset);
	  // $query = $this->db->get();
	  // return $query->result();

		
		
	 }

	 public function totalRecordsParameted($dataSubmit){

	 	$kpi_id = $dataSubmit['kpi_id'];
		$organization_id = $dataSubmit['organization_id'];

		$this->db->select('r.*');
		$this->db->from('kpi_association ka, records r,kpi k');
		$this->db->where('ka.kpi_id',$kpi_id);
		$this->db->where('k.organization_id',$organization_id);
		$this->db->where('ka.association_id = r.association_id');
		$this->db->where('ka.kpi_id = k.kpi_id');
		$query = $this->db->get();
		return $query->num_rows();


	  // return $this->db->count_all('records');
	 }


	  public function getRecordsParametedFilterWithDate($limit=null,$offset=NULL,$dataSubmit){


		$kpi_id = $dataSubmit['kpi_id'];
		$organization_id = $dataSubmit['organization_id'];
		$datepicker1 = $dataSubmit['datepicker1'];
		$datepicker2 = $dataSubmit['datepicker2'];
		
	//return print $datepicker1;

		$query  = $this->db->query(" 
			SELECT r.*,r.`created_time` as createTime,k.* FROM kpi_association ka, records r,kpi k 
			WHERE ka.kpi_id = ".$kpi_id." AND
			k.organization_id = ".$organization_id." AND 
			ka.association_id = r.association_id AND 
			ka.kpi_id = k.kpi_id AND
			r.`created_time` >=  '".$datepicker1."'  AND 
			r.`created_time` <= date_add( '".$datepicker2."', INTERVAL 1 DAY)
			LIMIT ".$limit." OFFSET ".$offset."
		");

		//return print $datepicker1;

		//return print_r($query->result());

		return $query->result();
		//print_r($query);
		//$this->output->enable_profiler(TRUE);
	
	 }


	 /**
	  * [totalRecordsParameted description]
	  * @param  [type] $dataSubmit [description]
	  * @return [type]             [description]
	  */
	 public function totalRecordsParametedFilterWithDate($dataSubmit){

	 	$kpi_id = $dataSubmit['kpi_id'];
		$organization_id = $dataSubmit['organization_id'];
		$datepicker1 = $dataSubmit['datepicker1'];
		$datepicker2 = $dataSubmit['datepicker2'];

		
		$query  = $this->db->query(" 
			SELECT r.*,r.`created_time` as createTime FROM kpi_association ka, records r,kpi k 
			WHERE ka.kpi_id = ".$kpi_id." AND
			k.organization_id = ".$organization_id." AND 
			ka.association_id = r.association_id AND 
			ka.kpi_id = k.kpi_id AND
			r.`created_time` >=  '".$datepicker1."'  AND 
			r.`created_time` <= date_add( '".$datepicker2."', INTERVAL 1 DAY)
		");


		return $query->num_rows();


	  
	 }

}