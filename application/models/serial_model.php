<?php

class Serial_model extends  CI_Model{


	public function save_serial_key($data){
		//insert data into serial head table
		$insert_query = $this->db->insert('serial',$data);
		return  $insert_query;

	}


	public function get_all_serial_key_data(){
		$this->db->select('*');
		$this->db->from('serial');
		$query = $this->db->get();
		return $query->result();
	}


	/**
	 * checkActiveStatus
	 * @param $user_id
	 * @param $user_type
	 * @return bool
	 */
	public function checkActiveStatus($user_id,$user_type){


		if($user_type == 'manager'){
			//get organization id by using manager user id
			$org_id = $this->organization_model->get_organization_id_by_user($user_id);
			if($org_id){
				//get serial number status
				$serial_number_status = $this->serial_model->get_serial_number_status($org_id);
				if($serial_number_status == '1'){
					return true;
				}
				else{
					return false;
				}
			}else{
				return false;
			}
		}else if($user_type == 'division_head'){
			//get division id by using divisional head user id
			$division_id = $this->division_head_model->get_division_id_by_user($user_id);
			if($division_id){
				// get org id using division id
				$org_id = $this->division_model->get_org_id_by_division_id($division_id);
				if($org_id){
					$serial_number_status = $this->serial_model->get_serial_number_status($org_id);
					if($serial_number_status == '1'){
						return true;
					}
					else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}

		}else if($user_type == 'sys_admin'){
			return true;
		}else{
			return false;
		}
	}

	public function get_serial_number_status($org_id){
		$this->db->where('org_id',$org_id);
		$query = $this->db->get('serial');
		return $query->row()->active_status;
	}

	/**
	 * @param $key
	 */
	public function check_serial_number($key){

		$this->db->where('serial_key',$key);
		$query = $this->db->get('serial');
		//return $query->row()->active_status;
		if($query->row()){
			// key is correct
			return 1;
		}else{
			return 0;
		}

	}


	public function activate_serial_number($key){
		$data = array(
			'active_status' =>  1
		);
		$this->db->where('serial_key',$key);
		$this->db->update('serial',$data);
		return true;

	}

}