<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends CI_Controller {
    // repeat_duration
    // 1 = daily
    // 2 = Weekly
    // 3 = Monthly
    // 4 = Quaterly
    // 5 = Annually
    // 
    //  curl --silent http://dev.modernie.com/apps/smartkpi.lk/cronjob/daily/
    //  curl --silent http://dev.modernie.com/apps/smartkpi.lk/cronjob/weekly/
    //  curl --silent http://dev.modernie.com/apps/smartkpi.lk/cronjob/monthly/
    //  curl --silent http://dev.modernie.com/apps/smartkpi.lk/cronjob/quaterly/
    //  curl --silent http://dev.modernie.com/apps/smartkpi.lk/cronjob/annually/

	 public function __construct(){
        parent::__construct();
        $this->load->model('organization_model');
        $this->load->model('manager_model');
        $this->load->model('user_model');
        $this->load->model('division_model');
        $this->load->model('division_head_model');
        $this->load->model('employee_model');
        $this->load->model('kpi_model');
        $this->load->model('kpi_to_division_model');
        $this->load->model('kpi_to_employee_model');
        $this->load->model('ajax_model');
        $this->load->model('cronjob_model');
    }

    public function index(){

    }

    // min function for test //
    public function min(){
        $data['records'] = $this->cronjob_model->update_tables(1);
    }

    public function daily(){
         $data['records'] = $this->cronjob_model->update_tables(1);
    }

    public function weekly(){
         $data['records'] = $this->cronjob_model->update_tables(2);
    }

    public function monthly(){
         $data['records'] = $this->cronjob_model->update_tables(3);
    }
    
    public function quaterly(){
         $data['records'] = $this->cronjob_model->update_tables(4);
    } 

    public function annually(){
         $data['records'] = $this->cronjob_model->update_tables(5);
    }

}
