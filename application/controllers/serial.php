<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Serial extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('organization_model');
        $this->load->model('manager_model');
        $this->load->model('user_model');
        $this->load->model('division_model');
        $this->load->model('division_head_model');
        $this->load->model('employee_model');
        $this->load->model('kpi_model');
        $this->load->model('kpi_to_division_model');
        $this->load->model('kpi_to_employee_model');
        $this->load->model('ajax_model');
        $this->load->model('data_kpi_model');
        $this->load->model('serial_model');
        if(!$this->session->userdata('logged_in')){
            $this->session->set_flashdata('no_access','Sorry you are not allowed');
            redirect('login');
        }

       
    }

    public function index()
	{
        $user_type = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');
        //get user info
        $data['user_info'] = $this->user_model->get_user_info($user_id);
        $data['serials'] = $this->serial_model->get_all_serial_key_data();

        $data['header_view'] = "templates/header";
        $data['main_view'] = "pages/serial";
        $this->load->view('templates/template_main',$data);

    }


    public function generate(){
        $user_type = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');

        //get user info
        $data['user_info'] = $this->user_model->get_user_info($user_id);
        $data['organizations'] = $this->organization_model->get_organizations($user_id, $user_type);

        $this->form_validation->set_rules('organization','Organization','trim|required');

        if($this->form_validation->run() == FALSE){
            $data['header_view'] = "templates/header";
            $data['main_view'] = "pages/serial_generate";
            $this->load->view('templates/template_main',$data);
        }
        else{
            $passphrase = 'cant u see.where is the key $@';
            $chars = array(0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
            $serial = '';
            $max = count($chars)-1;
            for($i=0;$i<60;$i++){
                $serial .= (!($i % 5) && $i ? '-' : '').$chars[rand(0, $max)];
            }

            $date = date_create();
            $dateTime = date_timestamp_get($date);

            $iv = substr(md5('iv'.$passphrase, true), 0, 20);
            $key = substr(md5('pass1'.$passphrase, true) .
                md5('pass2'.$passphrase, true), 0, 24);
            $opts = array('iv'=>$iv, 'key'=>$key);
            $fp = fopen('serial-key-'.$dateTime.'.enckey', 'wb');
            stream_filter_append($fp, 'mcrypt.tripledes', STREAM_FILTER_WRITE, $opts);
            fwrite($fp, $serial);
            fclose($fp);

            // get org name
            $organization_name = $this->organization_model->get_organization_name($this->input->post('organization'));

            $data = array(
                'org_id' => $this->input->post('organization'),
                'org_name' => $organization_name,
                'serial_key' => $serial,
                'serial_key_name' => 'serial-key-'.$dateTime.'.enckey',
                'active_status' => 0
            );
            if($this->serial_model->save_serial_key($data)){
                $this->session->set_flashdata('serial_generated','Serial has been generated');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename('serial-key-'.$dateTime.'.enckey'));
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize('serial-key-'.$dateTime.'.enckey'));
                readfile('serial-key-'.$dateTime.'.enckey');
                //exit;
               // redirect("serial");
            }
        }
    }


}