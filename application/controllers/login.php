<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('organization_model');
        $this->load->model('manager_model');
        $this->load->model('user_model');
        $this->load->model('division_model');
        $this->load->model('division_head_model');
        $this->load->model('employee_model');
        $this->load->model('kpi_model');
        $this->load->model('kpi_to_division_model');
        $this->load->model('kpi_to_employee_model');
        $this->load->model('ajax_model');
        $this->load->model('serial_model');
        $this->load->model('data_kpi_model');

    }

    public function index()
	{
        $user_db_data = array();
        if(isset($_COOKIE['username']) && isset($_COOKIE['password'])){
            if($this->auth_model->login_user($_COOKIE['username'],$_COOKIE['password'])){
                $user_db_data['logged_in'] = true;
                $this->session->set_userdata($user_db_data);

//                if($rememberMe == 'RememberMe'){
//                    setcookie("username", $username, time()+3600);
//                    setcookie("password", $password, time()+3600);
//                }
                 $this->session->set_flashdata('login_success','You are now logged in');
                 redirect('dashboard');
            }
        }
        else if($this->session->userdata('logged_in')){
           redirect('dashboard');
        }
        else{
            $this->load->view('pages/login');
        }
         //$this->load->view('pages/login');
        
	}

    public function auth(){

        

        $this->load->helper('cookie');


        $this->form_validation->set_rules('username','Username','trim|required|min_length[3]');
        $this->form_validation->set_rules('password','Password','trim|required|min_length[3]');


        if($this->form_validation->run() == FALSE){
            $data = array(
                'errors' => validation_errors()
            );

            $this->session->set_flashdata($data);
            redirect('login');
        }else{

            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $rememberMe = $this->input->post('remember');

            $user_db_data = array();
            $user_db_data = $this->auth_model->login_user($username,$password);

           // print_r($user_db_data);

            $user_id =  $user_db_data['user_id'];
            $user_type = $user_db_data['user_type'];



             if($user_db_data){

                 //  check application key
                $applicationActiveStatus =  $this->serial_model->checkActiveStatus($user_id,$user_type);
                if($applicationActiveStatus){
                   // "success";
                    $user_db_data['logged_in'] = true;
                    $this->session->set_userdata($user_db_data);

                    if($rememberMe == 'RememberMe'){
                        setcookie("username", $username, time()+3600);
                        setcookie("password", $password, time()+3600);
                    }
                    $this->session->set_flashdata('login_success','You are now logged in');
                    redirect('dashboard');
                }else{
                  // "fail";
                   // print "fail";
                    $user_db_data['logged_in'] = true;
                    $this->session->set_userdata($user_db_data);

                    if($rememberMe == 'RememberMe'){
                        setcookie("username", $username, time()+3600);
                        setcookie("password", $password, time()+3600);
                    }
                    $this->session->set_flashdata('login_success','You are now logged in');
                    redirect('licen');

                }

             }else{
                 $this->session->set_flashdata('login_failed','Sorry You are not logged in');
                 redirect('login');
             }
        }

    }

    /*
     *  Logout function
     */
    public  function logout(){
        $this->session->sess_destroy();
        setcookie("username","", time()+3600);
        setcookie("password","", time()+3600);
        redirect('login');

    }


}

