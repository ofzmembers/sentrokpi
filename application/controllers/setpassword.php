<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setpassword extends CI_Controller {

	//echo $this->uri->segment('2');

	 public function __construct(){
        parent::__construct();
        $this->load->model('organization_model');
        $this->load->model('manager_model');
        $this->load->model('user_model');
        $this->load->model('division_model');
        $this->load->model('division_head_model');
        $this->load->model('employee_model');
        $this->load->model('kpi_model');
        $this->load->model('kpi_to_division_model');
        $this->load->model('kpi_to_employee_model');
        $this->load->model('ajax_model');
        $this->load->model('data_kpi_model');
     
    }

    public function index()
	{
       redirect("login");
    }

    public function token($pwdUnique_id = NULL){
    	//echo $pwdUnique_id;
    

        $this->form_validation->set_rules('new_password','New Password','trim|required|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required');

        if($this->form_validation->run() == FALSE){
             $this->load->view('pages/set_password_manager');
        }
        else{
            $data = array(
				'new_password' => $this->input->post('new_password'),
				'confirm_password' => $this->input->post('confirm_password'),
				'pwdUnique_id' => $pwdUnique_id
            );
            if($this->manager_model->set_password_manager($data)){
                $this->session->set_flashdata('manager_password_updated','Manager password has been updated');
                redirect("login");
            }
            else{
 				$this->session->set_flashdata('manager_password_updated_fail','Cant set password more than one time');
                redirect("setpassword/token/".$pwdUnique_id);
            }
        }


    }

}