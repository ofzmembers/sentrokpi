<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    
    public function __construct(){
        parent::__construct();
        $this->load->model('ajax_model');
        $this->load->model('auth_model');
        $this->load->model('cronjob_model');
        $this->load->model('data_kpi_model');
        $this->load->model('division_head_model');
        $this->load->model('division_model');
        $this->load->model('employee_model');
        $this->load->model('kpi_model');
        $this->load->model('kpi_to_division_model');
        $this->load->model('kpi_to_employee_model');
        $this->load->model('manager_model');
        $this->load->model('organization_model');
        $this->load->model('reports_model');
        $this->load->model('serial_model');
        $this->load->model('user_model');

        if(!$this->session->userdata('logged_in')){
            $this->session->set_flashdata('no_access','Sorry you are not allowed');
            redirect('login');
        }

       
    }

    /**
     * createUniqueId
     * @return [type] [description]
     */
    public function createUniqueId(){
        $this->load->helper('string');
        return random_string('sha1',20);
        return uniqid();
    }

    /**
     * emailSend
     * @return [type] [description]
     */
    public function emailSend($uniqId){


        $from_email = "sahan@modernie.com"; 
        $to_email = $this->input->post('email'); 
   
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, 'Sentrokpi'); 
         $this->email->to($to_email);
         $this->email->subject('Sentrokpi Passowrd'); 
         //$this->email->message("Please click this link and set your password :- http://107.170.33.220/hostgrid/sentrokpi.com/setpassword/token/".$uniqId); 
         //$this->email->message("Please click this link and set your password :- http://107.170.33.220/test/sentrokpi.com/setpassword/token/".$uniqId);
         $this->email->message("Please click this link and set your password :- http://74.124.213.185/test/sentrokpi.com/setpassword/token/".$uniqId);

         //Send mail 
         if($this->email->send()) {
            //$this->session->set_flashdata("email_sent","Email sent successfully."); 
            $data['message'] = "Mail sent...";  
        
         }
         else {
             //$this->session->set_flashdata("email_sent","Error in sending Email."); 
               $data['message'] = $this->email->print_debugger(); 
         }

          return $data;
    }


   
   
	public function index()
	{
        $user_type = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');

        if($user_type == 'super_admin'){
           $data['organizations'] = $this->organization_model->get_organizations($user_id, $user_type);
        }
        if($user_type == 'sys_admin'){
            $data['organizations'] = $this->organization_model->get_organizations($user_id, $user_type);
        }
        if($user_type == 'manager'){
            $data['organization_single'] = $this->organization_model->get_organizations($user_id, $user_type);
            $data['divisions'] = $this->organization_model->get_organizations($user_id, $user_type);
             foreach($data['organization_single'] as $organization){
                $organization_uid = $organization->organization_uid;
            }
            $newData = array(
                'organization_uid' => $organization_uid
            );
            $this->session->set_userdata($newData);// set current organization uid
        }
        if($user_type == 'division_head'){
            $data['organization_single'] = $this->organization_model->get_organizations($user_id, $user_type);
            $data['divisions'] = $this->organization_model->get_organizations($user_id, $user_type);
            foreach($data['organization_single'] as $organization){
                $organization_uid = $organization->organization_uid;
            }
            $newData = array(
                'organization_uid' => $organization_uid
            );
            $this->session->set_userdata($newData);// set current organization uid
            
        }
		$data['header_view'] = "templates/header";
		$data['main_view'] = "pages/dashboard";
        $this->load->view('templates/template_main',$data);
	}
    

    // Manage Section / Method
    public function manage(){

        $user_type = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');

        $controller_name = $this->uri->segment(1);// controller
        $method_name = $this->uri->segment(2);// method
        $org_uniq_name = $this->uri->segment(3);// organization unique name    | section
        $section = $this->uri->segment(4);// section  | action 
        $action = $this->uri->segment(5);// action
        $ref_value = $this->uri->segment(6);// parameter


        /**
         * Manage all organizations
         * @var [type]
         */
        if($method_name == 'manage' &&  $org_uniq_name == 'organizations' && $user_type != 'division_head'){
            
            if($section == ''){
                $data['organizations'] = $this->organization_model->get_organizations($user_id, $user_type);
                $data['main_view'] = "pages/manage/organizations/index";
                $data['side_menu'] = "side_menus/manage_side_menu";
            }
            // organizations create
            else if($section == 'create'){

                  //validate from data submited or not
                $this->form_validation->set_rules('organization_name','Organization Name','trim|required');
                $this->form_validation->set_rules('organization_uid','Organization Unique ID','trim|required');

                
                if($this->form_validation->run() == FALSE){
                    //Default load
                    $data['main_view'] = "pages/manage/organizations/create_organization";
                    $data['side_menu'] = "side_menus/manage_side_menu";
                }

                else{
                     // When Form Data Submited
                    $new_org_uid = $this->input->post('organization_uid');
                    $new_org_uid = str_replace(" ", "-", strtolower($new_org_uid));
                    $data = array(
                        'organization_name' => $this->input->post('organization_name'),
                        'organization_uid' => $new_org_uid
                    );

                    if($this->organization_model->create_organization($data)){
                        $this->session->set_flashdata('organization_created','Organization has been created');
                        redirect("dashboard/manage/organizations/");
                    }
                }
                
            }
             // organizations edit
            else if($section == 'edit'){

                //get org id
                $organization_id = $this->uri->segment(5);

                //validate from data submited or not
                $this->form_validation->set_rules('organization_name','Organization Name','trim|required');
                $this->form_validation->set_rules('organization_uid','Organization Unique ID','trim|required');

                if($this->form_validation->run() == FALSE){
                    $data['organization_data'] = $this->organization_model->get_organization_info($organization_id);
                    $data['main_view'] = "pages/manage/organizations/edit_organization";
                    $data['side_menu'] = "side_menus/manage_side_menu";
                }else{

                    $new_org_uid = $this->input->post('organization_uid');
                    $new_org_uid = str_replace(" ", "-", strtolower($new_org_uid));
                    $data = array(
                        'organization_name' => $this->input->post('organization_name'),
                        'organization_uid' => $new_org_uid
                    );

                    if($this->organization_model->update_organization_info($organization_id,$data)){
                        $this->session->set_flashdata('organization_updated','Organization  has been updated');
                        redirect("dashboard/manage/organizations/");
                    }
                }
            } 
            // organizations delete
            else if($section == 'delete'){

                 //get org id
                $organization_id = $this->uri->segment(5);

                $data = array(
                    'active_status' => 0,
                );
                if($this->organization_model->update_organization_info($organization_id,$data)){
                    $this->session->set_flashdata('organization_deleted','Organization  has been deleted');
                    redirect("dashboard/manage/organizations/");
                }
            }
            else{
                $data['main_view'] = "pages/error";
            }
            $data['header_view'] = "templates/header";
            $this->load->view('templates/template_main',$data);
        }

        else if($org_uniq_name && 
            $this->auth_model->is_org_access($org_uniq_name,$user_id,$user_type) &&
            $this->auth_model->is_section($section,$user_id,$user_type,$method_name) && 
            $this->auth_model->is_action($action,$user_id,$user_type,$section,$method_name)){
              
                /**
                *  Manager Section
                */
               if($section == 'manager'){
                    if($action == ''){
                        //view managers
                        $data['managers'] = $this->manager_model->get_managers($org_uniq_name);
                        $data['main_view'] = "pages/manage/manager/index";
                    }
                    else if($action == 'create'){
                        // Create Manager
                        $this->form_validation->set_rules('first_name','Manager First Name','trim|required');
                        $this->form_validation->set_rules('last_name','Manager Last Name','trim|required');
                        $this->form_validation->set_rules('username','Manager Username','trim|required');
                        $this->form_validation->set_rules('email','Manager Email','trim|required');
                        if($this->form_validation->run() == FALSE){
                            // View Create Manager Form
                            $data['main_view'] = "pages/manage/manager/create_manager"; 
                        }else{

                            //uniq_id
                            $uniq_id = $this->createUniqueId();

                             // After Submit Create Manager Form
                            $data = array(
                                'first_name' => $this->input->post('first_name'),
                                'last_name' => $this->input->post('last_name'),
                                'email' => $this->input->post('email'),
                                'username' => $this->input->post('username'),
                                'pwd_unique_id' => $uniq_id
                               
                            );

                            if($this->manager_model->create_manager($data,$org_uniq_name)){

                                //send email with uniq id for enter password
                                 $data['messages'] = $this->emailSend($uniq_id);

                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('manager_created','Manager has been created.Password reset link has been sent to email');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/manager/'));
                            }
                        }

                         
                    }
                    else if($action == 'edit'){
                        //Edit manager

                        $manager_id = $this->uri->segment(6);//get manager id

                        $this->form_validation->set_rules('first_name','Manager First Name','trim|required');
                        $this->form_validation->set_rules('last_name','Manager Last Name','trim|required');
                        $this->form_validation->set_rules('username','Manager Username','trim|required');
                        $this->form_validation->set_rules('email','Manager Email','trim|required');

                        if($this->form_validation->run() == FALSE){
                             // View Edit Manager Form
                            $data['manager_details'] = $this->manager_model->get_manager_info($manager_id,$org_uniq_name);
                            if($data['manager_details']){
                                 //Manager ID and Organization ID match
                                 $data['main_view'] = "pages/manage/manager/edit_manager"; 
                            }else{
                                //Manager ID and Organization ID not match,unauthorized access from URL
                                $data['main_view'] = "pages/error";  
                            }
                           
                        }
                        else{
                             // After Submit Edit Manager Form
                            $data = array(
                                'first_name' => $this->input->post('first_name'),
                                'last_name' => $this->input->post('last_name'),
                                'email' => $this->input->post('email'),
                                'username' => $this->input->post('username')
                            );

                            if($this->manager_model->update_manager_info($manager_id,$data,$org_uniq_name)){
                                //Update Manager Success and return to managers view
                                $this->session->set_flashdata('manager_updated','Manager has been updated');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/manager/'));
                            }
                        }
                    }
                    else if($action == 'delete'){
                         //Delete  manager
                        $manager_id = $this->uri->segment(6);//get manager id
                
                        $data = array(
                            'active_status' => 0,
                        );

                        if($this->manager_model->delete_manager($manager_id,$data,$org_uniq_name)){
                            // Delete manager , and user
                            $this->session->set_flashdata('manager_deleted','Manager has been deleted');
                            redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/manager/'));
                        }else{
                            // Delete query fail with unauthorised organization or manager ID pass by the URL
                            $data['main_view'] = "pages/error"; 
                        }
                    }
               }
               
                /**
                *  Division Section
                */
               if($section == 'division'){
                    if($action == ''){
                        //view divisions
                        $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                        $data['main_view'] = "pages/manage/division/index";
                    }
                    else if($action == 'create'){
                        //Create divisions
                        $this->form_validation->set_rules('division_name','Division Name','trim|required');

                        if($this->form_validation->run() == FALSE){
                            // View Create Division Form
                           $data['main_view'] = "pages/manage/division/create_division";
                        }else{
                             // After Submit Create Division Form
                            $data = array(
                                'division_name' => $this->input->post('division_name')
                            );

                            if($this->division_model->create_division($data,$org_uniq_name)){
                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('division_created','Division has been created');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division/'));
                            }else{
                                // Insert query fail with unauthorised organization or manager ID pass by the URL
                                $data['main_view'] = "pages/error"; 
                            }
                        }
                    
                         
                    }else if($action == 'edit'){
                        //Edit divisions
                        
                        $division_id = $this->uri->segment(6);//get division id

                        $this->form_validation->set_rules('division_name','Division Name','trim|required');
                       
                        if($this->form_validation->run() == FALSE){
                             // View Edit Manager Form
                            $data['division_details'] = $this->division_model->get_division_info($division_id,$org_uniq_name);
                            if($data['division_details']){
                                 //division_id  and Organization ID match
                                 $data['main_view'] = "pages/manage/division/edit_division";
                            }else{
                                //division_id ID and Organization ID not match,unauthorized access from URL
                                $data['main_view'] = "pages/error";  
                            }
                           
                        }
                        else{
                              // After Submit Edit Division Form
                            $data = array(
                               'division_name' => $this->input->post('division_name')
                            );

                            if($this->division_model->update_division_info($division_id,$data,$org_uniq_name)){
                                //Update Manager Success and return to managers view
                                $this->session->set_flashdata('division_updated','Division has been updated');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division/'));
                            }
                        }
                       
                    }
                     else if($action == 'delete'){
                         //Delete  Division
                        $division_id = $this->uri->segment(6);//get division id
                
                        $data = array(
                            'active_status' => 0,
                        );

                        if($this->division_model->delete_division($division_id,$data,$org_uniq_name)){
                            // Delete Division success
                            $this->session->set_flashdata('division_deleted','Division has been deleted');
                            redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division/'));
                        }else{
                            // Delete query fail with unauthorised organization or division ID pass by the URL
                            $data['main_view'] = "pages/error"; 
                        }
                    }
               }

                /**
                *  Division Head Section
                */
               if($section == 'division_head'){
                    if($action == ''){

                         $data['division_heads'] = $this->division_head_model->get_division_heads($org_uniq_name,$user_type,$user_id);
                         $data['main_view'] = "pages/manage/division_head/index";

                    }
                    else if($action == 'create'){
                        // Create Division Head
                        $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                        
                        $this->form_validation->set_rules('first_name','Division Head First Name','trim|required');
                        $this->form_validation->set_rules('last_name','Division Head Last Name','trim|required');
                        $this->form_validation->set_rules('username','Division Head Username','trim|required');
                        $this->form_validation->set_rules('email','Division Head Email','trim|required');
                        $this->form_validation->set_rules('division_name','Division','trim|required');
                        if($this->form_validation->run() == FALSE){
                            // View Create Division Head Form
                            $data['main_view'] = "pages/manage/division_head/create_division_head";
                        }else{
                             // After Submit Create Manager Form
                            $data = array(
                                'first_name' => $this->input->post('first_name'),
                                'last_name' => $this->input->post('last_name'),
                                'email' => $this->input->post('email'),
                                'username' => $this->input->post('username'),
                                'division_id' => $this->input->post('division_name')
                            );

                            if($this->division_head_model->create_division_head($data,$org_uniq_name)){
                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('division_head_created','Division Head has been created');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division_head/'));
                            }
                        }


                    }
                    else if($action == 'edit'){
                       
                        //Edit Division Head
                        
                        $division_head_id = $this->uri->segment(6);//get division id

                        $this->form_validation->set_rules('first_name','Division Head First Name','trim|required');
                        $this->form_validation->set_rules('last_name','Division Head Last Name','trim|required');
                        $this->form_validation->set_rules('username','Division Head Username','trim|required');
                        $this->form_validation->set_rules('email','Division Head Email','trim|required');
                        $this->form_validation->set_rules('division_name','Division','trim|required');
                       
                        if($this->form_validation->run() == FALSE){
                             // View Edit Division Head Form
                            $data['division_head_details'] = $this->division_head_model->get_division_head_info($division_head_id,$org_uniq_name);
                            $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                            if($data['division_head_details']){
                                 //division_head_id  and Organization ID match
                                  $data['main_view'] = "pages/manage/division_head/edit_division_head";
                            }else{
                                //division_head_id ID and Organization ID not match,unauthorized access from URL
                                $data['main_view'] = "pages/error";  
                            }
                           
                        }
                        else{
                              // After Submit Edit Division Head Form
                            $data = array(
                                'first_name' => $this->input->post('first_name'),
                                'last_name' => $this->input->post('last_name'),
                                'email' => $this->input->post('email'),
                                'username' => $this->input->post('username'),
                                'division_id' => $this->input->post('division_name')
                            );

                            if($this->division_head_model->update_division_head_info($division_head_id,$data,$org_uniq_name)){
                                //Update Manager Success and return to managers view
                                $this->session->set_flashdata('division_head_updated','Division Head has been updated');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division_head/'));
                            }
                        }
                    }
                    else if($action == 'delete'){
                         //Delete  Division Head
                        $division_head_id = $this->uri->segment(6);//get division head id
                
                        $data = array(
                            'active_status' => 0,
                        );

                        if($this->division_head_model->delete_division_head($division_head_id,$data,$org_uniq_name)){
                            // Delete Division Head success
                            $this->session->set_flashdata('division_head_deleted','Division Head has been deleted');
                            redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division_head/'));
                        }else{
                            // Delete query fail with unauthorised organization or division ID pass by the URL
                            $data['main_view'] = "pages/error"; 
                        }
                    }
               }
                /**
                *  Employee Section  / Manage
                */
               if($section == 'employee'){
                    if($action == ''){
                        //view employees
                        $data['employees'] = $this->employee_model->get_employees($org_uniq_name);
                        $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                       

                        $this->form_validation->set_rules('division_id','Division','trim|required');
                        if($this->form_validation->run() == FALSE){
                             $data['selected_division_id'] = '';
                            // View all employee data
                             $data['main_view'] = "pages/manage/employee/index";
                        }else{
                             // After Submit Assign KPI to division Form
                            $data = array(
                                'division_id' => $this->input->post('division_id')
                            );

                            if($this->input->post('division_id')){
                                $data['employees'] = $this->employee_model->get_employees_filter_by_employee($org_uniq_name,$user_type,$user_id,$data);

                                $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                                $data['selected_division_id'] = $this->input->post('division_id');
                                // View Filtered division to kpi data
                                $data['main_view'] = "pages/manage/employee/index";  
                            }
                        }

                    }
                    else if($action == 'create'){
                          // Create Employee
                        $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                        
                        $this->form_validation->set_rules('employee_name','Employee Name','trim|required');
                        $this->form_validation->set_rules('division_name','Division','trim|required');
                        if($this->form_validation->run() == FALSE){
                            // View Create Employee Form
                             $data['main_view'] = "pages/manage/employee/create_employee";
                        }else{
                             // After Submit Create Employee Form
                            $data = array(
                                'employee_name' => $this->input->post('employee_name'),
                                'division_id' => $this->input->post('division_name')
                            );

                            if($this->employee_model->create_employee($data,$org_uniq_name)){
                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('employee_created','Employee has been created');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee/'));
                            }
                        }
                    }
                    else if($action == 'edit'){
                         //Edit Employee 
                        
                        $employee_id = $this->uri->segment(6);//get employee id

                        $this->form_validation->set_rules('employee_name','Employee Name','trim|required');
                        $this->form_validation->set_rules('division_name','Division','trim|required');
                       
                        if($this->form_validation->run() == FALSE){
                             // View Edit Employee  Form
                            $data['employee_details'] = $this->employee_model->get_employee_info($employee_id,$org_uniq_name);
                            $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                            if($data['employee_details']){
                                 //employee_id  and Organization ID match
                                $data['main_view'] = "pages/manage/employee/edit_employee";
                            }else{
                                //employee_id ID and Organization ID not match,unauthorized access from URL
                                $data['main_view'] = "pages/error";  
                            }
                           
                        }
                        else{
                              // After Submit Edit Employee  Form
                            $data = array(
                                'employee_name' => $this->input->post('employee_name'),
                                'division_id' => $this->input->post('division_name')
                            );

                            if($this->employee_model->update_employee_info($employee_id,$data,$org_uniq_name)){
                                //Update Manager Success and return to managers view
                                $this->session->set_flashdata('employee_updated','Employee has been updated');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee/'));
                            }
                        }
                    }
                    else if($action == 'delete'){
                         //Delete  Employee
                        $employee_id = $this->uri->segment(6);//get division head id
                
                        $data = array(
                            'active_status' => 0,
                        );

                        if($this->employee_model->delete_employee($employee_id,$data,$org_uniq_name)){
                            // Delete Division Head success
                            $this->session->set_flashdata('employee_deleted','Employee has been deleted');
                            redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/employee/'));
                        }else{
                            // Delete query fail with unauthorised organization or division ID pass by the URL
                            $data['main_view'] = "pages/error"; 
                        }
                    }
               }
                /**
                *  KPI Section
                */
               if($section == 'kpi'){
                    if($action == ''){
                        // View KPIs     
                         $data['kpis'] = $this->kpi_model->get_kpis($org_uniq_name,$user_type,$user_id);
                         $data['main_view'] = "pages/manage/kpi/index";
                    }
                    else if($action == 'create'){
                        // Create KPI
                
                        $this->form_validation->set_rules('kpi_name','KPI Name','trim|required');
                        $this->form_validation->set_rules('repeat_duration','Repeat Duration','trim|required');
                        $this->form_validation->set_rules('start_month','Start Month','trim|required');
                        $this->form_validation->set_rules('kpi_value','KPI by option','trim|required');

                        $kpi_by_val =  $this->input->post('kpi_value');
                        if($kpi_by_val == 'kpi_by_value'){

                            $this->form_validation->set_rules('value_by_value1','Value','trim|required');    
                            $this->form_validation->set_rules('operation_status','Operation Status','trim|required');
                             
                        } 
                        else if($kpi_by_val == 'kpi_by_range'){
                            $this->form_validation->set_rules('value_one','Value 1','trim|required');   
                            $this->form_validation->set_rules('value_two','Value 2','trim|required');   
                            $this->form_validation->set_rules('operation_status2','Operation Status','trim|required');   
                           
                        }


                        if($this->form_validation->run() == FALSE){
                            // View Create KPI Form
                            $data['main_view'] = "pages/manage/kpi/create_kpi";
                        }else{
                            // After Submit Create KPI Form

                            $kpi_by_val =  $this->input->post('kpi_value');

                            if($kpi_by_val == 'kpi_by_value'){

                                $data = array(
                                    'kpi_name' => $this->input->post('kpi_name'),
                                    'repeat_duration' => $this->input->post('repeat_duration'),
                                    'start_month' => $this->input->post('start_month'),
                                    'value_by_value1' => $this->input->post('value_by_value1'),
                                    'operation_status' => $this->input->post('operation_status')
                                );

                                if($this->kpi_model->create_kpi($data,$org_uniq_name,$kpi_by_val)){
                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('kpi_created','KPI has been created');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi/'));
                                }
                            }
                            else if($kpi_by_val == 'kpi_by_range'){

                                $data = array(
                                    'kpi_name' => $this->input->post('kpi_name'),
                                    'repeat_duration' => $this->input->post('repeat_duration'),
                                    'start_month' => $this->input->post('start_month'),
                                    'value_one' => $this->input->post('value_one'),
                                    'value_two' => $this->input->post('value_two'),
                                    'operation_status2' => $this->input->post('operation_status2')

                                );

                                if($this->kpi_model->create_kpi($data,$org_uniq_name,$kpi_by_val)){
                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('kpi_created','KPI has been created');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi/'));
                                }

                            }
    
                        }


                    }
                    else if($action == 'edit'){
                       

                        $kpi_id = $this->uri->segment(6);//get kpi id

                        $this->form_validation->set_rules('kpi_name','KPI Name','trim|required');
                        $this->form_validation->set_rules('repeat_duration','Repeat Duration','trim|required');
                        $this->form_validation->set_rules('kpi_value','KPI by option','trim|required');
                        $kpi_by_val =  $this->input->post('kpi_value');
                        if($kpi_by_val == 'kpi_by_value'){

                            $this->form_validation->set_rules('value_by_value1','Value','trim|required');    
                            $this->form_validation->set_rules('operation_status','Operation Status','trim|required');
                        } 
                        else if($kpi_by_val == 'kpi_by_range'){
                            $this->form_validation->set_rules('value_one','Value 1','trim|required');   
                            $this->form_validation->set_rules('value_two','Value 2','trim|required');   
                            $this->form_validation->set_rules('operation_status2','Operation Status','trim|required'); 
                            
                        }
                       
                        if($this->form_validation->run() == FALSE){
                             // View Edit Employee  Form
                            $data['kpi_details'] = $this->kpi_model->get_kpi_info($kpi_id,$org_uniq_name);
                           
                            if($data['kpi_details']){
                                 //kpi_id  and Organization ID match
                                 $data['main_view'] = "pages/manage/kpi/edit_kpi";
                            }else{
                                //kpi_id and Organization ID not match,unauthorized access from URL
                                $data['main_view'] = "pages/error";  
                            }
                           
                        }
                        else{
                              // After Submit Edit Employee  Form
                              
                            $kpi_by_val =  $this->input->post('kpi_value');

                            if($kpi_by_val == 'kpi_by_value'){

                                $data = array(
                                    'kpi_name' => $this->input->post('kpi_name'),
                                    'repeat_duration' => $this->input->post('repeat_duration'),
                                    'value_by_value1' => $this->input->post('value_by_value1'),
                                    'operation_status' => $this->input->post('operation_status')

                                );

                                if($this->kpi_model->update_kpi_info($data,$org_uniq_name,$kpi_by_val,$kpi_id)){
                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('kpi_updated','KPI has been updated');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi/'));
                                }
                            }
                            else if($kpi_by_val == 'kpi_by_range'){

                                $data = array(
                                    'kpi_name' => $this->input->post('kpi_name'),
                                    'repeat_duration' => $this->input->post('repeat_duration'),
                                    'value_one' => $this->input->post('value_one'),
                                    'value_two' => $this->input->post('value_two'),
                                    'operation_status2' => $this->input->post('operation_status2')   
                                );

                                if($this->kpi_model->update_kpi_info($data,$org_uniq_name,$kpi_by_val,$kpi_id)){
                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('kpi_updated','KPI has been updated');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi/'));
                                }

                            } 

                        }
                    }
                    else if($action == 'delete'){
                         //Delete  KPI
                        $kpi_id = $this->uri->segment(6);//get division head id
                
                        $data = array(
                            'active_status' => 0,
                        );

                        if($this->kpi_model->delete_kpi($kpi_id,$data,$org_uniq_name)){
                            // Delete KPI success
                            $this->session->set_flashdata('kpi_deleted','KPI has been deleted');
                            redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/kpi/'));
                        }else{
                            // Delete query fail with unauthorised organization or division ID pass by the URL
                            $data['main_view'] = "pages/error"; 
                        }
                    }
               }
                /**
                *  Assign KPI to Division Section
                */
               if($section == 'assign_kpi_to_division'){
                    if($action == ''){
                         // View KPI to divisions    
                        $data['kpi_to_divisions'] = $this->kpi_to_division_model->get_kpi_to_division($org_uniq_name,$user_type,$user_id);
                        $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                       


                         $this->form_validation->set_rules('division_id','Division','trim|required');
                         if($this->form_validation->run() == FALSE){
                            $data['selected_division_id'] = '';
                            // View all division to kpi data
                            $data['main_view'] = "pages/manage/assign_kpi_to_division/index";
                         }else{
                             // After Submit Assign KPI to division Form
                            $data = array(
                                'division_id' => $this->input->post('division_id')
                            );

                            if($this->input->post('division_id')){
                                $data['kpi_to_divisions'] = $this->kpi_to_division_model->get_kpi_to_division_filter_by_division($org_uniq_name,$user_type,$user_id,$data);
                                $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                                $data['selected_division_id'] = $this->input->post('division_id');
                                // View Filtered division to kpi data
                                $data['main_view'] = "pages/manage/assign_kpi_to_division/index";  
                            }
                        }
                    }
                    else if($action == 'create'){
                        // Assign KPI to division
                         $data['kpis'] = $this->kpi_model->get_kpis($org_uniq_name,$user_type,$user_id);
                         $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                        
                        $this->form_validation->set_rules('division_id','Division','trim|required');
                        $this->form_validation->set_rules('kpi_id','KPI name','trim|required');
                        $this->form_validation->set_rules('target_value','Target Value','trim|required');
                        if($this->form_validation->run() == FALSE){
                            // View Assign KPI to division Form
                             $data['main_view'] = "pages/manage/assign_kpi_to_division/create_assign_kpi_to_division";
                        }else{
                             // After Submit Assign KPI to division Form
                            $data = array(
                                'division_id' => $this->input->post('division_id'),
                                'kpi_id' => $this->input->post('kpi_id'),
                                'target_value' => $this->input->post('target_value')
                            );

                            if($this->kpi_to_division_model->assign_kpi_to_division($data,$org_uniq_name)){
                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('kpi_to_division_created','KPI has been assigned to Division');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_division/'));
                            }
                        }



                    }
                    else if($action == 'edit'){
                        //Edit KPI to Division
                        
                        $kpi_association_id = $this->uri->segment(6);//get kpi_association id

                        
                        $this->form_validation->set_rules('target_value','Target Value','trim|required');
                       
                        if($this->form_validation->run() == FALSE){
                             // View Edit KPI to Division
                            $data['kpis'] = $this->kpi_model->get_kpis($org_uniq_name,$user_type,$user_id);
                            $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);

                            $data['kpi_to_division_info'] = $this->kpi_to_division_model->get_kpi_to_division_info($kpi_association_id,$org_uniq_name,$user_type,$user_id);
                            if($data['kpi_to_division_info']){
                                 //kpi_association  and Organization ID match
                                 $data['main_view'] = "pages/manage/assign_kpi_to_division/edit_assign_kpi_to_division";
                            }else{
                                //kpi_association ID and Organization ID not match,unauthorized access from URL
                                $data['main_view'] = "pages/error";  
                            }
                           
                        }
                        else{
                              // After Submit Edit  KPI to Division  Form
                            $data = array(
                                'target_value' => $this->input->post('target_value')
                            );

                            if($this->kpi_to_division_model->update_kpi_to_division_info($kpi_association_id,$data,$org_uniq_name)){
                                //Update  KPI to Division Success and return to  view
                                $this->session->set_flashdata('kpi_to_division_updated','KPI to Division has been updated');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_division/'));
                            }
                        }

                    }
                    else if($action == 'delete'){
                         //Delete   KPI to Division
                        $kpi_association_id = $this->uri->segment(6);//get kpi_association_id 
                
                        $data = array(
                            'active_status' => 0,
                        );

                        if($this->kpi_to_division_model->delete_kpi_to_division($kpi_association_id,$data,$org_uniq_name)){
                            // Delete  KPI to Division success
                            $this->session->set_flashdata('kpi_to_division_deleted','KPI to Division has been deleted');
                            redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_division/'));
                        }else{
                            // Delete query fail with unauthorised organization or division ID pass by the URL
                            $data['main_view'] = "pages/error"; 
                        }
                    }
               }
                /**
                *  Assign KPI to Employee Section
                */
               if($section == 'assign_kpi_to_employee'){
                    if($action == ''){
                         // View KPI to employees    
                       // $data['kpi_to_employees'] = $this->kpi_to_employee_model->get_kpi_to_employee($org_uniq_name,$user_type,$user_id);
                        //$data['employees'] = $this->employee_model->get_employees($org_uniq_name);
                    
                        $this->form_validation->set_rules('division_id','Employee','trim|required');
                         if($this->form_validation->run() == FALSE){
                            // View all division to kpi data
                             $data['kpi_to_employees'] = $this->kpi_to_employee_model->get_kpi_to_employee($org_uniq_name,$user_type,$user_id);
                            $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                            $data['selected_division_id'] = '';
                            $div_id = 'all';
                             $data['employeeList'] = $this->employee_model->get_employee_list_for_division($div_id,$org_uniq_name);
                             $data['main_view'] = "pages/manage/assign_kpi_to_employee/index";
                         }else{
                             // After Submit Assign KPI to employee  Form
                            if($this->input->post('division_id') && $this->input->post('employee_id') == 'all'){
                                $data = array(
                                    'division_id' => $this->input->post('division_id')
                                );
                                $data['kpi_to_employees'] = $this->kpi_to_employee_model->get_kpi_to_employee_filter_by_employee($org_uniq_name,$user_type,$user_id,$data);
                                $div_id = $this->input->post('division_id');
                                $data['employeeList'] = $this->employee_model->get_employee_list_for_division($div_id,$org_uniq_name);
                                $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                                $data['selected_division_id'] = $this->input->post('division_id');
                                $data['selected_employee_id'] = 'all';
                                $data['main_view'] = "pages/manage/assign_kpi_to_employee/index";
                            }
                            else{
                                $data = array(
                                    'employee_id' => $this->input->post('employee_id')
                                );
                                $data['kpi_to_employees'] = $this->kpi_to_employee_model->get_kpi_to_employee_filter_by_division_employee($org_uniq_name,$user_type,$user_id,$data);
                                $div_id = $this->input->post('division_id');
                                $data['employeeList'] = $this->employee_model->get_employee_list_for_division($div_id,$org_uniq_name);
                                $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                                $data['selected_division_id'] = $this->input->post('division_id');
                                $data['selected_employee_id'] = $this->input->post('employee_id');
                                 $data['main_view'] = "pages/manage/assign_kpi_to_employee/index";

                            }

                        }
                    }
                    else if($action == 'create'){
                        

                          // Assign KPI to division
                         $data['kpis'] = $this->kpi_model->get_kpis($org_uniq_name,$user_type,$user_id);
                         $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                         $data['employees'] = $this->employee_model->get_employees($org_uniq_name);
                        
                        $this->form_validation->set_rules('employee_id','Employee','trim|required');
                        $this->form_validation->set_rules('kpi_id','KPI name','trim|required');
                        $this->form_validation->set_rules('target_value','Target Value','trim|required');
                        if($this->form_validation->run() == FALSE){
                            // View Assign KPI to division Form
                            // Assign KPI to employees   
                            $data['main_view'] = "pages/manage/assign_kpi_to_employee/create_assign_kpi_to_employee";;
                        }else{
                             // After Submit Assign KPI to division Form
                            $data = array(
                                'employee_id' => $this->input->post('employee_id'),
                                'kpi_id' => $this->input->post('kpi_id'),
                                'target_value' => $this->input->post('target_value')
                            );

                            if($this->kpi_to_employee_model->assign_kpi_to_employee($data,$org_uniq_name)){
                                //Create Manager Success and return to managers view
                                $this->session->set_flashdata('kpi_to_employee_created','KPI has been assigned to Employee');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_employee/'));
                            }
                        }

                    }
                    else if($action == 'edit'){
                         //Edit KPI to Employee
                                  
                        $kpi_association_id = $this->uri->segment(6);//get kpi_association id

                      
                        $this->form_validation->set_rules('target_value','Target Value','trim|required');
                       
                        if($this->form_validation->run() == FALSE){
                             // View Edit KPI to Employee
                            $data['kpis'] = $this->kpi_model->get_kpis($org_uniq_name,$user_type,$user_id);
                            $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                            $data['employees'] = $this->employee_model->get_employees($org_uniq_name);

                            $data['kpi_to_employee_info'] = $this->kpi_to_employee_model->get_kpi_to_employee_info($kpi_association_id,$org_uniq_name,$user_type,$user_id);

                            $data['kpi_to_employee_info_balance'] = $this->kpi_to_employee_model->get_available_employee_kpi_target_balance_to_edit($kpi_association_id,$org_uniq_name,$user_type,$user_id);

                            if($data['kpi_to_employee_info']){
                                 //kpi_association  and Organization ID match
                                $data['main_view'] = "pages/manage/assign_kpi_to_employee/edit_assign_kpi_to_employee";
                            }else{
                                //kpi_association ID and Organization ID not match,unauthorized access from URL
                                $data['main_view'] = "pages/error";  
                            }
                           
                        }
                        else{
                              // After Submit Edit  KPI to Division  Form
                            $data = array(
                                'target_value' => $this->input->post('target_value')
                            );

                            if($this->kpi_to_employee_model->update_kpi_to_employee_info($kpi_association_id,$data,$org_uniq_name)){
                                //Update  KPI to Division Success and return to  view
                                $this->session->set_flashdata('kpi_to_employee_updated','KPI to Employee has been updated');
                                redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_employee/'));
                            }
                        }
                    }
                     else if($action == 'delete'){
                         //Delete   KPI to Employee
                        $kpi_association_id = $this->uri->segment(6);//get kpi_association_id 
                
                        $data = array(
                            'active_status' => 0,
                        );

                        if($this->kpi_to_employee_model->delete_kpi_to_employee($kpi_association_id,$data,$org_uniq_name)){
                            // Delete  KPI to Employee Success
                            $this->session->set_flashdata('kpi_to_employee_deleted','KPI to Employee has been deleted');
                            redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/assign_kpi_to_employee/'));
                        }else{
                            // Delete query fail with unauthorised organization or division ID pass by the URL
                            $data['main_view'] = "pages/error"; 
                        }
                    }
               }

                $data['header_view'] = "templates/header_with_sidemenu";
                $data['side_menu'] = "side_menus/manage_side_menu";
                $this->load->view('templates/template_main_with_side_menu',$data);
        }
        else{
                $data['header_view'] = "templates/header";
                $data['main_view'] = "pages/error";
                $this->load->view('templates/template_main',$data);
        }


        
    }



    //Data section /Method
    public function data(){
        $user_type = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');

        $controller_name = $this->uri->segment(1);// controller
        $method_name = $this->uri->segment(2);// method
        $org_uniq_name = $this->uri->segment(3);// organization unique name    | section
        $section = $this->uri->segment(4);// section  | action 
        $action = $this->uri->segment(5);// action
        $ref_value = $this->uri->segment(6);// parameter

         /**
         * Data of all organizations
         * @var [type]
         */
        if($method_name == 'data' &&  $org_uniq_name == 'organizations' && $user_type != 'division_head'){
            
            if($section == ''){
                $data['organizations'] = $this->organization_model->get_organizations($user_id, $user_type);
                $data['main_view'] = "pages/data/organizations/index";
            }
            // organizations create
            else if($section == 'create'){

                  //validate from data submited or not
                $this->form_validation->set_rules('organization_name','Organization Name','trim|required');
                $this->form_validation->set_rules('organization_uid','Organization Unique ID','trim|required');

                
                if($this->form_validation->run() == FALSE){
                    //Default load
                    $data['main_view'] = "pages/manage/organizations/create_organization";
                    $data['side_menu'] = "side_menus/manage_side_menu";
                }

                else{
                     // When Form Data Submited
                    $new_org_uid = $this->input->post('organization_uid');
                    $new_org_uid = str_replace(" ", "-", strtolower($new_org_uid));
                    $data = array(
                        'organization_name' => $this->input->post('organization_name'),
                        'organization_uid' => $new_org_uid
                    );

                    if($this->organization_model->create_organization($data)){
                        $this->session->set_flashdata('organization_created','Organization has been created');
                        redirect("dashboard/manage/organizations/");
                    }
                }
                
            }
             // organizations edit
            else if($section == 'edit'){

                //get org id
                $organization_id = $this->uri->segment(5);

                //validate from data submited or not
                $this->form_validation->set_rules('organization_name','Organization Name','trim|required');
                $this->form_validation->set_rules('organization_uid','Organization Unique ID','trim|required');

                if($this->form_validation->run() == FALSE){
                    $data['organization_data'] = $this->organization_model->get_organization_info($organization_id);
                    $data['main_view'] = "pages/manage/organizations/edit_organization";
                    $data['side_menu'] = "side_menus/manage_side_menu";
                }else{

                    $new_org_uid = $this->input->post('organization_uid');
                    $new_org_uid = str_replace(" ", "-", strtolower($new_org_uid));
                    $data = array(
                        'organization_name' => $this->input->post('organization_name'),
                        'organization_uid' => $new_org_uid
                    );

                    if($this->organization_model->update_organization_info($organization_id,$data)){
                        $this->session->set_flashdata('organization_updated','Organization  has been updated');
                        redirect("dashboard/manage/organizations/");
                    }
                }
            } 
            // organizations delete
            else if($section == 'delete'){

                 //get org id
                $organization_id = $this->uri->segment(5);

                $data = array(
                    'active_status' => 0,
                );
                if($this->organization_model->update_organization_info($organization_id,$data)){
                    $this->session->set_flashdata('organization_deleted','Organization  has been deleted');
                    redirect("dashboard/manage/organizations/");
                }
            }
            else{
                $data['main_view'] = "pages/error";
            }
            $data['header_view'] = "templates/header";
            $this->load->view('templates/template_main',$data);

          
        }

        else if($org_uniq_name && 
            $this->auth_model->is_org_access($org_uniq_name,$user_id,$user_type)&&
            $this->auth_model->is_section($section,$user_id,$user_type,$method_name)&& 
            $this->auth_model->is_action($action,$user_id,$user_type,$section,$method_name)
           ){

            /**
             * Organization KPI list
             * @var [type]
             */
            if($section == 'organization-kpi-list'){
                if($action == ''){
                    $data['kpis'] = $this->kpi_model->get_kpis($org_uniq_name,$user_type,$user_id);
                    $data['main_view'] = "pages/data/organization_kpi_list";
                }  
            }
             /**
             * Division KPI list
             * @var [type]
             */
            if($section == 'division-kpi-list'){
                if($action == ''){
                    $data['kpi_to_divisions'] = $this->kpi_to_division_model->get_kpi_to_division($org_uniq_name,$user_type,$user_id);
                    $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                    $data['selected_division_id'] = '';
                   
                    $this->form_validation->set_rules('division_id','Division','trim|required');
                     if($this->form_validation->run() == FALSE){
                        // View all division to kpi data
                         $data['main_view'] = "pages/data/division_kpi_list";
                     }else{
                         // After Submit Assign KPI to division Form
                        $data = array(
                            'division_id' => $this->input->post('division_id')
                        );

                        if($this->input->post('division_id')){
                            $data['kpi_to_divisions'] = $this->kpi_to_division_model->get_kpi_to_division_filter_by_division($org_uniq_name,$user_type,$user_id,$data);
                            $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                            $data['selected_division_id'] = $this->input->post('division_id');
                            // View Filtered division to kpi data
                            $data['main_view'] = "pages/data/division_kpi_list"; 
                        }
                    }
                }
                //edit
                else if($action == 'edit'){
                    //get association_id
                    $association_id = $this->uri->segment(6);

                    $data['kpi_to_divisions'] = $this->kpi_to_division_model->get_single_kpi_to_division($org_uniq_name,$user_type,$user_id,$association_id);
                    $data['association_id'] = $this->uri->segment(6);
                    $data['main_view'] = "pages/data/division_kpi_list_edit";
                }
                //add previous data
                else if($action == 'previous-data'){
                    //get association_id
                    $association_id = $this->uri->segment(6);
                    $data['kpi_to_divisions_pre_data'] = $this->kpi_to_division_model->getKPIAssociateDataForPreviousData($org_uniq_name,$user_type,$user_id,$association_id);
                    $data['association_id'] = $this->uri->segment(6);


                    //validate from data submited or not
                    $this->form_validation->set_rules('datepicker1','Date','trim|required');
                    $this->form_validation->set_rules('actual_value','Actual Value','trim|required');


                    if($this->form_validation->run() == FALSE){
                        $data['main_view'] = "pages/data/division_kpi_list_edit_previous_data";
                    }else{
                        date_default_timezone_set('Asia/Calcutta');
                        $dateTime = "";
                        if($this->input->post('datepicker1')){
                            $dateTime = $this->input->post('datepicker1');
                        }

                        // After Submit Previous data form
                        $data = array(
                            'association_id' => $this->uri->segment(6),
                            'target_value' => $this->input->post('target_value'),
                            'actual_value' => $this->input->post('actual_value'),
                            'sum_or_avg_target_value' => $this->input->post('sum_or_avg_target_value') + $this->input->post('target_value'),
                            'accumulated_actual_value' => $this->input->post('accumulated_actual_value') + $this->input->post('actual_value'),
                            'created_time' => $dateTime
                        );


                        if($this->kpi_to_division_model->saveKPIAssociateDataForPreviousData($data)){
                            //Update  KPI to Division Success and return to  view
                            $this->session->set_flashdata('kpi_to_division_pre_data_updated','KPI to Division Previous Data has been updated');
                            $data['kpi_to_divisions_pre_data'] = $this->kpi_to_division_model->getKPIAssociateDataForPreviousData($org_uniq_name,$user_type,$user_id,$association_id);
                            $data['association_id'] = $this->uri->segment(6);
                            //redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division-kpi-list/edit/'.$this->uri->segment(6)));
                            $data['main_view'] = "pages/data/division_kpi_list_edit_previous_data";

                        }else{
                            $data['kpi_to_divisions_pre_data'] = $this->kpi_to_division_model->getKPIAssociateDataForPreviousData($org_uniq_name,$user_type,$user_id,$association_id);
                            $data['main_view'] = "pages/data/division_kpi_list_edit_previous_data";
                        }



                    }
                }
              
            }

             /**
             * Employee KPI list
             * @var [type]
             */
            if($section == 'employee-kpi-list'){
                if($action == ''){
                     

                    $this->form_validation->set_rules('division_id','Division','trim|required');
                    if($this->form_validation->run() == FALSE){
                        $data['kpi_to_employees'] = $this->kpi_to_employee_model->get_kpi_to_employee($org_uniq_name,$user_type,$user_id);
                        $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                        $data['selected_division_id'] = '';
                        $div_id = 'all';
                        $data['employeeList'] = $this->employee_model->get_employee_list_for_division($div_id,$org_uniq_name);
                        $data['main_view'] = "pages/data/employee_kpi_list";

                    }else{
                       
                        if($this->input->post('division_id') && $this->input->post('employee_id') == 'all'){

                            //only dropdown select division
                            
                            $data = array(
                                'division_id' => $this->input->post('division_id')
                            );
                            $data['kpi_to_employees'] = $this->kpi_to_employee_model->get_kpi_to_employee_filter_by_employee($org_uniq_name,$user_type,$user_id,$data);
                            $div_id = $this->input->post('division_id');
                            $data['employeeList'] = $this->employee_model->get_employee_list_for_division($div_id,$org_uniq_name);
                            $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                            $data['selected_division_id'] = $this->input->post('division_id');
                            $data['selected_employee_id'] = 'all';

                            // View Filtered employee kpi list data
                            $data['main_view'] = "pages/data/employee_kpi_list";

                        }
                        else{

                            $data = array(
                                'employee_id' => $this->input->post('employee_id')
                            );
                            $data['kpi_to_employees'] = $this->kpi_to_employee_model->get_kpi_to_employee_filter_by_division_employee($org_uniq_name,$user_type,$user_id,$data);
                            $div_id = $this->input->post('division_id');
                            $data['employeeList'] = $this->employee_model->get_employee_list_for_division($div_id,$org_uniq_name);
                            $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                            $data['selected_division_id'] = $this->input->post('division_id');
                            $data['selected_employee_id'] = $this->input->post('employee_id');

                            // View Filtered employee kpi list data
                            $data['main_view'] = "pages/data/employee_kpi_list";
                        }
                    }
                   
                  
                    

                }
                 //edit
                else if($action == 'edit'){
                    //get association_id
                    $association_id = $this->uri->segment(6);

                    $data['kpi_to_employees'] = $this->kpi_to_employee_model->get_single_kpi_to_employee($org_uniq_name,$user_type,$user_id,$association_id);
                    $data['association_id'] = $this->uri->segment(6);
                    $data['main_view'] = "pages/data/employee_kpi_list_edit";
                }
                //add employee kpi previous data
                else if($action == 'previous-data'){
                    //get association_id
                    $association_id = $this->uri->segment(6);
                    $data['kpi_to_divisions_pre_data'] = $this->kpi_to_division_model->getKPIAssociateDataForPreviousData($org_uniq_name,$user_type,$user_id,$association_id);
                    $data['association_id'] = $this->uri->segment(6);


                    //validate from data submited or not
                    $this->form_validation->set_rules('datepicker1','Date','trim|required');
                    $this->form_validation->set_rules('actual_value','Actual Value','trim|required');


                    if($this->form_validation->run() == FALSE){
                        $data['main_view'] = "pages/data/employee_kpi_list_edit_previous_data";
                    }else{
                        date_default_timezone_set('Asia/Calcutta');
                        $dateTime = "";
                        if($this->input->post('datepicker1')){
                            $dateTime = $this->input->post('datepicker1');
                        }

                        // After Submit Previous data form
                        $data = array(
                            'association_id' => $this->uri->segment(6),
                            'target_value' => $this->input->post('target_value'),
                            'actual_value' => $this->input->post('actual_value'),
                            'sum_or_avg_target_value' => $this->input->post('sum_or_avg_target_value') + $this->input->post('target_value'),
                            'accumulated_actual_value' => $this->input->post('accumulated_actual_value') + $this->input->post('actual_value'),
                            'created_time' => $dateTime
                        );


                        if($this->kpi_to_division_model->saveKPIAssociateDataForPreviousData($data)){
                            //Update  KPI to Division Success and return to  view
                            $this->session->set_flashdata('kpi_to_division_pre_data_updated','KPI to Division Previous Data has been updated');
                            $data['kpi_to_divisions_pre_data'] = $this->kpi_to_division_model->getKPIAssociateDataForPreviousData($org_uniq_name,$user_type,$user_id,$association_id);
                            $data['association_id'] = $this->uri->segment(6);
                            //redirect(site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/division-kpi-list/edit/'.$this->uri->segment(6)));
                            $data['main_view'] = "pages/data/employee_kpi_list_edit_previous_data";

                        }else{
                            $data['kpi_to_divisions_pre_data'] = $this->kpi_to_division_model->getKPIAssociateDataForPreviousData($org_uniq_name,$user_type,$user_id,$association_id);
                            $data['main_view'] = "pages/data/employee_kpi_list_edit_previous_data";
                        }



                    }
                }
              
            }

            $data['header_view'] = "templates/header_with_sidemenu";
            $data['side_menu'] = "side_menus/data_side_menu";
            $this->load->view('templates/template_main_with_side_menu',$data);


        }

        else{
                $data['header_view'] = "templates/header";
                $data['main_view'] = "pages/error";
                $this->load->view('templates/template_main',$data);
        }



    }



    //Reports section /Method
    public function report(){



        $user_type = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');

        $controller_name = $this->uri->segment(1);// controller
        $method_name = $this->uri->segment(2);// method
        $org_uniq_name = $this->uri->segment(3);// organization unique name    | section
        $section = $this->uri->segment(4);// section  | action 
        $action = $this->uri->segment(5);// action
        $ref_value = $this->uri->segment(6);// parameter  


        

        if($method_name == 'report' &&  $org_uniq_name == 'organizations' && $user_type != 'division_head'){
            
           if($section == ''){
                $data['organizations'] = $this->organization_model->get_organizations($user_id, $user_type);
                $data['main_view'] = "pages/reports/organizations/index";
            }
            $data['header_view'] = "templates/header";
            $this->load->view('templates/template_main',$data);  
 
        }

        else if($org_uniq_name && 
            $this->auth_model->is_org_access($org_uniq_name,$user_id,$user_type)&&
            $this->auth_model->is_section($section,$user_id,$user_type,$method_name) && 
            $this->auth_model->is_action($action,$user_id,$user_type,$section,$method_name)
           ){

                 /**
                 * KPI Report
                 * @var [type]
                 */
                if($section == 'kpi-report'){
                    if($action == ''){
                             
                        $data['kpis'] = $this->kpi_model->get_kpi_list_for_organization_report($org_uniq_name,$user_type,$user_id);
                        $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                        $data['main_view'] = "pages/reports/index";
                        $data['side_menu'] = "side_menus/report_side_menu";

                        $data['header_view'] = "templates/header";
                        $this->load->view('templates/template_main',$data);

                    }

                    /**
                        pdf download section
                    */
                    if($action == 'download'){

                        if (isset($_POST['submit'])){

                            $this->load->library('Pdf');

                            $organization_uid = $_POST['organization_uid'];
                            $division_id = $_POST['division_id'];
                            $employee_id = $_POST['employee_id'];
                            $datepicker1 = $_POST['datepicker1'];
                            $datepicker2 = $_POST['datepicker2'];

                            $finalDay = '';
                            if($datepicker2){
                                date_default_timezone_set("Asia/Bangkok");
                                $finalDay = DateTime::createFromFormat('Y-m-d', $datepicker2);
                                $finalDay->modify('+1 day');
                                $finalDay =  $finalDay->format('Y-m-d');
                            }


                            date_default_timezone_set("Asia/Bangkok");

                            $date = new DateTime('now');
                            $printTime = $date->format('Y / M / d - H:i:s A');
                            $headerData = 'Organization Unique ID : '.$organization_uid.' | Division Name : ' .$division_id.' |  Employee Name : ' .$employee_id.' | Print Time : '.$printTime.' | From : '.$datepicker1.' | To : '.$datepicker2;

                            $data['kpis'] = $this->kpi_model->get_kpi_for_reports($organization_uid,$division_id,$employee_id,$datepicker1,$finalDay);

                            $htmlString = $this->load->view('pages/reports/print_report',$data,true); // here we will pass view file name that contain table data
                            $styles = <<<EOF

<style>
/** ======================================== KPI report ============================================ */
.red-txt-op{
     background:#d9534f !important;
}
.yellow-txt-op{
     background:#DCCF36 !important;
}
.green-txt-op{
    background:#0EBD0B !important;
}
.txt-white{
    color:white !important;
}
#report-summary-table-area{
    display: none;
}
#report-no-result-found{
    display: none;
}

.kpi-report-pagenation>li>a.current{
    z-index: 2;
    background-color: #FFAE34;
    border-color: #ddd;
    color: #ffffff;
}
#print-preview-btn-js{
    display: none;
}
.table{
   border:1px solid black;
}

/** ====================== assign KPI to Employee ================================== */
#balanceTarget{
    color: #d9534f;
}


@media print {
    body {
        -webkit-print-color-adjust: exact;
    }
    h5,h4{
        margin:0;
    }
    th{
       text-align:center;
       vertical-align:middle;
       padding:10px
    }
    td
    {
        text-align:right;
        vertical-align:middle;
    }
    .table{
        border:1px solid black;
    }

   .no-print{
     display:none;
     visibility: hidden;
   }
   .btn-group.cust-group{
     display:none;
     visibility: hidden;
   }

    .red-txt-op{
        -webkit-print-color-adjust: exact;
         background-color:#d9534f !important;
    }
    .yellow-txt-op{
         background-color:#DCCF36 !important;
    }
    .green-txt-op{
        background-color:#0EBD0B !important;
    }
    td.txt-white{
        color:white;
    }
    #report-summary-table-area{
        display: none;
    }
    #report-no-result-found{
        display: none;
    }

    .kpi-report-pagenation>li>a.current{
        z-index: 2;
        background-color: #FFAE34;
        border-color: #ddd;
        color: #ffffff;
    }
    a{
        display:none !important;
        visibility: hidden;
    }
    @page {
        size: auto;   /* auto is the initial value */
        margin: 10px;  /* this affects the margin in the printer settings */
    }
    .small-square.actual{
        background: #800017 !important;
    }
    .small-square.target{
        background: #EAB800 !important;
    }
    .small-square.tv1{
        background: #00804d !important;
    }
    .small-square.tv2{
        background: #650080 !important;
    }

}
</style>


EOF;

                            $html = $styles .' '.$htmlString;

                            // create new PDF document
                            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

                            $pdf->SetCreator('Sentro KPI');
                            $pdf->SetAuthor('Sentro KPI');
                            $pdf->SetTitle('Sentro KPI');
                            $pdf->SetSubject('TCPDF Tutorial');
                            $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

                            $pdf->SetHeaderData('', 0, "SENTRO KPI ".' Report',$headerData, array(0,64,255), array(0,64,128));
                            $pdf->setFooterData(array(0,64,0), array(0,64,128));

                            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

                            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

                            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

                            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                                require_once(dirname(__FILE__).'/lang/eng.php');
                                $pdf->setLanguageArray($l);
                            }

                            $pdf->setFontSubsetting(true);
                            $pdf->SetFont('dejavusans', '', 10, '', true);
                            $pdf->AddPage('L');
                            $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
                            //$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                            $pdf->writeHTML($html, true, false, true, false, '');
                            $pdf->Output('kpi_report-'.$printTime.'.pdf', 'I');


                        }
                        else{
                            redirect("dashboard/report/".$org_uniq_name."/kpi-report");
                        }

                    }    
                   
                   
                    if($action == 'kpi-id'){
                        if (isset($_POST['submit'])){


                            $this->form_validation->set_rules('datepicker1','Date 1','trim|required');
                            $this->form_validation->set_rules('datepicker2','Date 2','trim|required');

                            if($this->form_validation->run() == FALSE){

                                // View all  kpi data
                                $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                                //return the organization id
                                $organization_id = $this->organization_model->get_organization_id($org_uniq_name);
                                $kpi_id = $this->uri->segment(6);// parameter  

                                // After Submit Filter Form
                                $dataSubmit = array(
                                    'kpi_id' => $kpi_id,
                                    'organization_id' => $organization_id
                                );

                                $data['datepicker1'] = 'All';
                                $data['datepicker2'] = 'All';

                                $this->load->library('pagination');
                                $data['base_url'] = base_url().$controller_name.'/'.$method_name.'/'.$org_uniq_name.'/'.$section.'/kpi-id/'.$kpi_id;
                                //$total_row = $this->data_kpi_model->totalRecordsParameted($dataSubmit);
                                $total_row = $this->data_kpi_model->totalRecordsParameted($dataSubmit);
                                $data['total_rows'] = $total_row;
                                
                                $data["per_page"] = 5;
                                $data['num_links'] = 10;
                                $data['uri_segment'] = 7;


                                $data['cur_tag_open'] = '&nbsp;<a class="current">';
                                $data['cur_tag_close'] = '</a>';
                                $data['next_link'] = ' <span aria-hidden="true">&raquo;</span>';
                                $data['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
                               

                                $this->pagination->initialize($data);

                                $page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
                                $data["results"] = $this->data_kpi_model->getRecordsParameted($data["per_page"], $page,$dataSubmit);
                                $str_links = $this->pagination->create_links();
                                $data["links"] = explode('&nbsp;',$str_links );

                                $data["summary_details"] = $this->kpi_model->get_single_kpi_details_for_organization_report($organization_id,$kpi_id);
                              
                                $data['main_view'] = "pages/reports/kpi_report";
                                $data['side_menu'] = "side_menus/single_report_side_menu"; 
                            }
                            else{
                                // After Submit date range filter
                               

                               
                                // View all  kpi data
                                $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                                //return the organization id
                                $organization_id = $this->organization_model->get_organization_id($org_uniq_name);
                                $kpi_id = $this->uri->segment(6);// parameter  

                                // After Submit Filter Form
                                $dataSubmit = array(
                                    'kpi_id' => $kpi_id,
                                    'organization_id' => $organization_id,
                                    'datepicker1' => $this->input->post('datepicker1'),
                                    'datepicker2' => $this->input->post('datepicker2')
                                );

                                $data['datepicker1'] = $this->input->post('datepicker1');
                                $data['datepicker2'] = $this->input->post('datepicker2');

                                $this->load->library('pagination');
                                $data['base_url'] = base_url().$controller_name.'/'.$method_name.'/'.$org_uniq_name.'/'.$section.'/kpi-id/'.$kpi_id;
                                //$total_row = $this->data_kpi_model->totalRecordsParameted($dataSubmit);
                                $total_row = $this->data_kpi_model->totalRecordsParametedFilterWithDate($dataSubmit);
                                $data['total_rows'] = $total_row;
                                
                                $data["per_page"] = 5;
                                $data['num_links'] = 10;
                                $data['uri_segment'] = 7;




                                $data['cur_tag_open'] = '&nbsp;<a class="current">';
                                $data['cur_tag_close'] = '</a>';
                                $data['next_link'] = ' <span aria-hidden="true">&raquo;</span>';
                                $data['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
                               

                                $this->pagination->initialize($data);

                                $page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
                                $data["results"] = $this->data_kpi_model->getRecordsParametedFilterWithDate($data["per_page"], $page,$dataSubmit);



                                $str_links = $this->pagination->create_links();
                                $data["links"] = explode('&nbsp;',$str_links );

                                $data["summary_details"] = $this->kpi_model->get_single_kpi_details_for_organization_report($organization_id,$kpi_id);
                              
                                $data['main_view'] = "pages/reports/kpi_report";
                                $data['side_menu'] = "side_menus/single_report_side_menu"; 

                            }


                            
                        }
                        else{
                            $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                            //return the organization id
                            $organization_id = $this->organization_model->get_organization_id($org_uniq_name);
                            $kpi_id = $this->uri->segment(6);// parameter  

                            // After Submit Filter Form
                            $dataSubmit = array(
                                'kpi_id' => $kpi_id,
                                'organization_id' => $organization_id
                            );

                            $data['datepicker1'] = 'All';
                            $data['datepicker2'] = 'All';

                            $this->load->library('pagination');
                            $data['base_url'] = base_url().$controller_name.'/'.$method_name.'/'.$org_uniq_name.'/'.$section.'/kpi-id/'.$kpi_id;
                            //$total_row = $this->data_kpi_model->totalRecordsParameted($dataSubmit);
                            $total_row = $this->data_kpi_model->totalRecordsParameted($dataSubmit);
                            $data['total_rows'] = $total_row;
                            
                            $data["per_page"] = 5;
                            $data['num_links'] = 10;
                            $data['uri_segment'] = 7;


                            $data['cur_tag_open'] = '&nbsp;<a class="current">';
                            $data['cur_tag_close'] = '</a>';
                            $data['next_link'] = ' <span aria-hidden="true">&raquo;</span>';
                            $data['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
                           

                            $this->pagination->initialize($data);

                            $page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
                            $data["results"] = $this->data_kpi_model->getRecordsParameted($data["per_page"], $page,$dataSubmit);
                            $str_links = $this->pagination->create_links();
                            $data["links"] = explode('&nbsp;',$str_links );

                            $data["summary_details"] = $this->kpi_model->get_single_kpi_details_for_organization_report($organization_id,$kpi_id);
                          
                            $data['main_view'] = "pages/reports/kpi_report";
                            $data['side_menu'] = "side_menus/single_report_side_menu";
                        }

                        $data['header_view'] = "templates/header_with_sidemenu";
                        $this->load->view('templates/template_main_with_side_menu',$data);
                       
                    }
                  
                }
                  /**
                 * kpi-indicator
                 * @var [type]
                 */
                if($section == 'kpi-indicator'){
                    if($action == ''){
                          $data['divisions'] = $this->division_model->get_divisions($org_uniq_name,$user_type,$user_id);
                          $data['side_menu'] = "side_menus/indicator_side_menu";
                          $data['main_view'] = "pages/reports/kpi_indicator";
                    }
                    $data['header_view'] = "templates/header_with_sidemenu";
                    $this->load->view('templates/template_main_with_side_menu',$data);
                

                }

                


            } 
            else{
                $data['header_view'] = "templates/header";
                $data['main_view'] = "pages/error";
                $this->load->view('templates/template_main',$data);
            }



    }



   






}

