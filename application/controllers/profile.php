<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('organization_model');
        $this->load->model('manager_model');
        $this->load->model('user_model');
        $this->load->model('division_model');
        $this->load->model('division_head_model');
        $this->load->model('employee_model');
        $this->load->model('kpi_model');
        $this->load->model('kpi_to_division_model');
        $this->load->model('kpi_to_employee_model');
        $this->load->model('ajax_model');
        $this->load->model('data_kpi_model');
        if(!$this->session->userdata('logged_in')){
            $this->session->set_flashdata('no_access','Sorry you are not allowed');
            redirect('login');
        }

       
    }

    public function index()
	{
        $user_type = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');

        //get user info
        $data['user_info'] = $this->user_model->get_user_info($user_id);

        $this->form_validation->set_rules('first_name','First Name','trim|required');
        $this->form_validation->set_rules('last_name','Last Name','trim|required');
        $this->form_validation->set_rules('username','Username','trim|required');

        if($this->form_validation->run() == FALSE){
            $data['header_view'] = "templates/header";
			$data['main_view'] = "pages/profile";
        	$this->load->view('templates/template_main',$data);
        }
        else{
            $data = array(
                'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'email' => $this->input->post('email')
            );
            if($this->user_model->update_user($user_id,$data)){
                $this->session->set_flashdata('user_updated','User data has been updated');
                redirect("profile");
            }
        }

       

    }


}