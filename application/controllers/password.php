<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('organization_model');
        $this->load->model('manager_model');
        $this->load->model('user_model');
        $this->load->model('division_model');
        $this->load->model('division_head_model');
        $this->load->model('employee_model');
        $this->load->model('kpi_model');
        $this->load->model('kpi_to_division_model');
        $this->load->model('kpi_to_employee_model');
        $this->load->model('ajax_model');
        $this->load->model('data_kpi_model');
        if(!$this->session->userdata('logged_in')){
            $this->session->set_flashdata('no_access','Sorry you are not allowed');
            redirect('login');
        }

       
    }

    public function index()
	{
        $user_type = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');

        //get user info
        $data['user_info'] = $this->user_model->get_user_info($user_id);

        $this->form_validation->set_rules('current_password','Current Passowrd','trim|required');
        $this->form_validation->set_rules('new_password','New Password','trim|required|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required');

        if($this->form_validation->run() == FALSE){
            $data['header_view'] = "templates/header";
			$data['main_view'] = "pages/change_password";
        	$this->load->view('templates/template_main',$data);
        }
        else{
            $data = array(
                'current_password' => $this->input->post('current_password'),
				'new_password' => $this->input->post('new_password'),
				'confirm_password' => $this->input->post('confirm_password')
            );
            if($this->user_model->update_user_password($user_id,$data)){
                $this->session->set_flashdata('user_updated','User data has been updated');
                redirect("dashboard");
            }
            else{
 				$this->session->set_flashdata('password_change_fail','Password update has been failed.Please check again');
                redirect("password");
            }
        }

       

    }


}