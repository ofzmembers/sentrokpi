<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgotpassword extends CI_Controller {

	 public function __construct(){
        parent::__construct();
        $this->load->model('organization_model');
        $this->load->model('manager_model');
        $this->load->model('user_model');
        $this->load->model('division_model');
        $this->load->model('division_head_model');
        $this->load->model('employee_model');
        $this->load->model('kpi_model');
        $this->load->model('kpi_to_division_model');
        $this->load->model('kpi_to_employee_model');
        $this->load->model('ajax_model');
        $this->load->model('data_kpi_model');
     
    }

    public function index()
	{
		
     
		$this->form_validation->set_rules('email','Email','trim|required');

        if($this->form_validation->run() == FALSE){
            $this->load->view('pages/forgot_password');
        }
        else{
            $data = array(
				'email' => $this->input->post('email')
            );

            $unique_id = $this->user_model->get_user_unique_id($data);

            if($unique_id){

            	//send email with uniq id for enter password
                $message = $this->emailSend($unique_id);
                if($message == true){

                	$data = array(
						'active_pwd' => 0
		            );
		            if($this->user_model->reset_user_password($unique_id,$data)){
						$this->session->set_flashdata('forgot_pwd_success','Password reset link sent');
						redirect("forgotpassword");
		            }

                }else{
                	$this->session->set_flashdata('forgot_pwd_fail','Please check your email or this is not already active email');
                    redirect("forgotpassword");
                }

                
            }
            else{
 				$this->session->set_flashdata('forgot_pwd_fail','Please check your email or this is not already active email');
                redirect("forgotpassword");
            }
        }

    }


    public function resetpassword($pwdUnique_id = NULL)
	{
		$this->form_validation->set_error_delimiters('<p class="bg-danger">', '</p>');
		
        $this->form_validation->set_rules('current_password','Current Passowrd','trim|required');
        $this->form_validation->set_rules('new_password','New Password','trim|required|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required');

        if($this->form_validation->run() == FALSE){
           $this->load->view('pages/forgot_password_reset_form');
        }
        else{
            $data = array(
                'current_password' => $this->input->post('current_password'),
				'new_password' => $this->input->post('new_password'),
				'confirm_password' => $this->input->post('confirm_password')
            );
            if($this->user_model->update_reset_user_password($pwdUnique_id,$data)){
                $this->session->set_flashdata('user_password_reset_success','User password reset success');
                redirect("login");
            }
            else{
 				$this->session->set_flashdata('user_password_reset_fail','User password reset has been failed.Please check again');
               redirect("forgotpassword/resetpassword/".$pwdUnique_id);
            }
        }
    }


        /**
     * createUniqueId
     * @return [type] [description]
     */
    public function createUniqueId(){
        $this->load->helper('string');
        return random_string('sha1',20);
        return uniqid();
    }

    /**
     * emailSend
     * @return [type] [description]
     */
    public function emailSend($uniqId){


        $from_email = "sahan@modernie.com"; 
        $to_email = $this->input->post('email'); 
   
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, 'Sentrokpi'); 
         $this->email->to($to_email);
         $this->email->subject('Sentrokpi Password Reset'); 
         $this->email->message("Please click this link and set your password :- http://107.170.33.220/hostgrid/sentrokpi.com/forgotpassword/resetpassword/".$uniqId); 
   
         //Send mail 
         if($this->email->send()) {
            $message = true;  
        
         }
         else {
               $message = false; 
         }

          return $message;
    }


}
