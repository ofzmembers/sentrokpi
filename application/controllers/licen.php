<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Licen extends CI_Controller {

	 public function __construct(){
        parent::__construct();
         $this->load->model('organization_model');
         $this->load->model('manager_model');
         $this->load->model('user_model');
         $this->load->model('division_model');
         $this->load->model('division_head_model');
         $this->load->model('employee_model');
         $this->load->model('kpi_model');
         $this->load->model('kpi_to_division_model');
         $this->load->model('kpi_to_employee_model');
         $this->load->model('ajax_model');
         $this->load->model('serial_model');
         $this->load->model('data_kpi_model');
       
    }

    public function index()
    {
        $this->load->view('pages/licen_enter');
    }




    public function  UploadImage($settings = false)
    {
        // Input allows you to change where your file is coming from so you can port this code easily
        $inputname      =   (isset($settings['input']) && !empty($settings['input']))? $settings['input'] : "fileToUpload";
        // Sets your document root for easy uploading reference
        $root_dir       =   (isset($settings['root']) && !empty($settings['root']))? $settings['root'] : site_url();
        // Allows you to set a folder where your file will be dropped, good for porting elsewhere
        $target_dir     =   (isset($settings['dir']) && !empty($settings['dir']))? $settings['dir'] : "uploads/";
        // Check the file is not empty (if you want to change the name of the file are uploading)
        if(isset($settings['filename']) && !empty($settings['filename']))
            $filename   =   $settings['filename'];
        // Use the default upload name
        else
            $filename   =   preg_replace('/[^a-zA-Z0-9\.\_\-]/',"",$_FILES[$inputname]["name"]);
        // If empty name, just return false and end the process
        if(empty($filename))
            return false;
        // Check if the upload spot is a real folder
        if(!is_dir($root_dir.$target_dir))
            // If not, create the folder recursively
            mkdir($root_dir.$target_dir,0755,true);
        // Create a root-based upload path
        $target_file    =   $root_dir.$target_dir.$filename;
        // If the file is uploaded successfully...
        if(move_uploaded_file($_FILES[$inputname]["tmp_name"],$target_file)) {
            // Save out all the stats of the upload
            $stats['filename']  =   $filename;
            $stats['fullpath']  =   $target_file;
            $stats['localpath'] =   $target_dir.$filename;
            $stats['filesize']  =   filesize($target_file);
            // Return the stats
            return $stats;
        }
        // Return false
        return false;
    }



    public function auth(){
        if(isset($_FILES["fileToUpload"]["name"]) && !empty($_FILES["fileToUpload"]["name"])) {
            // Process and return results
            $file   =    $this->UploadImage();
            // If success, show image
            if($file != false)
                //echo $file['fullpath'];
                //echo $file['fullpath'];
            $passphrase = 'cant u see.where is the key $@';

            $iv = substr(md5('iv'.$passphrase, true), 0, 8);

            $key = substr(md5('pass1'.$passphrase, true) .

                md5('pass2'.$passphrase, true), 0, 24);

            $opts = array('iv'=>$iv, 'key'=>$key);

            $fp = fopen($file['fullpath'], 'rb');


            stream_filter_append($fp, 'mdecrypt.tripledes', STREAM_FILTER_READ, $opts);

            $data = rtrim(stream_get_contents($fp));

            fclose($fp);
           // echo $data;

            //check available or not that key
            $licenStatus = $this->serial_model->check_serial_number($data);
            //echo $licenStatus;
            if($licenStatus == 1){
                //key is correct
                $activeKey = $this->serial_model->activate_serial_number($data);
                //echo $activeKey;
                if($activeKey){
                    // redirect('dashboard');
                   // print "dashboard";
                    redirect('dashboard');
                }else{
                    return false;
                }
            }else{
                //key is invalid
                redirect('login');

            }


        }
    }



}