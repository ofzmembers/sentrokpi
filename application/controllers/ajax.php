<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	 public function __construct(){
        parent::__construct();
         $this->load->model('organization_model');
         $this->load->model('manager_model');
         $this->load->model('user_model');
         $this->load->model('division_model');
         $this->load->model('division_head_model');
         $this->load->model('employee_model');
         $this->load->model('kpi_model');
         $this->load->model('kpi_to_division_model');
         $this->load->model('kpi_to_employee_model');
         $this->load->model('ajax_model');
         $this->load->model('serial_model');
         $this->load->model('data_kpi_model');
        if(!$this->session->userdata('logged_in')){
            $this->session->set_flashdata('no_access','Sorry you are not allowed');
            redirect('login');
        }

       
    }

    /**
     * [update_division_actual_row_value description]
     * @return [type] [description]
     */
	public function update_division_actual_row_value(){

		$actual_value =  $_POST['actual_value'];
		$association_id =  $_POST['association_id'];
		$organization_uid =  $_POST['organization_uid'];
		$result = $this->ajax_model->update_actual_value_kpi_to_division($actual_value,$association_id,$organization_uid);

	}

	/**
	 * [update_employee_actual_row_value description]
	 * @return [type] [description]
	 */
	public function update_employee_actual_row_value(){
		$actual_value =  $_POST['actual_value'];
		$association_id =  $_POST['association_id'];
		$organization_uid =  $_POST['organization_uid'];
		$result = $this->ajax_model->update_actual_value_kpi_to_employee($actual_value,$association_id,$organization_uid);
	}

    /**
     *  update_division_kpi_actual_row_value for records table
     */

    public function update_division_kpi_actual_row_value(){
        $actual_value =  $_POST['actual_value'];
        $record_id =  $_POST['record_id'];
        $association_id =  $_POST['association_id'];
        $organization_uid =  $_POST['organization_uid'];

        //update record tables previous data
        $result = $this->ajax_model->update_actual_value_kpi_records_to_division($actual_value,$record_id,$association_id,$organization_uid);
        
    }

    /**
     * [update_employee_kpi_actual_row_value description]
     * update previous records of the record table according to employee
     * @return [type] [description]
     */
    public function update_employee_kpi_actual_row_value(){
        $actual_value =  $_POST['actual_value'];
        $record_id =  $_POST['record_id'];
        $association_id =  $_POST['association_id'];
        $organization_uid =  $_POST['organization_uid'];

        //update record tables previous data
        $result = $this->ajax_model->update_actual_value_kpi_records_to_employee($actual_value,$record_id,$association_id,$organization_uid);
    }
	


    public function getKpiList(){
        if(isset($_POST['employee_id'])){
            $emp_id = $_POST['employee_id'];
            $org_uid = $_POST['organization_uid'];

            $result = $this->kpi_to_employee_model->get_kpi_list_for_employee($emp_id,$org_uid);
            $this->output->set_output(json_encode($result));
        }else{
            return false;
        }
    }


    public function getKpiListForDivisions(){
         if(isset($_POST['division_id'])){
            $div_id = $_POST['division_id'];
            $org_uid = $_POST['organization_uid'];

            $result = $this->kpi_to_division_model->get_kpi_list_for_division($div_id,$org_uid);
            $this->output->set_output(json_encode($result));
        }else{
            return false;
        }
    }


    public function getEmployeeListForDivision(){
         if(isset($_POST['division_id'])){
            $div_id = $_POST['division_id'];
            $org_uid = $_POST['organization_uid'];

            $result = $this->employee_model->get_employee_list_for_division($div_id,$org_uid);
            $this->output->set_output(json_encode($result));
        }else{
            return false;
        }   
    }


    public function getKpiListForEmployees(){
         if(isset($_POST['employee_id'])){
            $employee_id = $_POST['employee_id'];
            $org_uid = $_POST['organization_uid'];

            $result = $this->kpi_model->get_kpi_list_for_employee($employee_id,$org_uid);
            $this->output->set_output(json_encode($result));
        }else{
            return false;
        }   
    }


    public function load_KPI_detail_report(){
         if(isset($_POST['kpi_id'])){
            $kpi_id = $_POST['kpi_id'];

            redirect('dashboard/report/');
           
        }else{
            return false;
        }   
    }


    /**
     * [getValuesForIndicators ajax
     * @return [type] [description]
     */
    public function getValuesForIndicators(){
        if(isset($_POST['organization_uid'])){
            $organization_uid = $_POST['organization_uid'];
            $division_id = $_POST['division_id'];
            $employee_id = $_POST['employee_id'];
          
            $result = $this->kpi_model->get_kpi_for_indicators($organization_uid ,$division_id,$employee_id);

            $this->output->set_output(json_encode($result));
        }else{
            return false;
        }   
    }

    /**
     * [getAvailableEmployeeTargetForKPI description]
     * @return [type] [description]
     */
    public function getAvailableEmployeeTargetForKPI(){
        if(isset($_POST['organization_uid'])){
            $organization_uid = $_POST['organization_uid'];
            $kpi_id = $_POST['kpi_id'];
            $employee_id = $_POST['employee_id'];
           
            $result = $this->kpi_model->get_available_employee_kpi_target_balance($organization_uid ,$kpi_id,$employee_id);
            $this->output->set_output(json_encode($result));
        }else{
            return false;
        }   
    }


    public function getReportSummary(){

        if(isset($_POST['organization_uid'])){
            $organization_uid = $_POST['organization_uid'];
            $division_id = $_POST['division_id'];
            $employee_id = $_POST['employee_id'];
            $from_date = $_POST['from_date'];
            $to_date = $_POST['to_date'];


            $finalDay = '';
            if($to_date){
                date_default_timezone_set("Asia/Bangkok");
                $finalDay = DateTime::createFromFormat('Y-m-d', $to_date);
                $finalDay->modify('+1 day');
                $finalDay =  $finalDay->format('Y-m-d');
            }



            $result = $this->kpi_model->get_kpi_for_reports($organization_uid,$division_id,$employee_id,$from_date,$finalDay);

            $this->output->set_output(json_encode($result));
            //$this->output->set_output(json_encode($_POST));
        }else{
            return false;
        }   
                           
       
    }



}